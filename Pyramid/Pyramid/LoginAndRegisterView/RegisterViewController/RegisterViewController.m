//
//  RegisterViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "RegisterViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"
#import "NBPhoneMetaDataGenerator.h"
#import "NBAsYouTypeFormatter.h"
#import "NBPhoneNumberUtil.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.titleView = [SHARED_APPDELEGATE getNavigationCenterWithTitle:NavigationSignUp fontSize:18];
    
    UIImage *backButtonImage = [UIImage imageNamed:@"Back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton setImage:backButtonImage
                forState:UIControlStateNormal];
    
    backButton.frame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self
                   action:@selector(popViewController)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
    txtFirstName.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtFirstName.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtFirstName.leftViewMode = UITextFieldViewModeAlways;
    txtFirstName.rightViewMode = UITextFieldViewModeAlways;
    txtFirstName.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtFirstName.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtFirstName
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtLastName.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtLastName.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtLastName.leftViewMode = UITextFieldViewModeAlways;
    txtLastName.rightViewMode = UITextFieldViewModeAlways;
    txtLastName.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtLastName.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtLastName
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtEmailID.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtEmailID.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtEmailID.leftViewMode = UITextFieldViewModeAlways;
    txtEmailID.rightViewMode = UITextFieldViewModeAlways;
    txtEmailID.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtEmailID.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtYourEmail
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtPassword.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPassword.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPassword.leftViewMode = UITextFieldViewModeAlways;
    txtPassword.rightViewMode = UITextFieldViewModeAlways;
    txtPassword.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtPassword.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtPassword
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtMobileNumber.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtMobileNumber
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    viewMobile.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    imgFlag.image = [UIImage imageNamed:@"India"];
    
    [btnCountry setTitle:@"+91" forState:UIControlStateNormal];
    
    strCountryName = @"India";
    strCountryDialCode = @"+91";
    strCountryCode = @"IN";
}

-(void)viewWillAppear:(BOOL)animated {
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

#pragma mark -
#pragma mark - Pop To View Mothod

-(void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickSelectCountryBtn:(id)sender {
    CountryListViewController *countryView = [[CountryListViewController alloc] initWithNibName:@"CountryListViewController" delegate:self];
    countryView.delegate = self;
    [self presentPopupViewController:countryView animationType:MJPopupViewAnimationFade];
}

#pragma mark -
#pragma mark - Country_Flag_And_Code_Delegate_Method

- (void)didSelectCountry:(NSDictionary *)country atAction:(NSString *)strAction {
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        
    if ([strAction isEqualToString:@"AddCountry"]) {
        NSLog(@"%@",[country safeObjectForKey:@"code"]);
        NSLog(@"%@",[country safeObjectForKey:@"dial_code"]);
        NSLog(@"%@",[country safeObjectForKey:@"name"]);
        
        strCountryName = [NSString stringWithFormat:@"%@",[country safeObjectForKey:@"name"]];
        strCountryDialCode = [NSString stringWithFormat:@"%@",[country safeObjectForKey:@"dial_code"]];
        strCountryCode = [NSString stringWithFormat:@"%@",[country safeObjectForKey:@"code"]];
        
        imgFlag.image = [UIImage imageNamed:strCountryName];
        
//        NSString *strDisplayCountry = [NSString stringWithFormat:@"%@ (%@)",strCountryName,strCountryDialCode];
        [btnCountry setTitle:strCountryDialCode forState:UIControlStateNormal];
    }
}

-(void)setCountryCodeBasePhoneNumber:(NSString *)strPhoneCode {
    
//    imgPhoneNumberCorrect.hidden = NO;
    
    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
    
    NBPhoneNumber *phoneNumberCountry = [phoneUtil parse:txtMobileNumber.text defaultRegion:strPhoneCode error:nil];
    NSString *strPhoneFormate = [NSString stringWithFormat:@"%@",[phoneUtil format:phoneNumberCountry numberFormat:NBEPhoneNumberFormatNATIONAL error:nil]];
    
    NSLog(@"- isValidNumber [%@]", [phoneUtil isValidNumber:phoneNumberCountry] ? @"YES" : @"NO");
    strPhoneNumberValid = [NSString stringWithFormat:@"%@",[phoneUtil isValidNumber:phoneNumberCountry] ? @"YES" : @"NO"];
    
    if ([[phoneUtil isValidNumber:phoneNumberCountry] ? @"YES" : @"NO" isEqualToString:@"YES"]) {
        //imgPhoneNumberCorrect.image = [UIImage imageNamed:@"CorrectNumber"];
    }else{
        //imgPhoneNumberCorrect.image = [UIImage imageNamed:@"wrongBtn"];
    }
    
    if (![strPhoneFormate isEqualToString:@"(null)"]) {
        txtMobileNumber.text = strPhoneFormate;
    }
}

#pragma mark -
#pragma mark - Validation_Signup_User_Info

-(BOOL)PyramideSignupValidation {

    NSString *strFirstName  = [CommonClass trimString:txtFirstName.text];
    NSString *strLastName  = [CommonClass trimString:txtLastName.text];
    NSString *strUserEmailId  = [CommonClass trimString:txtEmailID.text];
    NSString *strPassword  = [CommonClass trimString:txtPassword.text];
    NSString *strMobileNumber  = [CommonClass trimString:txtMobileNumber.text];
     
    if ([strFirstName length] == 0) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideFirstName delegate:self];
        return NO;
    }
    if ([strLastName length] == 0) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideLastName delegate:self];
        return NO;
    }
    if ([strUserEmailId length] == 0) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideEmailID delegate:self];
        return NO;
    }
    if(![CommonClass textIsValidEmailFormat:[CommonClass trimString:strUserEmailId]]) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideValidEmail delegate:self];
        return NO;
    }
    if([CommonClass trimString:strPassword].length == 0) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:providePassword delegate:self];
        return NO;
    }
    if(strPassword.length < 6) {
       [CommonClass showAlertWithTitle:provideAlert andMessage:providePasswordValid delegate:self];
       return NO;
    }
    if ([strMobileNumber length] == 0) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideMobileNumber delegate:self];
        return NO;
    }

    return  YES;
}

- (IBAction)onClickSignUpBtn:(id)sender {
    
    if ([self PyramideSignupValidation]) {
        
        if (afPyramideSignUp) {
           [afPyramideSignUp cancelRequest];
           afPyramideSignUp.delegate = self;
           afPyramideSignUp = nil;
        }

        afPyramideSignUp = [[AFNetworkUtility alloc] init];
        afPyramideSignUp.delegate = self;

        [SHARED_APPDELEGATE showLoadingView];
                
        NSMutableDictionary *dictSignup = [[NSMutableDictionary alloc] init];
        
        [dictSignup setValue:@"" forKey:@"person_type"];
        [dictSignup setValue:txtFirstName.text forKey:@"firstname"];
        [dictSignup setValue:txtLastName.text forKey:@"lastname"];
        [dictSignup setValue:txtEmailID.text forKey:@"email"];
        [dictSignup setValue:txtPassword.text forKey:@"password"];
        [dictSignup setValue:txtMobileNumber.text forKey:@"mobile"];
        [dictSignup setValue:@"english" forKey:@"lang"];
            
        NSLog(@"LoginPARAM ::: %@",dictSignup);

        [afPyramideSignUp requestWithUrl:SignUp_Url param:dictSignup];
    }
}

- (IBAction)onClickLoginBtn:(id)sender {
    [self popViewController];
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataSuccess:(NSMutableDictionary *)dictResponce {
        
    if (utility == afPyramideSignUp) {
        
        NSLog(@"SIGNUPRESPONCE ::: %@", dictResponce);
        
        [SHARED_APPDELEGATE hideLoadingView];
                        
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            [CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
            UIAlertController *alert = [UIAlertController
                                       alertControllerWithTitle:provideAlert
                                       message:[dictResponce safeObjectForKey:@"message"]
                                       preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *Ok = [UIAlertAction
                              actionWithTitle:provideOk
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action) {
                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                  [self popViewController];
                              }];

            [alert addAction:Ok];
            [self presentViewController:alert animated:YES completion:nil];
        }else {
            [CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
        }
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Fail_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataFail:(NSError *)error {
    [SHARED_APPDELEGATE hideLoadingView];
    NSString *strError = [NSString stringWithFormat:@"%ld",(long)error.code];
    [CommonClass showAlertWithTitle:provideAlert andMessage:strError delegate:self];
}


@end
