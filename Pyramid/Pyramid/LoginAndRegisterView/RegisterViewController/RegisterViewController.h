//
//  RegisterViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryListViewController.h"
#import "AFNetworkUtility.h"

NS_ASSUME_NONNULL_BEGIN

@interface RegisterViewController : UIViewController <CountryListViewDelegate,AFNetworkUtilityDelegate> {
    
    IBOutlet UIView *viewMobile;
    
    IBOutlet UITextField *txtFirstName;
    IBOutlet UITextField *txtLastName;
    IBOutlet UITextField *txtEmailID;
    IBOutlet UITextField *txtPassword;
    IBOutlet UITextField *txtMobileNumber;
    
    IBOutlet UIButton *btnCountry;
    
    IBOutlet UIImageView *imgFlag;
    
    NSString *strCountryName;
    NSString *strCountryDialCode;
    NSString *strCountryCode;
    NSString *strPhoneNumberValid;
    
    AFNetworkUtility *afPyramideSignUp;
}

@end

NS_ASSUME_NONNULL_END
