//
//  LoginViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    txtEmailID.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtEmailID.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtEmailID.leftViewMode = UITextFieldViewModeAlways;
    txtEmailID.rightViewMode = UITextFieldViewModeAlways;
    txtEmailID.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtEmailID.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtYourEmail
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtPassword.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPassword.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPassword.leftViewMode = UITextFieldViewModeAlways;
    txtPassword.rightViewMode = UITextFieldViewModeAlways;
    txtPassword.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtPassword.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtPassword
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    btnSelectLanguage.layer.borderColor = appTextFieldBoardColor.CGColor;
}

-(void)viewWillAppear:(BOOL)animated {
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (IBAction)onClickSelectLanguageBtn:(id)sender {
    NSMutableArray *arrLanguageList = [[NSMutableArray alloc] initWithObjects:@"English",@"Croatian",@"Slovene",@"Italian", nil];

    if (dropDown == nil) {
       CGFloat f = 160;
       dropDown = [[NIDropDown alloc] showDropDown:sender :&f :arrLanguageList :nil :@"down" atAction:@""];
       dropDown.delegate = self;
    }else{
       [dropDown hideDropDown:sender];
       [self rel];
    }
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender atIndexPath:(NSInteger)index {
    NSLog(@"LanguageName: %@",btnSelectLanguage.titleLabel.text);
    [self rel];
}

-(void)rel {
    dropDown = nil;
}

- (IBAction)onClickFacebookBtn:(id)sender {
    
}

- (IBAction)onClickGoogleBtn:(id)sender {
    
}

- (IBAction)onClickForgotPasswordBtn:(id)sender {
    ForgotPasswordViewController *viewForgotPass = [[ForgotPasswordViewController alloc] initWithNibName:@"ForgotPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:viewForgotPass animated:YES];
}

- (IBAction)onClickPyramidSignUpBtn:(id)sender {
    RegisterViewController *viewRegister = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:viewRegister animated:YES];
}

#pragma mark -
#pragma mark - Validation_Login_User_Info

-(BOOL)PyramideLoginValidation {

    NSString *strUserEmailId  = [CommonClass trimString:txtEmailID.text];
    NSString *strPassword  = [CommonClass trimString:txtPassword.text];
 
    if ([strUserEmailId length] == 0) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideEmailID delegate:self];
        return NO;
    }
    if(![CommonClass textIsValidEmailFormat:[CommonClass trimString:strUserEmailId]]) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideValidEmail delegate:self];
        return NO;
    }
    if([CommonClass trimString:strPassword].length == 0) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:providePassword delegate:self];
        return NO;
    }
    if(strPassword.length < 6) {
       [CommonClass showAlertWithTitle:provideAlert andMessage:providePasswordValid delegate:self];
       return NO;
    }
    if ([btnSelectLanguage.titleLabel.text isEqualToString:@""]) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideSelectLanguage delegate:self];
        return NO;
    }
    return  YES;
}

- (IBAction)onClickPyramidLoginBtn:(id)sender {
    
    if ([self PyramideLoginValidation]) {
        
        if (afPyramidePlayerLogin) {
           [afPyramidePlayerLogin cancelRequest];
           afPyramidePlayerLogin.delegate = self;
           afPyramidePlayerLogin = nil;
        }

        afPyramidePlayerLogin = [[AFNetworkUtility alloc] init];
        afPyramidePlayerLogin.delegate = self;

        [SHARED_APPDELEGATE showLoadingView];
        
        NSMutableDictionary *dictLogin = [[NSMutableDictionary alloc] init];
        
        [dictLogin setValue:txtEmailID.text forKey:@"email"];
        [dictLogin setValue:txtPassword.text forKey:@"password"];
        [dictLogin setValue:@"" forKey:@"competition_id"];
        
        NSString *strLanguage = [NSString stringWithFormat:@"%@",[btnSelectLanguage.titleLabel.text lowercaseString]];
        
        [dictLogin setValue:strLanguage forKey:@"lang"];
            
        NSLog(@"LoginPARAM ::: %@",dictLogin);

        [afPyramidePlayerLogin requestWithUrl:Login_Url param:dictLogin andToken:@"9f03af9e4c9d0da7a26af71c2a8da2da"];
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataSuccess:(NSMutableDictionary *)dictResponce {
        
    if (utility == afPyramidePlayerLogin) {
        
        NSLog(@"LOGINRESPONCE ::: %@", dictResponce);
        
        [SHARED_APPDELEGATE hideLoadingView];
                        
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            
            NSDictionary *dictLogin = [dictResponce safeObjectForKey:@"data"];
            
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            
            [userDefault setObject:[dictLogin safeObjectForKey:@"id"] forKey:USERID];
            [userDefault setObject:[dictLogin safeObjectForKey:@"api-key"] forKey:APIKEY];
            [userDefault setObject:[dictLogin safeObjectForKey:@"firstname"] forKey:FIRSTNAME];
            [userDefault setObject:[dictLogin safeObjectForKey:@"lastname"] forKey:LASTNAME];
            [userDefault setObject:[dictLogin safeObjectForKey:@"phone"] forKey:PHONENUMBER];
            [userDefault setObject:[dictLogin safeObjectForKey:@"profile"] forKey:PROFILEPIC];
            [userDefault setObject:[dictLogin safeObjectForKey:@"last_selected_language"] forKey:SELECTLANG];
            [userDefault setObject:[dictLogin safeObjectForKey:@"subscription_plan_id"] forKey:SUBPLANID];
            [userDefault setObject:[dictLogin safeObjectForKey:@"token"] forKey:TOKEN];
            [userDefault setObject:[dictLogin safeObjectForKey:@"user_email"] forKey:USEREMAIL];
            [userDefault setObject:[dictLogin safeObjectForKey:@"user_name"] forKey:USERNAME];
            [userDefault setObject:[dictLogin safeObjectForKey:@"user_type"] forKey:USERTYPE];
            
            NSDictionary *dictSubsctiption = [dictLogin safeObjectForKey:@"subscription_plan"];
            
            [userDefault setObject:[dictSubsctiption safeObjectForKey:@"is_plan_expire"] forKey:SUBSCRIPPLANEXPIRE];
            [userDefault setObject:[dictSubsctiption safeObjectForKey:@"name"] forKey:SUBSCRIPPLANNAME];
            [userDefault setObject:[dictSubsctiption safeObjectForKey:@"no_of_competition"] forKey:SUBSCRIPPLANCOMPETITION];
            [userDefault setObject:[dictSubsctiption safeObjectForKey:@"no_of_players"] forKey:SUBSCRIPPLANPLAYERS];
            [userDefault setObject:[dictSubsctiption safeObjectForKey:@"plan_end_date"] forKey:SUBSCRIPPLANENDDATE];
            [userDefault setObject:[dictSubsctiption safeObjectForKey:@"plan_price"] forKey:SUBSCRIPPLANPRICE];
            [userDefault setObject:[dictSubsctiption safeObjectForKey:@"plan_start_date"] forKey:SUBSCRIPPLANSTARTDATE];
            
            [SHARED_APPDELEGATE setPyramidViewScreen];
        }else {
            [CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
        }
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Fail_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataFail:(NSError *)error {
    [SHARED_APPDELEGATE hideLoadingView];
    NSString *strError = [NSString stringWithFormat:@"%ld",(long)error.code];
    [CommonClass showAlertWithTitle:provideAlert andMessage:strError delegate:self];
}

@end
