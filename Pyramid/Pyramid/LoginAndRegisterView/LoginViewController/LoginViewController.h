//
//  LoginViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "ForgotPasswordViewController.h"
#import "RegisterViewController.h"
#import "AFNetworkUtility.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginViewController : UIViewController <NIDropDownDelegate,AFNetworkUtilityDelegate> {
    
    IBOutlet UITextField *txtEmailID;
    IBOutlet UITextField *txtPassword;
    
    IBOutlet UIButton *btnSelectLanguage;
    
    NIDropDown *dropDown;
    
    AFNetworkUtility *afPyramidePlayerLogin;
}

@end

NS_ASSUME_NONNULL_END
