//
//  ForgotPasswordViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.titleView = [SHARED_APPDELEGATE getNavigationCenterWithTitle:NavigationForgotPassword fontSize:18];
    
    UIImage *backButtonImage = [UIImage imageNamed:@"Back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton setImage:backButtonImage
                forState:UIControlStateNormal];
    
    backButton.frame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self
                   action:@selector(popViewController)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
    txtEmailID.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtEmailID.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtEmailID.leftViewMode = UITextFieldViewModeAlways;
    txtEmailID.rightViewMode = UITextFieldViewModeAlways;
    txtEmailID.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtEmailID.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtYourEmail
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
}

-(void)viewWillAppear:(BOOL)animated {
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

#pragma mark -
#pragma mark - Pop To View Mothod

-(void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickResetPasswordBtn:(id)sender {
    
    NSString *strUserEmailId  = [CommonClass trimString:txtEmailID.text];

    if ([strUserEmailId length] == 0) {
       [CommonClass showAlertWithTitle:provideAlert andMessage:provideEmailID delegate:self];
       return;
    }

    if(![CommonClass textIsValidEmailFormat:[CommonClass trimString:strUserEmailId]]) {
         [CommonClass showAlertWithTitle:provideAlert andMessage:provideValidEmail delegate:self];
         return;
    }
    
    if (afPyramideForgotPassword) {
       [afPyramideForgotPassword cancelRequest];
       afPyramideForgotPassword.delegate = self;
       afPyramideForgotPassword = nil;
    }

    afPyramideForgotPassword = [[AFNetworkUtility alloc] init];
    afPyramideForgotPassword.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];
    
    NSMutableDictionary *dictForgot = [[NSMutableDictionary alloc] init];
    
    [dictForgot setValue:txtEmailID.text forKey:@"email"];
    [dictForgot setValue:@"english" forKey:@"lang"];
        
    NSLog(@"ForgotPARAM ::: %@",dictForgot);

    [afPyramideForgotPassword requestWithUrl:ForgotPas_Url param:dictForgot andToken:@"9f03af9e4c9d0da7a26af71c2a8da2da"];
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataSuccess:(NSMutableDictionary *)dictResponce {
        
    if (utility == afPyramideForgotPassword) {
        
        NSLog(@"FORGOTRESPONCE ::: %@", dictResponce);
        
        [SHARED_APPDELEGATE hideLoadingView];
                        
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            [CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
        }else {
            [CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
        }
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Fail_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataFail:(NSError *)error {
    [SHARED_APPDELEGATE hideLoadingView];
    NSString *strError = [NSString stringWithFormat:@"%ld",(long)error.code];
    [CommonClass showAlertWithTitle:provideAlert andMessage:strError delegate:self];
}

@end
