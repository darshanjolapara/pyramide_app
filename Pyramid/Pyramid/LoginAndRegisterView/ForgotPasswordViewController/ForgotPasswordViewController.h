//
//  ForgotPasswordViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworkUtility.h"

NS_ASSUME_NONNULL_BEGIN

@interface ForgotPasswordViewController : UIViewController <AFNetworkUtilityDelegate> {
    
    IBOutlet UITextField *txtEmailID;
    
    AFNetworkUtility *afPyramideForgotPassword;
}

@end

NS_ASSUME_NONNULL_END
