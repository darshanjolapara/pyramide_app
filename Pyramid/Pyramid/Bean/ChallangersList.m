//
//  ChallangersList.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/8/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "ChallangersList.h"

@implementation ChallangersList

@synthesize player_row_id;
@synthesize player_name;
@synthesize player_position;
@synthesize player_efficiency;
@synthesize player_is_challenge;
@synthesize player_challenged_stattus;
@synthesize player_status_color;
@synthesize player_status;
@synthesize player_profile;
@synthesize player_born_year;

@end
