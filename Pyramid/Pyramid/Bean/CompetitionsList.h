//
//  CompetitionsList.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/8/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CompetitionsList : UIView

@property (nonatomic , strong) NSString *CompetitionsID;
@property (nonatomic , strong) NSString *profile;
@property (nonatomic , strong) NSString *name;
@property (nonatomic , strong) NSString *slug;
@property (nonatomic , strong) NSString *club_id;
@property (nonatomic , strong) NSString *ComPetDescription;
@property (nonatomic , strong) NSString *add_uid;
@property (nonatomic , strong) NSString *startDate;
@property (nonatomic , strong) NSString *endDate;
@property (nonatomic , strong) NSString *LastSignUpDate;
@property (nonatomic , strong) NSString *is_cmpt_over;

@end

NS_ASSUME_NONNULL_END
