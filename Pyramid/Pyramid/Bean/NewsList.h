//
//  NewsList.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/8/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewsList : UIView

@property (nonatomic , strong) NSString *add_date;
@property (nonatomic , strong) NSString *add_uid;
@property (nonatomic , strong) NSString *competition_id;
@property (nonatomic , strong) NSString *date;
@property (nonatomic , strong) NSString *del_date;
@property (nonatomic , strong) NSString *del_uid;
@property (nonatomic , strong) NSString *NewsDescription;
@property (nonatomic , strong) NSString *displayDate;
@property (nonatomic , strong) NSString *edit_date;
@property (nonatomic , strong) NSString *edit_uid;
@property (nonatomic , strong) NSString *Newsid;
@property (nonatomic , strong) NSString *lang;
@property (nonatomic , strong) NSString *photo;
@property (nonatomic , strong) NSString *status;
@property (nonatomic , strong) NSString *title;
@property (nonatomic , strong) NSString *video;
@property (nonatomic , strong) NSString *video_name;
@property (nonatomic , strong) UIImage *imgThumbnail;

@end

NS_ASSUME_NONNULL_END
