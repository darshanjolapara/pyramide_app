//
//  RulesList.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/8/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "RulesList.h"

@implementation RulesList

@synthesize ConfigID;
@synthesize lang;
@synthesize rule_id;
@synthesize ruleCount;
@synthesize ruleDescription;

@end
