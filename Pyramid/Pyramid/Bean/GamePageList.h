//
//  GamePageList.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/8/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GamePageList : UIView

@property (nonatomic , strong) NSString *score_1_1;
@property (nonatomic , strong) NSString *score_1_2;
@property (nonatomic , strong) NSString *score_1_3;
@property (nonatomic , strong) NSString *score_1_4;
@property (nonatomic , strong) NSString *score_1_5;
@property (nonatomic , strong) NSString *score_1_6;
@property (nonatomic , strong) NSString *score_1_7;
@property (nonatomic , strong) NSString *score_1_8;
@property (nonatomic , strong) NSString *score_1_9;
@property (nonatomic , strong) NSString *score_1_10;
@property (nonatomic , strong) NSString *score_1_11;
@property (nonatomic , strong) NSString *score_2_1;
@property (nonatomic , strong) NSString *score_2_2;
@property (nonatomic , strong) NSString *score_2_3;
@property (nonatomic , strong) NSString *score_2_4;
@property (nonatomic , strong) NSString *score_2_5;
@property (nonatomic , strong) NSString *score_2_6;
@property (nonatomic , strong) NSString *score_2_7;
@property (nonatomic , strong) NSString *score_2_8;
@property (nonatomic , strong) NSString *score_2_9;
@property (nonatomic , strong) NSString *score_2_10;
@property (nonatomic , strong) NSString *score_2_11;
@property (nonatomic , strong) NSString *challenger_score;
@property (nonatomic , strong) NSString *challenged_to_score;
@property (nonatomic , strong) NSString *challenged_to_Profile;
@property (nonatomic , strong) NSString *challenged_to_PlayerName;
@property (nonatomic , strong) NSString *challenged_Profile;
@property (nonatomic , strong) NSString *challenged_PlayerName;
@property (nonatomic , strong) NSString *challengerDate;
@property (nonatomic , strong) NSString *competition_play_hour;
@property (nonatomic , strong) NSString *court_name;
@property (nonatomic , strong) NSString *video;
@property (nonatomic , strong) NSString *pictures;

@end

NS_ASSUME_NONNULL_END
