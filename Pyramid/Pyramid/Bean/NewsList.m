//
//  NewsList.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/8/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "NewsList.h"

@implementation NewsList

@synthesize add_date;
@synthesize add_uid;
@synthesize competition_id;
@synthesize date;
@synthesize del_date;
@synthesize del_uid;
@synthesize NewsDescription;
@synthesize displayDate;
@synthesize edit_date;
@synthesize edit_uid;
@synthesize Newsid;
@synthesize lang;
@synthesize photo;
@synthesize status;
@synthesize title;
@synthesize video;
@synthesize video_name;
@synthesize imgThumbnail;

@end
