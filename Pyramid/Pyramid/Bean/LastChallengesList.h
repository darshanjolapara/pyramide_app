//
//  LastChallengesList.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/8/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LastChallengesList : UIView

@property (nonatomic , strong) NSString *challengerDate;
@property (nonatomic , strong) NSString *PlayerInfo;

@property (nonatomic , strong) NSString *challenger_score;
@property (nonatomic , strong) NSString *challenged_to_score;
@property (nonatomic , strong) NSString *challenger_FirstName;
@property (nonatomic , strong) NSString *challenged_to_FirstName;
@property (nonatomic , strong) NSString *challenger_LastName;
@property (nonatomic , strong) NSString *challenged_to_LastName;
@property (nonatomic , strong) NSString *pictures;
@property (nonatomic , strong) NSString *video;

@end

NS_ASSUME_NONNULL_END
