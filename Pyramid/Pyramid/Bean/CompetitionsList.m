//
//  CompetitionsList.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/8/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "CompetitionsList.h"

@implementation CompetitionsList

@synthesize CompetitionsID;
@synthesize profile;
@synthesize name;
@synthesize slug;
@synthesize club_id;
@synthesize ComPetDescription;
@synthesize add_uid;
@synthesize startDate;
@synthesize endDate;
@synthesize LastSignUpDate;
@synthesize is_cmpt_over;

@end
