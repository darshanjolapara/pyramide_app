//
//  StatisticsList.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/8/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "StatisticsList.h"

@implementation StatisticsList

@synthesize month_index;
@synthesize player_efficiency;
@synthesize player_id;
@synthesize player_name;
@synthesize player_o_s;
@synthesize player_total_games;
@synthesize player_total_lose_games;
@synthesize player_total_win_games;
@synthesize total_months;

@synthesize arrMonthsList;

@end
