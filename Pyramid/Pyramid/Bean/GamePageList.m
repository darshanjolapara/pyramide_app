//
//  GamePageList.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/8/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "GamePageList.h"

@implementation GamePageList

@synthesize score_1_1;
@synthesize score_1_2;
@synthesize score_1_3;
@synthesize score_1_4;
@synthesize score_1_5;
@synthesize score_1_6;
@synthesize score_1_7;
@synthesize score_1_8;
@synthesize score_1_9;
@synthesize score_1_10;
@synthesize score_1_11;
@synthesize score_2_1;
@synthesize score_2_2;
@synthesize score_2_3;
@synthesize score_2_4;
@synthesize score_2_5;
@synthesize score_2_6;
@synthesize score_2_7;
@synthesize score_2_8;
@synthesize score_2_9;
@synthesize score_2_10;
@synthesize score_2_11;
@synthesize challenger_score;
@synthesize challenged_to_score;
@synthesize challenged_to_Profile;
@synthesize challenged_to_PlayerName;
@synthesize challenged_Profile;
@synthesize challenged_PlayerName;
@synthesize challengerDate;
@synthesize competition_play_hour;
@synthesize court_name;
@synthesize video;
@synthesize pictures;

@end
