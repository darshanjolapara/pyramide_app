//
//  ChallangersList.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/8/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChallangersList : UIView

@property (nonatomic , strong) NSString *player_row_id;
@property (nonatomic , strong) NSString *player_name;
@property (nonatomic , strong) NSString *player_position;
@property (nonatomic , strong) NSString *player_efficiency;
@property (nonatomic , strong) NSString *player_is_challenge;
@property (nonatomic , strong) NSString *player_challenged_stattus;
@property (nonatomic , strong) NSString *player_status_color;
@property (nonatomic , strong) NSString *player_status;
@property (nonatomic , strong) NSString *player_profile;
@property (nonatomic , strong) NSString *player_born_year;

@end

NS_ASSUME_NONNULL_END
