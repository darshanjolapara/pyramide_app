//
//  StatisticsList.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/8/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StatisticsList : UIView

@property (nonatomic , strong) NSString *month_index;
@property (nonatomic , strong) NSString *player_efficiency;
@property (nonatomic , strong) NSString *player_id;
@property (nonatomic , strong) NSString *player_name;
@property (nonatomic , strong) NSString *player_o_s;
@property (nonatomic , strong) NSString *player_total_games;
@property (nonatomic , strong) NSString *player_total_lose_games;
@property (nonatomic , strong) NSString *player_total_win_games;
@property (nonatomic , strong) NSString *total_months;

@property (nonatomic , strong) NSMutableArray *arrMonthsList;

@end

NS_ASSUME_NONNULL_END
