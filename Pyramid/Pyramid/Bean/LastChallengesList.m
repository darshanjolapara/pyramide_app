//
//  LastChallengesList.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/8/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "LastChallengesList.h"

@implementation LastChallengesList

@synthesize challengerDate;
@synthesize PlayerInfo;

@synthesize challenger_score;
@synthesize challenged_to_score;
@synthesize challenger_FirstName;
@synthesize challenged_to_FirstName;
@synthesize challenger_LastName;
@synthesize challenged_to_LastName;
@synthesize pictures;
@synthesize video;

@end
