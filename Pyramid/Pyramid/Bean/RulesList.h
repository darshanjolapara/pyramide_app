//
//  RulesList.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/8/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RulesList : UIView

@property (nonatomic , strong) NSString *ConfigID;
@property (nonatomic , strong) NSString *lang;
@property (nonatomic , strong) NSString *rule_id;
@property (nonatomic , strong) NSString *ruleCount;
@property (nonatomic , strong) NSString *ruleDescription;

@end

NS_ASSUME_NONNULL_END
