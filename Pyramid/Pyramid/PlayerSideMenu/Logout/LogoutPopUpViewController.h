//
//  LogoutPopUpViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MJPopupViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol LogoutPopUpViewControllerDelegate <NSObject>

-(void)setPyramidePlayerLogoutAction:(NSString *)strAction;

@end

@interface LogoutPopUpViewController : UIViewController

@property (nonatomic , strong) id<LogoutPopUpViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
