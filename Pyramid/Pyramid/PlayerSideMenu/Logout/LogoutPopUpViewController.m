//
//  LogoutPopUpViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "LogoutPopUpViewController.h"

@interface LogoutPopUpViewController ()

@end

@implementation LogoutPopUpViewController

@synthesize  delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)onClickOkayBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setPyramidePlayerLogoutAction:)]) {
        [delegate setPyramidePlayerLogoutAction:@"Okay"];
    }
}

- (IBAction)onClickCancelBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setPyramidePlayerLogoutAction:)]) {
        [delegate setPyramidePlayerLogoutAction:@"Cancel"];
    }
}


@end
