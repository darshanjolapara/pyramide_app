//
//  ChangePasswordViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.titleView = [SHARED_APPDELEGATE getNavigationCenterWithTitle:NavigationChangePassword fontSize:18];
    
    txtOldPassword.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtOldPassword.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtOldPassword.leftViewMode = UITextFieldViewModeAlways;
    txtOldPassword.rightViewMode = UITextFieldViewModeAlways;
    txtOldPassword.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtOldPassword.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtOldPPlaceholder
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtNewPassword.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtNewPassword.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtNewPassword.leftViewMode = UITextFieldViewModeAlways;
    txtNewPassword.rightViewMode = UITextFieldViewModeAlways;
    txtNewPassword.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtNewPassword.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtNewPPlaceholder
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtConfirmPassword.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtConfirmPassword.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtConfirmPassword.leftViewMode = UITextFieldViewModeAlways;
    txtConfirmPassword.rightViewMode = UITextFieldViewModeAlways;
    txtConfirmPassword.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtConfirmPassword.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtNewPPlaceholder
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
}

#pragma mark -
#pragma mark - Validation_Change_Password_User_Info

-(BOOL)PyramideChangePasswordValidation {

    NSString *strOldPass  = [CommonClass trimString:txtOldPassword.text];
    NSString *strNewPass  = [CommonClass trimString:txtNewPassword.text];
    NSString *strConfirmPass  = [CommonClass trimString:txtConfirmPassword.text];
     
    if ([strOldPass length] == 0) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideOldPassword delegate:self];
        return NO;
    }
    if([strNewPass length] == 0) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideNewPassword delegate:self];
        return NO;
    }
    if([strNewPass length] < 6) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:providePasswordValid delegate:self];
        return NO;
    }
    if([strConfirmPass length] == 0) {
       [CommonClass showAlertWithTitle:provideAlert andMessage:provideConfirmPassword delegate:self];
       return NO;
    }
    if (![strNewPass isEqualToString:strConfirmPass]) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideNewConfirmPass delegate:self];
        return NO;
    }
    return  YES;
}

- (IBAction)onClickResetPasswordBtn:(id)sender {

    if ([self PyramideChangePasswordValidation]) {
        if (afPyramideChangePassword) {
           [afPyramideChangePassword cancelRequest];
           afPyramideChangePassword.delegate = self;
           afPyramideChangePassword = nil;
        }

        afPyramideChangePassword = [[AFNetworkUtility alloc] init];
        afPyramideChangePassword.delegate = self;

        [SHARED_APPDELEGATE showLoadingView];
        
        NSMutableDictionary *dictForgot = [[NSMutableDictionary alloc] init];
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        
        NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
        NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];
        
        [dictForgot setValue:txtOldPassword.text forKey:@"old_password"];
        [dictForgot setValue:txtNewPassword.text forKey:@"new_password"];
        [dictForgot setValue:txtConfirmPassword.text forKey:@"confirm_password"];
        
        if ([strSelectLang isEqualToString:@""]) {
            [dictForgot setValue:@"english" forKey:@"lang"];
        }else {
            [dictForgot setValue:strSelectLang forKey:@"lang"];
        }
            
        NSLog(@"ChangePassPARAM ::: %@",dictForgot);

        [afPyramideChangePassword requestWithUrl:ChangePass_Url param:dictForgot andToken:strAPIKey];
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataSuccess:(NSMutableDictionary *)dictResponce {
        
    if (utility == afPyramideChangePassword) {
        
        NSLog(@"CHANGEPASSRESPONCE ::: %@", dictResponce);
        
        [SHARED_APPDELEGATE hideLoadingView];
                        
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            [CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
        }else {
            [CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
        }
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Fail_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataFail:(NSError *)error {
    [SHARED_APPDELEGATE hideLoadingView];
    NSString *strError = [NSString stringWithFormat:@"%ld",(long)error.code];
    [CommonClass showAlertWithTitle:provideAlert andMessage:strError delegate:self];
}

@end
