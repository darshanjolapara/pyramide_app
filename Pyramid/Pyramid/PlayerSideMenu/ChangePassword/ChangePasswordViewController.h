//
//  ChangePasswordViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "MasterViewController.h"
#import "AFNetworkUtility.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChangePasswordViewController : MasterViewController <AFNetworkUtilityDelegate> {
    
    IBOutlet UITextField *txtOldPassword;
    IBOutlet UITextField *txtNewPassword;
    IBOutlet UITextField *txtConfirmPassword;
    
    AFNetworkUtility *afPyramideChangePassword;
}

@end

NS_ASSUME_NONNULL_END
