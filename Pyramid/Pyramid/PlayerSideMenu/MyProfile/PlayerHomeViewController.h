//
//  PlayerHomeViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailCellTableViewCell.h"
#import "RankingCell.h"
#import "NewsCell.h"
#import "GamePageCell.h"
#import "StaticsticCell.h"
#import "NIDropDown.h"
#import "ChangeStatusPopUpViewController.h"
#import "PlayerDetailPopUpViewController.h"
#import "StaticsticDetailViewController.h"
#import "AllGamesListViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PlayerHomeViewController : MasterViewController <UITableViewDelegate,UITableViewDataSource,NIDropDownDelegate,ChangeStatusPopUpViewControllerDelegate,DetailCellTableViewCellDelegate,PlayerDetailPopUpViewControllerDelegate,StaticsticCellDelegate> {
    
    IBOutlet UIButton *btnSelectCompetition;

    IBOutlet UIView *viewAdd;
    IBOutlet UIView *viewDisplayBtn;
    
    IBOutlet UIView *viewSelectDetail;
    IBOutlet UIView *viewSelectRatting;
    IBOutlet UIView *viewSelectNews;
    IBOutlet UIView *viewSelectGamePage;
    IBOutlet UIView *viewSelectStaticstic;
    
    IBOutlet UIView *viewHeader;
    IBOutlet UIView *viewFooter;
    
    IBOutlet UIButton *btnDetail;
    IBOutlet UIButton *btnRatting;
    IBOutlet UIButton *btnNews;
    IBOutlet UIButton *btnGamePage;
    IBOutlet UIButton *btnStaticstic;
    IBOutlet UIButton *btnMoreDetail;
    IBOutlet UIButton *btnViewChallangers;
    IBOutlet UIButton *btnAllGames;
    
    IBOutlet UITableView *tblDetailList;
    
    IBOutlet UITextField *txtSearchPlayer;
    
    IBOutlet NSLayoutConstraint *constHeightSearchView;
    IBOutlet NSLayoutConstraint *constHeightBtnView;
    
    NSMutableArray *arrDetailList;
    NSMutableArray *arrCompetitionList;
    
    NIDropDown *dropDown;
    
    BOOL isRanking;
    BOOL isNews;
    BOOL isGamePage;
    BOOL isStaticstic;
}

@end

NS_ASSUME_NONNULL_END
