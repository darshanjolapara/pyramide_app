//
//  MyProfileViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "BirthdayPopUpViewController.h"

@interface BirthdayPopUpViewController ()

@end

@implementation BirthdayPopUpViewController

@synthesize delegate;
@synthesize strAction;
@synthesize strTitle;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.layer.cornerRadius = 8.0f;
    self.view.layer.borderColor = appNavBarColor.CGColor;
    
    if ([strAction isEqualToString:@"Date"]) {
        lblbTitle.text = @"Select Date";
    }else if ([strAction isEqualToString:@"Hour"]) {
        timePicker.hidden = NO;
        datePicker.hidden = YES;
        lblbTitle.text = @"Select Hour";
    }else if ([strAction isEqualToString:@"Birth"]) {
        lblbTitle.text = @"Select Date Of Birth";
    }
}

- (IBAction)onScrollValueChange:(id)sender {
    
    if ([strAction isEqualToString:@"Hour"]) {
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        NSDate *pickerDate = [timePicker date];
        [format setDateFormat:@"hh:mm a"];
        strBDate = [format stringFromDate:pickerDate];
        
        [format setDateFormat:@"hh:mm a"];
        strSendBDate = [format stringFromDate:pickerDate];
    }else {
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        NSDate *pickerDate = [datePicker date];
        [format setDateFormat:@"dd-MMM-YYYY"];
        strBDate = [format stringFromDate:pickerDate];
        
        [format setDateFormat:@"dd-MMM-YYYY"];
        strSendBDate = [format stringFromDate:pickerDate];
    }
}

- (IBAction)onClickBirthdaySelectOkBtn:(id)sender {
    
    if (delegate && [delegate respondsToSelector:@selector(setBirthdayDetail:andSendField:atAction:)]) {
        [delegate setBirthdayDetail:strBDate andSendField:strSendBDate atAction:strAction];
    }
}

@end
