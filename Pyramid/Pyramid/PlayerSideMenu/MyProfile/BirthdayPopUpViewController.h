//
//  BirthdayPopUpViewController.h
//  LetsPlay
//
//  Created by Darshan Jolapara on 12/11/19.
//  Copyright © 2019 Augmenta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MJPopupViewController.h"

@protocol BirthdayPopUpViewControllerDelegate <NSObject>

-(void)setBirthdayDetail:(NSString *_Nullable)strBirthDate andSendField:(NSString *_Nonnull)strSendDate atAction:(NSString *)strPickerType;

@end

NS_ASSUME_NONNULL_BEGIN

@interface BirthdayPopUpViewController : UIViewController {
    
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIDatePicker *timePicker;
    
    IBOutlet UILabel *lblbTitle;
    
    NSString *strBDate;
    NSString *strSendBDate;
}

@property (nonatomic , strong) id <BirthdayPopUpViewControllerDelegate> delegate;

@property (nonatomic , strong) NSString *strTitle;
@property (nonatomic , strong) NSString *strAction;

@end

NS_ASSUME_NONNULL_END
