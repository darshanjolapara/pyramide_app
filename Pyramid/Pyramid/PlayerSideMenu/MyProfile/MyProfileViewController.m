//
//  MyProfileViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "MyProfileViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"
#import "NBPhoneMetaDataGenerator.h"
#import "NBAsYouTypeFormatter.h"
#import "NBPhoneNumberUtil.h"

@interface MyProfileViewController ()

@end

@implementation MyProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.titleView = [SHARED_APPDELEGATE getNavigationCenterWithTitle:NavigationUserProfile fontSize:18];
    
    txtName.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtName.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtName.leftViewMode = UITextFieldViewModeAlways;
    txtName.rightViewMode = UITextFieldViewModeAlways;
    txtName.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtName.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtName
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];

    txtSurname.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtSurname.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtSurname.leftViewMode = UITextFieldViewModeAlways;
    txtSurname.rightViewMode = UITextFieldViewModeAlways;
    txtSurname.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtSurname.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtSurname
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtMobileNumber.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtMobileNumber.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtMobileNumber.leftViewMode = UITextFieldViewModeAlways;
    txtMobileNumber.rightViewMode = UITextFieldViewModeAlways;
//    txtMobileNumber.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtMobileNumber.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtMobileNumber
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtEmailID.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtEmailID.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtEmailID.leftViewMode = UITextFieldViewModeAlways;
    txtEmailID.rightViewMode = UITextFieldViewModeAlways;
    txtEmailID.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtEmailID.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtYourEmail
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtAddress.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtAddress.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtAddress.leftViewMode = UITextFieldViewModeAlways;
    txtAddress.rightViewMode = UITextFieldViewModeAlways;
    txtAddress.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtAddress.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtAddress
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtAge.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtAge.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtAge.leftViewMode = UITextFieldViewModeAlways;
    txtAge.rightViewMode = UITextFieldViewModeAlways;
    txtAge.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtAge.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtAge
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtPostalCode.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPostalCode.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPostalCode.leftViewMode = UITextFieldViewModeAlways;
    txtPostalCode.rightViewMode = UITextFieldViewModeAlways;
    txtPostalCode.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtPostalCode.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtPostcode
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtCity.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtCity.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtCity.leftViewMode = UITextFieldViewModeAlways;
    txtCity.rightViewMode = UITextFieldViewModeAlways;
    txtCity.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtCity.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtCity
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtState.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtState.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtState.leftViewMode = UITextFieldViewModeAlways;
    txtState.rightViewMode = UITextFieldViewModeAlways;
    txtState.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtState.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtState
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtCountry.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtCountry.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtCountry.leftViewMode = UITextFieldViewModeAlways;
    txtCountry.rightViewMode = UITextFieldViewModeAlways;
    txtCountry.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtCountry.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideTxtCountry
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    btnDateOfBirth.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    viewPhone.layer.borderColor = appTextFieldBoardColor.CGColor;
    viewPlayerNote1.layer.borderColor = appTextFieldBoardColor.CGColor;
    viewPlayerNote2.layer.borderColor = appTextFieldBoardColor.CGColor;
    viewPlayerNote3.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txvPlayerNote1.text = [NSString stringWithFormat:@"%@",provideTxtPlayerNote1];
    txvPlayerNote2.text = [NSString stringWithFormat:@"%@",provideTxtPlayerNote2];
    txvPlayerNote3.text = [NSString stringWithFormat:@"%@",provideTxtPlayerNote3];
    
    txvPlayerNote1.textColor = [UIColor lightGrayColor];
    txvPlayerNote2.textColor = [UIColor lightGrayColor];
    txvPlayerNote3.textColor = [UIColor lightGrayColor];
    
    isSelectProfilePic= NO;
    isClunLogo= NO;
    
    imgProfilePic.hidden = YES;
    imgUploadClubPic.hidden = YES;
    
//    imgFlag.image = [UIImage imageNamed:@"India"];
//
//    [btnCountry setTitle:@"+91" forState:UIControlStateNormal];
//
//    strCountryName = @"India";
//    strCountryDialCode = @"+91";
//    strCountryCode = @"IN";
    
    [self getPlayerProfileInfoServiceCall];
    
    //[self parseCountryJSON:[dictProfile safeObjectForKey:@"ec_dial_code"] countryDialCode:[dictProfile safeObjectForKey:@"country_code"] andNumberType:@""];
}

-(void)getPlayerProfileInfoServiceCall {
    
    if (afPyramidePlayerProfile) {
       [afPyramidePlayerProfile cancelRequest];
       afPyramidePlayerProfile.delegate = self;
       afPyramidePlayerProfile = nil;
    }

    afPyramidePlayerProfile = [[AFNetworkUtility alloc] init];
    afPyramidePlayerProfile.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];
    
    NSMutableDictionary *dictPlayerProfile = [[NSMutableDictionary alloc] init];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                
    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strUserID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:USERID]];
    
    [dictPlayerProfile setValue:strUserID forKey:@"clause[id]"];
    
    NSLog(@"PlayerProfilePARAM ::: %@",dictPlayerProfile);

    [afPyramidePlayerProfile requestWithUrl:GetPlayerInfo_Url param:dictPlayerProfile andToken:strAPIKey];
}

- (IBAction)onClickSelectDOBBtn:(id)sender {
    BirthdayPopUpViewController *viewBDate = [[BirthdayPopUpViewController alloc] initWithNibName:@"BirthdayPopUpViewController" bundle:nil];
    viewBDate.delegate = self;
    viewBDate.strAction = @"Birth";
    [self presentPopupViewController:viewBDate animationType:MJPopupViewAnimationFade];
}

-(void)setBirthdayDetail:(NSString *_Nullable)strBirthDate andSendField:(NSString *_Nonnull)strSendDate atAction:(NSString *)strPickerType {
    [btnDateOfBirth setTitle:strBirthDate forState:UIControlStateNormal];
    strSendBDate = [NSString stringWithFormat:@"%@",strSendDate];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd-MMM-YYYY"];
    NSDate *pickerDate = [[NSDate alloc] init];
    pickerDate = [format dateFromString:strSendBDate];
    
    NSDate *now = [NSDate date];
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:pickerDate
                                       toDate:now
                                       options:0];
    
    txtAge.text = [NSString stringWithFormat:@"%ld",(long)[ageComponents year]];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

- (IBAction)onClickSelectCountryBtn:(id)sender {
        
    CountryListViewController *countryView = [[CountryListViewController alloc] initWithNibName:@"CountryListViewController" delegate:self];
    countryView.delegate = self;
    [self presentPopupViewController:countryView animationType:MJPopupViewAnimationFade];
}

#pragma mark -
#pragma mark - Country_Flag_And_Code_Delegate_Method

- (void)didSelectCountry:(NSDictionary *)country atAction:(NSString *)strAction {
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        
    if ([strAction isEqualToString:@"AddCountry"]) {
        NSLog(@"%@",[country safeObjectForKey:@"code"]);
        NSLog(@"%@",[country safeObjectForKey:@"dial_code"]);
        NSLog(@"%@",[country safeObjectForKey:@"name"]);
        
        strCountryName = [NSString stringWithFormat:@"%@",[country safeObjectForKey:@"name"]];
        strCountryDialCode = [NSString stringWithFormat:@"%@",[country safeObjectForKey:@"dial_code"]];
        strCountryCode = [NSString stringWithFormat:@"%@",[country safeObjectForKey:@"code"]];
        
        imgFlag.image = [UIImage imageNamed:strCountryName];
        
//        NSString *strDisplayCountry = [NSString stringWithFormat:@"%@ (%@)",strCountryName,strCountryDialCode];
        [btnCountry setTitle:strCountryDialCode forState:UIControlStateNormal];
    }
}

-(void)setCountryCodeBasePhoneNumber:(NSString *)strPhoneCode {
    
//    imgPhoneNumberCorrect.hidden = NO;
    
    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
    
    NBPhoneNumber *phoneNumberCountry = [phoneUtil parse:txtMobileNumber.text defaultRegion:strPhoneCode error:nil];
    NSString *strPhoneFormate = [NSString stringWithFormat:@"%@",[phoneUtil format:phoneNumberCountry numberFormat:NBEPhoneNumberFormatNATIONAL error:nil]];
    
    NSLog(@"- isValidNumber [%@]", [phoneUtil isValidNumber:phoneNumberCountry] ? @"YES" : @"NO");
    strPhoneNumberValid = [NSString stringWithFormat:@"%@",[phoneUtil isValidNumber:phoneNumberCountry] ? @"YES" : @"NO"];
    
    if ([[phoneUtil isValidNumber:phoneNumberCountry] ? @"YES" : @"NO" isEqualToString:@"YES"]) {
        //imgPhoneNumberCorrect.image = [UIImage imageNamed:@"CorrectNumber"];
    }else{
        //imgPhoneNumberCorrect.image = [UIImage imageNamed:@"wrongBtn"];
    }
    
    if (![strPhoneFormate isEqualToString:@"(null)"]) {
        txtMobileNumber.text = strPhoneFormate;
    }
}

- (void)parseCountryJSON:(NSString *)strCountryCode countryDialCode:(NSString *)strDialCode andNumberType:(NSString *)strType{
    
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    NSMutableArray *arrCountry = [[NSMutableArray alloc] init];
    arrCountry = (NSMutableArray *)parsedObject;
    
    for (NSDictionary *dict in arrCountry) {
        if ([strDialCode isEqualToString:[dict safeObjectForKey:@"dial_code"]]) {
                                            
            strCountryCode = [NSString stringWithFormat:@"%@",[dict safeObjectForKey:@"code"]];
            strCountryDialCode = [NSString stringWithFormat:@"%@",[dict safeObjectForKey:@"dial_code"]];
            strCountryName = [NSString stringWithFormat:@"%@",[dict safeObjectForKey:@"name"]];
            
//            NSString *strCTitle = [NSString stringWithFormat:@"%@ %@",strEmgCountryDialCode,strEmgCountryCode];
            
            imgFlag.image = [UIImage imageNamed:[dict objectForKey:@"name"]];
            [btnCountry setTitle:strCountryDialCode forState:UIControlStateNormal];
            
            if (txtMobileNumber.text.length > 0) {
                [self setCountryCodeBasePhoneNumber:strCountryCode];
            }
        }
    }
}

- (BOOL)textViewShouldReturn:(UITextView *)textField {
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textField {

    if (textField.tag == 0) {
        if ([txvPlayerNote1.text isEqualToString:provideTxtPlayerNote1]) {
           txvPlayerNote1.text = @"";
        }
    }else if (textField.tag == 1) {
        if ([txvPlayerNote2.text isEqualToString:provideTxtPlayerNote2]) {
           txvPlayerNote2.text = @"";
        }
    }else if (textField.tag == 2) {
        if ([txvPlayerNote3.text isEqualToString:provideTxtPlayerNote3]) {
           txvPlayerNote3.text = @"";
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textField {

    if (textField.tag == 0) {
        if ([[CommonClass trimString:txvPlayerNote1.text] isEqualToString:@""]) {
           txvPlayerNote1.text = [NSString stringWithFormat:@"%@",provideTxtPlayerNote1];
        }
    }else if (textField.tag == 1) {
        if ([[CommonClass trimString:txvPlayerNote2.text] isEqualToString:@""]) {
           txvPlayerNote2.text = [NSString stringWithFormat:@"%@",provideTxtPlayerNote2];
        }
    }else if (textField.tag == 2) {
        if ([[CommonClass trimString:txvPlayerNote3.text] isEqualToString:@""]) {
           txvPlayerNote3.text = [NSString stringWithFormat:@"%@",provideTxtPlayerNote3];
        }
    }
}

- (IBAction)onClickProfilePicBtn:(id)sender {
    strIsClubLogo = @"No";
    [self setSelectProfilePicAndIDProof:SELECTPICTURE];
}

- (IBAction)onClickClubLogoBtn:(id)sender {
    strIsClubLogo = @"Yes";
    [self setSelectProfilePicAndIDProof:SELECTPassport];
}

-(void)setSelectProfilePicAndIDProof:(NSString *)strAction {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:strAction
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *takePhotoAction = [UIAlertAction
                                      actionWithTitle:TAKEPHOTO
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          [self TakePhotoIntoCamera];
                                          [alertController dismissViewControllerAnimated:YES completion:nil];
                                      }];
    
    UIAlertAction *galleryPhotoAction = [UIAlertAction
                                         actionWithTitle:PHOTOGALLERY
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action) {
                                             [self TakePhotoIntoGallary];
                                             [alertController dismissViewControllerAnimated:YES completion:nil];
                                         }];
    
    UIAlertAction *cancelAction  = [UIAlertAction
                                    actionWithTitle:provideCancel
                                    style:UIAlertActionStyleCancel
                                    handler:^(UIAlertAction * action) {
                                        [alertController dismissViewControllerAnimated:YES completion:nil];
                                    }];
    
    [alertController addAction:takePhotoAction];
    [alertController addAction:galleryPhotoAction];
    [alertController addAction:cancelAction];
    
    alertController.popoverPresentationController.sourceView = self.view;
        
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)TakePhotoIntoCamera {
    NSLog(@"TakePhotoIntoCamera");
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *cameraMsg = [UIAlertController
                                        alertControllerWithTitle:provideAlert
                                        message:provideDeviceCamera
                                        preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [cameraMsg addAction:okAction];
        [self presentViewController: cameraMsg animated:YES completion:nil];
        
    }else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

-(void)TakePhotoIntoGallary {
    NSLog(@"TakePhotoIntoGallary");
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
        
    if ([strIsClubLogo isEqualToString:@"No"]) {
        
        imgProfilePic.hidden = NO;
        asyImgPlayerPic.hidden = YES;
                
        isSelectProfilePic = YES;
        
        chosenProImage = info[UIImagePickerControllerOriginalImage];
        imgUserPic = chosenProImage;
        
        NSString *strRemdomName = [SHARED_APPDELEGATE getfileUniqueNameWithextention:@"png"];
        
        strProfilePic = [SHARED_APPDELEGATE getNutritionImagePathwithImageName:strRemdomName];
        imageData = UIImageJPEGRepresentation(chosenProImage ,1.0);
        [imageData writeToFile:strProfilePic atomically:YES];
    }else {
        isClunLogo = YES;
        
        imgUploadClubPic.hidden = NO;
        asyImgClubLogo.hidden = YES;
        
        chosenIDProofImage = info[UIImagePickerControllerOriginalImage];
        imgIDProoofPic = chosenIDProofImage;
        
        NSString *strRemdomName = [SHARED_APPDELEGATE getfileUniqueNameWithextention:@"png"];
        
        strIDProofPic = [SHARED_APPDELEGATE getNutritionImagePathwithImageName:strRemdomName];
        imageIDProofData = UIImageJPEGRepresentation(chosenIDProofImage ,1.0);
        [imageIDProofData writeToFile:strIDProofPic atomically:YES];
    }
        
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    [self performSelector:@selector(getImageIntoDocumentDirectery) withObject:nil afterDelay:0.0];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    if ([strIsClubLogo isEqualToString:@"No"]) {
        isSelectProfilePic = NO;
    }else {
        isClunLogo = NO;
    }

    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark -
#pragma mark - IMAGE_DOCUMENT_DIRECTERT_PATH_METHOD

-(void)getImageIntoDocumentDirectery {
    
    if ([strIsClubLogo isEqualToString:@"No"]) {
        [imageData writeToFile:strProfilePic atomically:YES];
        [imgProfilePic setImage:[UIImage imageWithContentsOfFile:strProfilePic]];
        
        imgProfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
        imgProfilePic.layer.borderWidth = 1.0f;
    }else {
        [imageIDProofData writeToFile:strIDProofPic atomically:YES];
        [imgUploadClubPic setImage:[UIImage imageWithContentsOfFile:strIDProofPic]];
        
        imgUploadClubPic.layer.borderColor = [UIColor whiteColor].CGColor;
        imgUploadClubPic.layer.borderWidth = 1.0f;
    }
}

- (void)removeImage:(NSString *)filename {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        //UIAlertView *removedSuccessFullyAlert = [[UIAlertView alloc] initWithTitle:[provideAlert localize] message:provideImageRemove delegate:self cancelButtonTitle:[provideOk localize] otherButtonTitles:nil];
        //[removedSuccessFullyAlert show];
    }else{
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}
#pragma mark -
#pragma mark - Validation_Update_User_Info

-(BOOL)PyramideUpdateValidation {
    
    NSString *strName  = [CommonClass trimString:txtName.text];
    NSString *strSurname  = [CommonClass trimString:txtSurname.text];
    NSString *strMobile  = [CommonClass trimString:txtMobileNumber.text];
    NSString *strEmail  = [CommonClass trimString:txtEmailID.text];
    NSString *strAddress  = [CommonClass trimString:txtAddress.text];
    NSString *strAge  = [CommonClass trimString:txtAge.text];
    NSString *strPostalCode  = [CommonClass trimString:txtPostalCode.text];
    NSString *strCity  = [CommonClass trimString:txtCity.text];
    NSString *strState  = [CommonClass trimString:txtState.text];
    NSString *strCountry  = [CommonClass trimString:txtCountry.text];


    if ([strName length] == 0) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideName delegate:self];
        return NO;
    }
    
    if ([strSurname length] == 0) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideSurname delegate:self];
        return NO;
    }
    
    if ([strMobile length] == 0) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideMobileNumber delegate:self];
        return NO;
    }
    
    if ([strEmail length] == 0) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideEmailID delegate:self];
        return NO;
    }
    if(![CommonClass textIsValidEmailFormat:[CommonClass trimString:strEmail]]) {
        [CommonClass showAlertWithTitle:provideAlert andMessage:provideValidEmail delegate:self];
        return NO;
    }
    
    if ([strAddress length] == 0) {
       [CommonClass showAlertWithTitle:provideAlert andMessage:provideAddress delegate:self];
       return NO;
    }
    
    if ([btnDateOfBirth.titleLabel.text isEqualToString:@""]) {
       [CommonClass showAlertWithTitle:provideAlert andMessage:provideBirthday delegate:self];
       return NO;
    }
    
    if ([strAge length] == 0) {
       [CommonClass showAlertWithTitle:provideAlert andMessage:provideAge delegate:self];
       return NO;
    }
    
    if ([strPostalCode length] == 0) {
       [CommonClass showAlertWithTitle:provideAlert andMessage:providePostalCode delegate:self];
       return NO;
    }
    
    if ([strCity length] == 0) {
       [CommonClass showAlertWithTitle:provideAlert andMessage:provideCity delegate:self];
       return NO;
    }
    
    if ([strState length] == 0) {
       [CommonClass showAlertWithTitle:provideAlert andMessage:provideState delegate:self];
       return NO;
    }
    
    if ([strCountry length] == 0) {
       [CommonClass showAlertWithTitle:provideAlert andMessage:provideCountry delegate:self];
       return NO;
    }

    return  YES;
}

- (IBAction)onClickUpdateProfileBtn:(id)sender {
    
    if ([self PyramideUpdateValidation]) {
        
        if (afPyramidePlayerProfileUpdate) {
           [afPyramidePlayerProfileUpdate cancelRequest];
           afPyramidePlayerProfileUpdate.delegate = self;
           afPyramidePlayerProfileUpdate = nil;
        }

        afPyramidePlayerProfileUpdate = [[AFNetworkUtility alloc] init];
        afPyramidePlayerProfileUpdate.delegate = self;

        [SHARED_APPDELEGATE showLoadingView];
        
        NSMutableDictionary *dictPlayerProfileUpdate = [[NSMutableDictionary alloc] init];
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                    
        NSString *strUserID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:USERID]];
        NSString *strUserType = [NSString stringWithFormat:@"%@",[userDefault objectForKey:USERTYPE]];
        
        [dictPlayerProfileUpdate setValue:strUserID forKey:@"existing_row_id"];
        [dictPlayerProfileUpdate setValue:@"1" forKey:@"totalPlayers"];
        [dictPlayerProfileUpdate setValue:strUserType forKey:@"person_type"];
        [dictPlayerProfileUpdate setValue:[CommonClass trimString:txtName.text]forKey:@"firstname"];
        [dictPlayerProfileUpdate setValue:[CommonClass trimString:txtSurname.text]forKey:@"lastname"];
        [dictPlayerProfileUpdate setValue:[CommonClass trimString:txtMobileNumber.text]forKey:@"mobile"];
        [dictPlayerProfileUpdate setValue:[CommonClass trimString:txtAddress.text]forKey:@"address"];
        [dictPlayerProfileUpdate setValue:[CommonClass trimString:txtPostalCode.text]forKey:@"postcode"];
        [dictPlayerProfileUpdate setValue:[CommonClass trimString:txtCity.text]forKey:@"city"];
        [dictPlayerProfileUpdate setValue:[CommonClass trimString:txtCountry.text]forKey:@"country"];
        
        if (![txvPlayerNote1.text isEqualToString:provideTxtPlayerNote1]) {
            [dictPlayerProfileUpdate setValue:[CommonClass trimString:txvPlayerNote1.text]forKey:@"player_note1"];
        }else {
            [dictPlayerProfileUpdate setValue:@""forKey:@"player_note1"];
        }
        
        if (![txvPlayerNote2.text isEqualToString:provideTxtPlayerNote2]) {
            [dictPlayerProfileUpdate setValue:[CommonClass trimString:txvPlayerNote2.text]forKey:@"player_note2"];
        }else {
            [dictPlayerProfileUpdate setValue:@""forKey:@"player_note2"];
        }
        
        if (![txvPlayerNote2.text isEqualToString:provideTxtPlayerNote2]) {
            [dictPlayerProfileUpdate setValue:[CommonClass trimString:txvPlayerNote3.text]forKey:@"player_note3"];
        }else {
            [dictPlayerProfileUpdate setValue:@""forKey:@"player_note3"];
        }
        
        [dictPlayerProfileUpdate setValue:[CommonClass trimString:txtState.text]forKey:@"state"];
        [dictPlayerProfileUpdate setValue:[CommonClass trimString:txtAge.text]forKey:@"age"];
        [dictPlayerProfileUpdate setValue:strSendBDate forKey:@"dob"];
        [dictPlayerProfileUpdate setValue:btnCountry.titleLabel.text forKey:@"country_code"];
         
        NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
        
        NSLog(@"PlayerProfilePARAM ::: %@",dictPlayerProfileUpdate);

//        NSDictionary *headers = @{
//          @"api-key": strAPIKey,
//        };
//
//        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//
//        [request setAllHTTPHeaderFields:headers];
//
//        request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:UpdateUserInfo_Url parameters:dictPlayerProfileUpdate constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//
//            //[request setAllHTTPHeaderFields:headers];
//
//
////            NSData *imageProData = UIImageJPEGRepresentation(chosenProImage, 0.8);
////            NSData *imageIDProofData = UIImageJPEGRepresentation(uploadIDProofImg, 0.8);
//
//            [formData appendPartWithFileURL:[NSURL fileURLWithPath:self->strProfilePic] name:@"profile_pic" fileName:[self->strProfilePic lastPathComponent] mimeType:@"image/jpeg" error:nil];
//            [formData appendPartWithFileURL:[NSURL fileURLWithPath:self->strIDProofPic] name:@"club_logo" fileName:[self->strIDProofPic lastPathComponent] mimeType:@"image/jpeg" error:nil];
//            } error:nil];
//
//        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//
//        NSURLSessionUploadTask *uploadTask;
//        uploadTask = [manager
//                      uploadTaskWithStreamedRequest:request
//                      progress:^(NSProgress * _Nonnull uploadProgress) {
//                          // This is not called back on the main queue.
//                          // You are responsible for dispatching to the main queue for UI updates
//                          dispatch_async(dispatch_get_main_queue(), ^{
//                              //Update the progress view
////                              [progressView setProgress:uploadProgress.fractionCompleted];
//                          });
//                      }
//                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//                          if (error) {
//                              NSLog(@"Error: %@", error);
//                          } else {
//                              NSLog(@"%@ %@", response, responseObject);
//                          }
//                      }];
//
//        [uploadTask resume];
        
        if (isSelectProfilePic == YES && isClunLogo == YES) {
//            [afPyramidePlayerProfileUpdate requestWithUrl123:UpdateUserInfo_Url param:dictPlayerProfileUpdate withProImage:strProfilePic imageProName:[strProfilePic lastPathComponent] imgParamProName:@"profile_pic" andIDProofImage:strIDProofPic imageIDProofName:[strIDProofPic lastPathComponent] imgParamIDProofName:@"club_logo" strKeyToken:strAPIKey];
//
            [afPyramidePlayerProfileUpdate requestWithUrl:UpdateUserInfo_Url param:dictPlayerProfileUpdate withProImage:chosenProImage imageProName:[strProfilePic lastPathComponent] imgParamProName:@"profile_pic" andIDProofImage:chosenIDProofImage imageIDProofName:[strIDProofPic lastPathComponent] imgParamIDProofName:@"club_logo" strKeyToken:strAPIKey];
        }else if (isSelectProfilePic == YES && isClunLogo == NO) {
            [dictPlayerProfileUpdate setValue:strClubPicUrl forKey:@"club_logo"];
            [afPyramidePlayerProfileUpdate requestWithUrl:UpdateUserInfo_Url param:dictPlayerProfileUpdate withProImage:chosenProImage imageProName:[strProfilePic lastPathComponent] imgParamProName:@"profile_picture" andToken:strAPIKey];
        }else if (isSelectProfilePic == NO && isClunLogo == YES) {
            [dictPlayerProfileUpdate setValue:strProfilePicUrl forKey:@"profile_pic"];
            [afPyramidePlayerProfileUpdate requestWithUrl:UpdateUserInfo_Url param:dictPlayerProfileUpdate withProImage:chosenIDProofImage imageProName:[strIDProofPic lastPathComponent] imgParamProName:@"club_logo" andToken:strAPIKey];
        }
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataSuccess:(NSMutableDictionary *)dictResponce {
        
    if (utility == afPyramidePlayerProfile) {
        
        NSLog(@"PLAYERINFORESPONCE ::: %@", dictResponce);
        
        [SHARED_APPDELEGATE hideLoadingView];
                        
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            
            NSArray *arrData = [dictResponce safeObjectForKey:@"data"];
            
            if ([arrData count] > 0) {
                
                NSDictionary *dictPlayerInfo = [arrData firstObject];
                
                txtName.text = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"firstname"]];
                txtSurname.text = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"lastname"]];
                txtMobileNumber.text = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"mobile"]];
                txtEmailID.text = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"email"]];
                txtAddress.text = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"address"]];
                txtAge.text = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"age"]];
                txtPostalCode.text = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"postcode"]];
                txtCity.text = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"city"]];
                txtState.text = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"state"]];
                txtCountry.text = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"country"]];
                txvPlayerNote1.text = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@""]];
                txvPlayerNote2.text = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@""]];
                txvPlayerNote3.text = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@""]];
                
                strCountryCode = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"country_code"]];
                
                [btnDateOfBirth setTitle:[NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"dob"]] forState:UIControlStateNormal];
                
                NSString *strDateBirth = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"dob"]];
                
                if (![strDateBirth isEqualToString:@""]) {
                    NSDateFormatter *format = [[NSDateFormatter alloc] init];
                    [format setDateFormat:@"yyyy-MM-dd"];
                    NSDate *date = [format dateFromString:strDateBirth];
                    [format setDateFormat:@"dd-MMM-YYYY"];
                    strSendBDate = [NSString stringWithFormat:@"%@",[format stringFromDate:date]];
                }else {
                    strSendBDate = @"01-Jun-1979";
                }
            
                [btnCountry setTitle:strCountryCode forState:UIControlStateNormal];
                
                NSString *strTxvPNote1 = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"player_note1"]];
                NSString *strTxvPNote2 = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"player_note2"]];
                NSString *strTxvPNote3 = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"player_note3"]];
                
                if ([strTxvPNote1 isEqualToString:@""]) {
                    txvPlayerNote1.text = @"Player Note 1";
                }else {
                    txvPlayerNote1.text = strTxvPNote1;
                }
                
                if ([strTxvPNote2 isEqualToString:@""]) {
                    txvPlayerNote2.text = @"Player Note 2";
                }else {
                    txvPlayerNote2.text = strTxvPNote2;
                }
                
                if ([strTxvPNote3 isEqualToString:@""]) {
                    txvPlayerNote3.text = @"Player Note 3";
                }else {
                    txvPlayerNote3.text = strTxvPNote3;
                }
                            
                strProfilePicUrl = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"profile"]];

                if (strProfilePicUrl.length > 0) {
                   [asyImgPlayerPic loadingImage:strProfilePicUrl placeHolderImage:USER_PLACEHOLDER_IMAGE];
                }else {
                   [asyImgPlayerPic loadingImage:nil placeHolderImage:USER_PLACEHOLDER_IMAGE];
                }
                
                strClubPicUrl = [NSString stringWithFormat:@"%@",[dictPlayerInfo safeObjectForKey:@"club_logo"]];

                if (strClubPicUrl.length > 0) {
                   [asyImgClubLogo loadingImage:strClubPicUrl placeHolderImage:USER_PLACEHOLDER_IMAGE];
                }else {
                   [asyImgClubLogo loadingImage:nil placeHolderImage:USER_PLACEHOLDER_IMAGE];
                }
                
                [self parseCountryJSON:@"" countryDialCode:strCountryCode andNumberType:@""];
//                [self setCountryCodeBasePhoneNumber:strCountryCode];
            }
            
        }else {
            [CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
        }
    }else if (utility == afPyramidePlayerProfileUpdate) {
        
        NSLog(@"UPDATEPROFILERESPONCE ::: %@", dictResponce);
        
        [SHARED_APPDELEGATE hideLoadingView];
                        
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            
            
        }else {
            [CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
        }
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Fail_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataFail:(NSError *)error {
    [SHARED_APPDELEGATE hideLoadingView];
    NSString *strError = [NSString stringWithFormat:@"%ld",(long)error.code];
    [CommonClass showAlertWithTitle:provideAlert andMessage:strError delegate:self];
}

@end
