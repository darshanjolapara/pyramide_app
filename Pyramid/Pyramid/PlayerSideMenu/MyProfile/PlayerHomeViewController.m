//
//  PlayerHomeViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "PlayerHomeViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"

@interface PlayerHomeViewController ()

@end

@implementation PlayerHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
        
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.navigationItem.titleView = [SHARED_APPDELEGATE getNavigationCenterWithTitle:@"Home" fontSize:18];
    
    btnSelectCompetition.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    viewSelectDetail.hidden = NO;
    viewSelectRatting.hidden = YES;
    viewSelectNews.hidden = YES;
    viewSelectGamePage.hidden = YES;
    viewSelectStaticstic.hidden = YES;
    
    isRanking = NO;
    isNews = NO;
    isGamePage = NO;
    isStaticstic = NO;
    
    constHeightSearchView.constant = 0.0f;
    
    [btnDetail setTitleColor:appNavBarColor forState:UIControlStateNormal];
    
    arrDetailList = [[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"",@"",@"", nil];
    
    txtSearchPlayer.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:@"Search for player name.."
                attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

- (IBAction)onClickSelectCompetitionBtn:(id)sender {
    
    arrCompetitionList = [[NSMutableArray alloc] initWithObjects:@"test 2",@"Odin",@"New Competition",@"Piramida TK Skibini", nil];

    if (dropDown == nil) {
        
        CGFloat f = 0;
        if ([arrCompetitionList count] <= 5) {
            f = [arrCompetitionList count] * 40;
        }else {
            f = 200;
        }
       dropDown = [[NIDropDown alloc] showDropDown:sender :&f :arrCompetitionList :nil :@"down"];
       dropDown.delegate = self;
    }else{
       [dropDown hideDropDown:sender];
       [self rel];
    }
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    NSLog(@"LanguageName: %@",btnSelectCompetition.titleLabel.text);
    [self rel];
}

-(void)rel {
    dropDown = nil;
}

- (IBAction)onClickDetailBtn:(id)sender {
    
    isRanking = NO;
    isNews = NO;
    isGamePage = NO;
    isStaticstic = NO;
    
    viewSelectDetail.hidden = NO;
    viewSelectRatting.hidden = YES;
    viewSelectNews.hidden = YES;
    viewSelectGamePage.hidden = YES;
    viewSelectStaticstic.hidden = YES;
    viewDisplayBtn.hidden = NO;
    
    [btnDetail setTitleColor:appNavBarColor forState:UIControlStateNormal];
    [btnRatting setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnNews setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnGamePage setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnStaticstic setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [btnMoreDetail setTitle:@"More Details" forState:UIControlStateNormal];
    [btnViewChallangers setTitle:@"View Challangers" forState:UIControlStateNormal];
    
    btnAllGames.hidden = YES;
    btnMoreDetail.hidden = NO;
    btnViewChallangers.hidden = NO;
    
    constHeightBtnView.constant = 60.0f;
    constHeightSearchView.constant = 0.0f;
    
    [tblDetailList reloadData];
}

- (IBAction)onClickRattingBtn:(id)sender {
    
    isRanking = YES;
    isNews = NO;
    isGamePage = NO;
    isStaticstic = NO;
    
    viewSelectDetail.hidden = YES;
    viewSelectRatting.hidden = NO;
    viewSelectNews.hidden = YES;
    viewSelectGamePage.hidden = YES;
    viewSelectStaticstic.hidden = YES;
    viewDisplayBtn.hidden = NO;
    
    [btnDetail setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnRatting setTitleColor:appNavBarColor forState:UIControlStateNormal];
    [btnNews setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnGamePage setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnStaticstic setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [btnMoreDetail setTitle:@"Last Challanges" forState:UIControlStateNormal];
    [btnViewChallangers setTitle:@"Last Matches" forState:UIControlStateNormal];
    
    btnAllGames.hidden = YES;
    btnMoreDetail.hidden = NO;
    btnViewChallangers.hidden = NO;
    
    constHeightBtnView.constant = 60.0f;
    constHeightSearchView.constant = 0.0f;
    
    [tblDetailList reloadData];
}

- (IBAction)onClickNewsBtn:(id)sender {
    
    isRanking = NO;
    isNews = YES;
    isGamePage = NO;
    isStaticstic = NO;
    
    viewSelectDetail.hidden = YES;
    viewSelectRatting.hidden = YES;
    viewSelectNews.hidden = NO;
    viewSelectGamePage.hidden = YES;
    viewSelectStaticstic.hidden = YES;
    
    viewDisplayBtn.hidden = YES;
    
    [btnDetail setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnRatting setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnNews setTitleColor:appNavBarColor forState:UIControlStateNormal];
    [btnGamePage setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnStaticstic setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    constHeightBtnView.constant = 0.0f;
    constHeightSearchView.constant = 0.0f;
    
    tblDetailList.frame = CGRectMake(0, 0, SHARED_APPDELEGATE.window.frame.size.width, SHARED_APPDELEGATE.window.frame.size.height - 110);
    
    [tblDetailList reloadData];
}

- (IBAction)onClickGamePageBtn:(id)sender {
    
    isRanking = NO;
    isNews = NO;
    isGamePage = YES;
    isStaticstic = NO;
    
    btnAllGames.hidden = NO;
    btnMoreDetail.hidden = YES;
    btnViewChallangers.hidden = YES;
    
    viewSelectDetail.hidden = YES;
    viewSelectRatting.hidden = YES;
    viewSelectNews.hidden = YES;
    viewSelectGamePage.hidden = NO;
    viewSelectStaticstic.hidden = YES;
    
    [btnDetail setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnRatting setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnNews setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnGamePage setTitleColor:appNavBarColor forState:UIControlStateNormal];
    [btnStaticstic setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
    constHeightBtnView.constant = 60.0f;
    constHeightSearchView.constant = 0.0f;
    
    [tblDetailList reloadData];
}

- (IBAction)onClickStaticsticBtn:(id)sender {
    
    isRanking = NO;
    isNews = NO;
    isGamePage = NO;
    isStaticstic = YES;
        
    viewSelectDetail.hidden = YES;
    viewSelectRatting.hidden = YES;
    viewSelectNews.hidden = YES;
    viewSelectGamePage.hidden = YES;
    viewSelectStaticstic.hidden = NO;
    
    [btnDetail setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnRatting setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnNews setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnGamePage setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnStaticstic setTitleColor:appNavBarColor forState:UIControlStateNormal];
    
    constHeightBtnView.constant = 0.0f;
    constHeightSearchView.constant = 40.0f;

    [tblDetailList reloadData];
}

#pragma mark -
#pragma mark - TableView Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (isRanking == YES || isNews == YES || isGamePage == YES || isStaticstic == YES) {
        return 0.0f;
    }else {
        return 155.0f;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (isRanking == YES || isNews == YES || isGamePage == YES || isStaticstic == YES) {
        return nil;
    }else {
        return viewHeader;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (isRanking == YES) {
        return 58.0f;
    }else if (isNews == YES) {
        NewsCell *cell = (NewsCell *)[self tableView:tblDetailList cellForRowAtIndexPath:indexPath];
        return [cell getCellHeight];
    }else if (isGamePage == YES) {
        GamePageCell *cell = (GamePageCell *)[self tableView:tblDetailList cellForRowAtIndexPath:indexPath];
        return [cell getCellHeight];
    }else if (isStaticstic == YES) {
        return 50.0f;
    }else {
        DetailCellTableViewCell *cell = (DetailCellTableViewCell *)[self tableView:tblDetailList cellForRowAtIndexPath:indexPath];
        return [cell getCellHeight];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (isRanking == YES) {
        return  [arrDetailList count];
    }else if (isNews == YES) {
        return  [arrDetailList count];
    }else if (isGamePage == YES) {
        return  [arrDetailList count];
    }else if (isStaticstic == YES) {
        return  [arrDetailList count];
    }else {
        return  [arrDetailList count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (isRanking == YES) {
        static NSString *simpleTableIdentifier = @"RankingCell";
        
        RankingCell  *cell = (RankingCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RankingCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.selectionStyle  = UITableViewCellSelectionStyleNone;
        
//        cell.delegate = self;
//        cell.index = indexPath.row;
//
//        if (indexPath.row == 2) {
//            cell.btnEditStatus.hidden = NO;
//        }
        
        return cell;
    }else if (isNews == YES) {
        static NSString *simpleTableIdentifier = @"NewsCell";

        NewsCell  *cell = (NewsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NewsCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }

        cell.selectionStyle  = UITableViewCellSelectionStyleNone;

        //        cell.delegate = self;
        //        cell.index = indexPath.row;
        //
        //        if (indexPath.row == 2) {
        //            cell.btnEditStatus.hidden = NO;
        //        }

        return cell;
    }else if (isGamePage == YES) {
        static NSString *simpleTableIdentifier = @"GamePageCell";

        GamePageCell  *cell = (GamePageCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GamePageCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }

        cell.selectionStyle  = UITableViewCellSelectionStyleNone;

        //        cell.delegate = self;
        //        cell.index = indexPath.row;
        //
        //        if (indexPath.row == 2) {
        //            cell.btnEditStatus.hidden = NO;
        //        }

        return cell;
    }else if (isStaticstic == YES) {
        static NSString *simpleTableIdentifier = @"StaticsticCell";

        StaticsticCell  *cell = (StaticsticCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StaticsticCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }

        cell.selectionStyle  = UITableViewCellSelectionStyleNone;

        cell.delegate = self;
        cell.index = indexPath.row;

        return cell;
    }else {
        static NSString *simpleTableIdentifier = @"DetailCellTableViewCell";
        
        DetailCellTableViewCell  *cell = (DetailCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DetailCellTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.selectionStyle  = UITableViewCellSelectionStyleNone;
        
        cell.delegate = self;
        cell.index = indexPath.row;
        
        if (indexPath.row == 2) {
            cell.btnEditStatus.hidden = NO;
        }
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (isRanking == YES) {
        PlayerDetailPopUpViewController *viewPlayerDetail = [[PlayerDetailPopUpViewController alloc] initWithNibName:@"PlayerDetailPopUpViewController" bundle:nil];
        viewPlayerDetail.delegate = self;
        [self presentPopupViewController:viewPlayerDetail animationType:MJPopupViewAnimationFade];
    }else if (isNews == YES) {
        
    }
}

#pragma mark -
#pragma mark - Change Status Delegate Method

-(void)setChangeStatusAtIndex:(NSInteger)index {
    
    ChangeStatusPopUpViewController *viewChangeStatus = [[ChangeStatusPopUpViewController alloc] initWithNibName:@"ChangeStatusPopUpViewController" bundle:nil];
    viewChangeStatus.delegate = self;
    [self presentPopupViewController:viewChangeStatus animationType:MJPopupViewAnimationFade];
}

-(void)selectStatus:(NSString *)strStatus AndAction:(NSString *)strType {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)setPlayerDetailInfoAction:(NSString *)strAction {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)setStaticsticInfoAtIndex:(NSInteger)index {
    StaticsticDetailViewController *viewStaticsticDetail = [[StaticsticDetailViewController alloc] initWithNibName:@"StaticsticDetailViewController" bundle:nil];
    [self.navigationController pushViewController:viewStaticsticDetail animated:YES];
}

- (IBAction)onClickMoreDetailBtn:(id)sender {
    
    if (isRanking == YES) {
        
    }else {
        if ([btnMoreDetail.titleLabel.text isEqualToString:@"More Details"]) {
            [btnMoreDetail setTitle:@"Hide Details" forState:UIControlStateNormal];
            arrDetailList = [[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"",@"",@"",@"",@"",@"",@"", nil];
        }else {
            [btnMoreDetail setTitle:@"More Details" forState:UIControlStateNormal];
            arrDetailList = [[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"",@"",@"", nil];
        }
        
        [tblDetailList reloadData];
    }
}

- (IBAction)onClickViewChallangersBtn:(id)sender {
    
}

- (IBAction)onClickAllGamesBtn:(id)sender {

    AllGamesListViewController *viewAllGame = [[AllGamesListViewController alloc] initWithNibName:@"AllGamesListViewController" bundle:nil];
    [self.navigationController pushViewController:viewAllGame animated:YES];
}

@end
