//
//  MyProfileViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "MasterViewController.h"
#import "CountryListViewController.h"
#import "BirthdayPopUpViewController.h"
#import "AsyImage.h"
#import "AFNetworkUtility.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyProfileViewController : MasterViewController <BirthdayPopUpViewControllerDelegate,CountryListViewDelegate,AFNetworkUtilityDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate> {
    
    IBOutlet UIView *viewPhone;
    IBOutlet UIView *viewPlayerNote1;
    IBOutlet UIView *viewPlayerNote2;
    IBOutlet UIView *viewPlayerNote3;
    
    IBOutlet UITextField *txtName;
    IBOutlet UITextField *txtSurname;
    IBOutlet UITextField *txtMobileNumber;
    IBOutlet UITextField *txtEmailID;
    IBOutlet UITextField *txtAddress;
    IBOutlet UITextField *txtAge;
    IBOutlet UITextField *txtPostalCode;
    IBOutlet UITextField *txtCity;
    IBOutlet UITextField *txtState;
    IBOutlet UITextField *txtCountry;
    
    IBOutlet UITextView *txvPlayerNote1;
    IBOutlet UITextView *txvPlayerNote2;
    IBOutlet UITextView *txvPlayerNote3;
    
    IBOutlet UIButton *btnCountry;
    IBOutlet UIButton *btnDateOfBirth;
    
    IBOutlet UIImageView *imgFlag;
    
    IBOutlet AsyImage *asyImgPlayerPic;
    IBOutlet AsyImage *asyImgClubLogo;
    
    IBOutlet UIImageView *imgProfilePic;
    IBOutlet UIImageView *imgUploadClubPic;
    
    BOOL isSelectProfilePic;
    BOOL isClunLogo;
    
    NSData *imageData;
    NSData *imageIDProofData;
    
    NSString *strCountryName;
    NSString *strCountryDialCode;
    NSString *strCountryCode;
    NSString *strPhoneNumberValid;
    NSString *strSendBDate;
    NSString *strIsClubLogo;
    UIImage *imgUserPic;
    UIImage *imgIDProoofPic;
    UIImage *chosenProImage;
    UIImage *chosenIDProofImage;
    NSString *strProfilePic;
    NSString *strIDProofPic;
    NSString *strIsIDProof;
    
    NSString *strProfilePicUrl;
    NSString *strClubPicUrl;
    
    AFNetworkUtility *afPyramidePlayerProfile;
    AFNetworkUtility *afPyramidePlayerProfileUpdate;
}

@end

NS_ASSUME_NONNULL_END
