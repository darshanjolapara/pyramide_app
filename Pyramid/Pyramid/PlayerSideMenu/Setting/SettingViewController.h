//
//  SettingViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "MasterViewController.h"
#import "NIDropDown.h"

NS_ASSUME_NONNULL_BEGIN

@interface SettingViewController : MasterViewController <NIDropDownDelegate> {
    
    IBOutlet UIButton *btnSelectLanguage;
    
    NIDropDown *dropDown;
}

@end

NS_ASSUME_NONNULL_END
