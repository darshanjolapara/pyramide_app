//
//  SettingViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "SettingViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"

@interface SettingViewController ()

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.titleView = [SHARED_APPDELEGATE getNavigationCenterWithTitle:NavigationSetting fontSize:18];
    
    btnSelectLanguage.layer.borderColor = appTextFieldBoardColor.CGColor;
}

- (IBAction)onClickSelectLanguageBtn:(id)sender {
    NSMutableArray *arrLanguageList = [[NSMutableArray alloc] initWithObjects:@"English",@"Croatian",@"Slovene",@"Italian", nil];

    if (dropDown == nil) {
       CGFloat f = 160;
       dropDown = [[NIDropDown alloc] showDropDown:sender :&f :arrLanguageList :nil :@"down" atAction:@""];
       dropDown.delegate = self;
    }else{
       [dropDown hideDropDown:sender];
       [self rel];
    }
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender atIndexPath:(NSInteger)index {
    NSLog(@"LanguageName: %@",btnSelectLanguage.titleLabel.text);
    [self rel];
}

-(void)rel {
    dropDown = nil;
}

@end
