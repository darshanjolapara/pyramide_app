//
//  RankingCell.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChallangersList.h"
#import "AsyImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface RankingCell : UITableViewCell {
    
    IBOutlet UIView *viewBack;
    IBOutlet UIView *viewLine;
    
    IBOutlet UILabel *lblPlayerName;
    IBOutlet UILabel *lblPosition;
}

-(void)getRankingPlayerDataList:(ChallangersList *)objRanking;

@end

NS_ASSUME_NONNULL_END
