//
//  LastMatchesListCell.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/10/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LastChallengesList.h"

NS_ASSUME_NONNULL_BEGIN

@protocol LastMatchesListCellDelegate <NSObject>

-(void)setLastMatchAction:(NSString *)strAction atIndex:(NSInteger)index;

@end

@interface LastMatchesListCell : UITableViewCell {
    
    IBOutlet UIView *viewBack;
    
    IBOutlet UILabel *lblMatchDate;
    IBOutlet UILabel *lblCScore;
    IBOutlet UILabel *lblCTScore;
    IBOutlet UILabel *lblCDetailFirstName;
    IBOutlet UILabel *lblCDetailLastName;
    IBOutlet UILabel *lblCToDetailFirstName;
    IBOutlet UILabel *lblCToDetailLastName;
    
    IBOutlet UIButton *btnPicture;
    IBOutlet UIButton *btnVideo;
}

-(void)getLastMatchesDataList:(LastChallengesList*)objLastMatches;

//Delegate
@property (nonatomic , strong) id<LastMatchesListCellDelegate> delegate;

//Integer
@property (nonatomic) NSInteger index;

@end

NS_ASSUME_NONNULL_END
