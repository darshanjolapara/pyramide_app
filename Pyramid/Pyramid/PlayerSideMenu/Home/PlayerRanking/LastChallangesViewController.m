//
//  LastChallangesViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "LastChallangesViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"

@interface LastChallangesViewController ()

@end

@implementation LastChallangesViewController

@synthesize strAction;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.titleView = [SHARED_APPDELEGATE getNavigationCenterWithTitle:NavigationLastChallanges fontSize:18];
    
    UIImage *backButtonImage = [UIImage imageNamed:@"Back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton setImage:backButtonImage
                forState:UIControlStateNormal];
    
    backButton.frame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self
                   action:@selector(popViewController)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;

    arrLastChallanges = [[NSMutableArray alloc] init];
    
    if ([strAction isEqualToString:@"LastChallanges"]) {
        constHeightSearch.constant = 0.0f;
        
        if (afPyramideLastChallanges) {
           [afPyramideLastChallanges cancelRequest];
           afPyramideLastChallanges.delegate = self;
           afPyramideLastChallanges = nil;
        }

        afPyramideLastChallanges = [[AFNetworkUtility alloc] init];
        afPyramideLastChallanges.delegate = self;

        [SHARED_APPDELEGATE showLoadingView];
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        
        NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
        NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];
        NSString *strCompetitionID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:COMPETITIONID]];
        
        NSMutableDictionary *dictLastChallange = [[NSMutableDictionary alloc] init];
        
        [dictLastChallange setValue:strCompetitionID forKey:@"competition_id"];
        [dictLastChallange setValue:@"" forKey:@"find"];
        
        if ([strSelectLang isEqualToString:@""]) {
            [dictLastChallange setValue:@"english" forKey:@"lang"];
        }else {
            [dictLastChallange setValue:strSelectLang forKey:@"lang"];
        }
        
        NSLog(@"LastChallengesPARAM ::: %@",dictLastChallange);

        [afPyramideLastChallanges requestWithUrl:GetMyChallenges_Url param:dictLastChallange andToken:strAPIKey];
    }else {
        [self getLastMatechesDataServiceCall:@""];
    }
}

#pragma mark -
#pragma mark - Pop To View Mothod

-(void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getLastMatechesDataServiceCall:(NSString *)strFind {
    
    txtSearchPlayer.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:@"Search matches here.."
                attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    constHeightSearch.constant = 50.0f;
    
    if (afPyramideLastmatches) {
       [afPyramideLastmatches cancelRequest];
       afPyramideLastmatches.delegate = self;
       afPyramideLastmatches = nil;
    }

    afPyramideLastmatches = [[AFNetworkUtility alloc] init];
    afPyramideLastmatches.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];
    NSString *strCompetitionID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:COMPETITIONID]];
    
    NSMutableDictionary *dictLastMatches = [[NSMutableDictionary alloc] init];
    
    [dictLastMatches setValue:strCompetitionID forKey:@"competition_id"];
    [dictLastMatches setValue:strFind forKey:@"find"];
    [dictLastMatches setValue:@"1" forKey:@"played_challenges"];
    
    if ([strSelectLang isEqualToString:@""]) {
        [dictLastMatches setValue:@"english" forKey:@"lang"];
    }else {
        [dictLastMatches setValue:strSelectLang forKey:@"lang"];
    }
    
    NSLog(@"LastMatchesPARAM ::: %@",dictLastMatches);

    [afPyramideLastmatches requestWithUrl:GetMatchChallenges_Url param:dictLastMatches andToken:strAPIKey];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if ([[CommonClass trimString:txtSearchPlayer.text] length] != 0) {
        [arrLastChallanges removeAllObjects];
        arrLastChallanges = [[NSMutableArray alloc] init];
        [self getLastMatechesDataServiceCall:txtSearchPlayer.text];
    }
    
    return YES;
}

#pragma mark -
#pragma mark - TableView Delegate Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([strAction isEqualToString:@"LastChallanges"]) {
        return 65.0f;
    }else {
        return 107.0f;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrLastChallanges count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([strAction isEqualToString:@"LastChallanges"]) {
        static NSString *simpleTableIdentifier = @"LastChallangesListCell";

        LastChallangesListCell  *cell = (LastChallangesListCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LastChallangesListCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }

        cell.selectionStyle  = UITableViewCellSelectionStyleNone;

        if ([arrLastChallanges count] > 0) {
            LastChallengesList *objLastChalleng = [arrLastChallanges objectAtIndex:indexPath.row];
            [cell getLastChallengesDataList:objLastChalleng];
        }
        return cell;
    }else {
        static NSString *simpleTableIdentifier = @"LastMatchesListCell";

        LastMatchesListCell  *cell = (LastMatchesListCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LastMatchesListCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }

        cell.selectionStyle  = UITableViewCellSelectionStyleNone;

        cell.delegate = self;
        cell.index = indexPath.row;
        
        if ([arrLastChallanges count] > 0) {
            LastChallengesList *objLastChalleng = [arrLastChallanges objectAtIndex:indexPath.row];
            [cell getLastMatchesDataList:objLastChalleng];
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(void)setLastMatchAction:(NSString *)strAction atIndex:(NSInteger)index {

    LastChallengesList *objLastChalleng = [arrLastChallanges objectAtIndex:index];
    
    if ([strAction isEqualToString:@"Picture"]) {
        GameMediaViewController *viewMediaPlay = [[GameMediaViewController alloc] initWithNibName:@"GameMediaViewController" bundle:nil];
        viewMediaPlay.strMediaLink = objLastChalleng.pictures;
        [self.navigationController pushViewController:viewMediaPlay animated:YES];
    }else {
        AVPlayer *player = [AVPlayer playerWithURL:[[NSURL alloc] initWithString:objLastChalleng.video]];

        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setVideoIsNotPlay) name:AVPlayerItemDidPlayToEndTimeNotification object:player.currentItem];

        // create a player view controller
        AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
        [self presentViewController:controller animated:YES completion:nil];
        controller.player = player;
        [player play];
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataSuccess:(NSMutableDictionary *)dictResponce {
        
    if (utility == afPyramideLastChallanges) {
        
        NSLog(@"LASTCHALLANGESRESPONCE ::: %@", dictResponce);
        
        [SHARED_APPDELEGATE hideLoadingView];
                        
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            
            NSArray *arrLastChallData = [dictResponce safeObjectForKey:@"data"];
            
            if ([arrLastChallData count] > 0) {
                
                for (NSDictionary *dictChallange in arrLastChallData) {
                 
                    LastChallengesList *objLastChallenge = [[LastChallengesList alloc] init];
                    
                    objLastChallenge.challengerDate = [NSString stringWithFormat:@"%@",[dictChallange safeObjectForKey:@"challengerDate"]];
                    
                    NSString *strPlayerName1 = [NSString stringWithFormat:@"Mr.%@ %@",[[dictChallange safeObjectForKey:@"challenger_detail"] valueForKey:@"firstname"],[[dictChallange safeObjectForKey:@"challenger_detail"] valueForKey:@"lastname"]];
                                        
                    NSString *strPlayerName2 = [NSString stringWithFormat:@"Mr.%@ %@",[[dictChallange safeObjectForKey:@"challenge_to_detail"] valueForKey:@"firstname"],[[dictChallange safeObjectForKey:@"challenge_to_detail"] valueForKey:@"lastname"]];
                    
                    objLastChallenge.PlayerInfo = [NSString stringWithFormat:@"%@ challaenged %@",strPlayerName1,strPlayerName2];
                    
                    [arrLastChallanges addObject:objLastChallenge];
                }
            }else {
                if ([arrLastChallanges count] == 0) {
                    tblLastChallanges.hidden = YES;
                    viewRecoardNotFound.hidden = NO;
                }
            }
            [tblLastChallanges reloadData];
        }else {
            if ([arrLastChallanges count] == 0) {
                tblLastChallanges.hidden = YES;
                viewRecoardNotFound.hidden = NO;
            }
            [CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
        }
    }else if (utility == afPyramideLastmatches) {
        
               NSLog(@"LASTRESPONCE ::: %@", dictResponce);
               
               [SHARED_APPDELEGATE hideLoadingView];
                               
               if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
                   
                   NSArray *arrLastMatcheData = [dictResponce safeObjectForKey:@"data"];
                   
                   if ([arrLastMatcheData count] > 0) {
                       
                       for (NSDictionary *dictLstMatches in arrLastMatcheData) {
                           
                           LastChallengesList *objLastMatches = [[LastChallengesList alloc] init];
                           
                           objLastMatches.challengerDate = [NSString stringWithFormat:@"%@",[dictLstMatches safeObjectForKey:@"challengerDate"]];
                           objLastMatches.challenger_score = [NSString stringWithFormat:@"%@",[dictLstMatches safeObjectForKey:@"challenger_score"]];
                           objLastMatches.challenged_to_score = [NSString stringWithFormat:@"%@",[dictLstMatches safeObjectForKey:@"challenged_to_score"]];
                           objLastMatches.pictures = [NSString stringWithFormat:@"%@",[dictLstMatches safeObjectForKey:@"pictures"]];
                           objLastMatches.video = [NSString stringWithFormat:@"%@",[dictLstMatches safeObjectForKey:@"video"]];
                           
                           NSDictionary *dictChallengerDetail = [dictLstMatches safeObjectForKey:@"challenger_detail"];
                           NSDictionary *dictChallengerToDetail = [dictLstMatches safeObjectForKey:@"challenge_to_detail"];
                                                 
                           objLastMatches.challenger_FirstName = [NSString stringWithFormat:@"%@",[dictChallengerDetail safeObjectForKey:@"firstname"]];
                           objLastMatches.challenger_LastName = [NSString stringWithFormat:@"%@",[dictChallengerDetail safeObjectForKey:@"lastname"]];
                           objLastMatches.challenged_to_FirstName = [NSString stringWithFormat:@"%@",[dictChallengerToDetail safeObjectForKey:@"firstname"]];
                           objLastMatches.challenged_to_LastName = [NSString stringWithFormat:@"%@",[dictChallengerToDetail safeObjectForKey:@"lastname"]];
                           
                           [arrLastChallanges addObject:objLastMatches];
                       }
                   }else {
                       if ([arrLastChallanges count] == 0) {
                           tblLastChallanges.hidden = YES;
                           viewRecoardNotFound.hidden = NO;
                       }
                   }
                   [tblLastChallanges reloadData];
               }else {
                   if ([arrLastChallanges count] == 0) {
                       tblLastChallanges.hidden = YES;
                       viewRecoardNotFound.hidden = NO;
                   }
                   [CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
               }
       }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Fail_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataFail:(NSError *)error {
    [SHARED_APPDELEGATE hideLoadingView];
    NSString *strError = [NSString stringWithFormat:@"%ld",(long)error.code];
    [CommonClass showAlertWithTitle:provideAlert andMessage:strError delegate:self];
}

@end
