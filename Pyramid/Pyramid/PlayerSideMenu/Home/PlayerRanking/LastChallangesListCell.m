//
//  LastChallangesListCell.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "LastChallangesListCell.h"

@implementation LastChallangesListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    viewBack.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)getLastChallengesDataList:(LastChallengesList*)objLastChallenge {
    
    lblDate.text = [NSString stringWithFormat:@"%@",objLastChallenge.challengerDate];
    lblPlayerName.text = [NSString stringWithFormat:@"%@",objLastChallenge.PlayerInfo];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
