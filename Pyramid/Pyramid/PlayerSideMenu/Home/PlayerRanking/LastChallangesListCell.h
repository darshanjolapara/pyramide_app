//
//  LastChallangesListCell.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LastChallengesList.h"

NS_ASSUME_NONNULL_BEGIN

@interface LastChallangesListCell : UITableViewCell {
    
    IBOutlet UIView *viewBack;
    
    IBOutlet UILabel *lblDate;
    IBOutlet UILabel *lblPlayerName;
}

-(void)getLastChallengesDataList:(LastChallengesList*)objLastChallenge;

@end

NS_ASSUME_NONNULL_END
