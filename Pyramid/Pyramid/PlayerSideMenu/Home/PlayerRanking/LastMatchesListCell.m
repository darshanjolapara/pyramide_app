//
//  LastMatchesListCell.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/10/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "LastMatchesListCell.h"

@implementation LastMatchesListCell

@synthesize delegate;
@synthesize index;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    viewBack.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)getLastMatchesDataList:(LastChallengesList*)objLastMatches {
    
    lblMatchDate.text = [NSString stringWithFormat:@"Match Date : %@",objLastMatches.challengerDate];
    
    NSRange rangeMatch = [lblMatchDate.text rangeOfString:@"Match Date :"];
    
    NSMutableAttributedString *attributMatch = [[NSMutableAttributedString alloc] initWithString:lblMatchDate.text];
    
    [attributMatch setAttributes:@{NSFontAttributeName:FONT_Bold(14), NSForegroundColorAttributeName: [UIColor blackColor]} range:rangeMatch];
    
    lblMatchDate.attributedText = attributMatch;
        
    lblCScore.text = [NSString stringWithFormat:@"%@",objLastMatches.challenger_score];
    lblCDetailFirstName.text = [NSString stringWithFormat:@"%@",objLastMatches.challenger_FirstName];
    lblCDetailLastName.text = [NSString stringWithFormat:@"%@",objLastMatches.challenger_LastName];
    lblCTScore.text = [NSString stringWithFormat:@"%@",objLastMatches.challenged_to_score];
    lblCToDetailFirstName.text = [NSString stringWithFormat:@"%@",objLastMatches.challenged_to_FirstName];
    lblCToDetailLastName.text = [NSString stringWithFormat:@"%@",objLastMatches.challenged_to_LastName];
    
    if (![objLastMatches.video isEqualToString:@""]) {
        btnVideo.hidden = NO;
    }else {
        btnVideo.hidden = YES;
    }
    
    if (![objLastMatches.pictures isEqualToString:@""]) {
        btnPicture.hidden = NO;
    }else {
        btnPicture.hidden = YES;
    }
}

- (IBAction)onClickPictureViewBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setLastMatchAction:atIndex:)]) {
        [delegate setLastMatchAction:@"Picture" atIndex:index];
    }
}

- (IBAction)onClickVideoePlayBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setLastMatchAction:atIndex:)]) {
        [delegate setLastMatchAction:@"Video" atIndex:index];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
