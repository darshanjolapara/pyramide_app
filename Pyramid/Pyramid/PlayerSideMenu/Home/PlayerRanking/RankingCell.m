//
//  RankingCell.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "RankingCell.h"
#import "AppDelegate.h"

@implementation RankingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIBezierPath *cornersPath = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(0, 0, SHARED_APPDELEGATE.window.frame.size.width - 16, 50)  byRoundingCorners:(UIRectCornerBottomRight|UIRectCornerTopRight) cornerRadii:CGSizeMake(8, 8)];

    //Create a new layer to use as a mask
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    [maskLayer setStrokeColor:[appTextFieldBoardColor CGColor]];
    [maskLayer setFillColor:[[UIColor clearColor] CGColor]];
    
    maskLayer.path = cornersPath.CGPath;
    viewBack.layer.mask = maskLayer;

    [[viewBack layer] addSublayer:maskLayer];
}

-(void)getRankingPlayerDataList:(ChallangersList *)objRanking {
    
    NSString *strHexColor = [objRanking.player_status_color stringByReplacingOccurrencesOfString:@"#" withString:@""];
    [viewLine setBackgroundColor:[SHARED_APPDELEGATE colorWithHexString:strHexColor]];
    
    lblPlayerName.text = [NSString stringWithFormat:@"%@",objRanking.player_name];
    lblPosition.text = [NSString stringWithFormat:@"%@",objRanking.player_position];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
