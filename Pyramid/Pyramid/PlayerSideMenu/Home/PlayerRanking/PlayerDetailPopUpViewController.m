//
//  PlayerDetailPopUpViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "PlayerDetailPopUpViewController.h"
#import "AppDelegate.h"

@interface PlayerDetailPopUpViewController ()

@end

@implementation PlayerDetailPopUpViewController

@synthesize delegate;
@synthesize objChallanger;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //[self.view setBackgroundColor: [self colorWithHexString:@"FFFFFF"]]; /* white */

    NSString *imgProfilePicUrl = [NSString stringWithFormat:@"%@",objChallanger.player_profile];

    if (imgProfilePicUrl.length > 0) {
       [asyImgPlayer loadingImage:imgProfilePicUrl placeHolderImage:USER_PLACEHOLDER_IMAGE];
    }else {
       [asyImgPlayer loadingImage:nil placeHolderImage:USER_PLACEHOLDER_IMAGE];
    }
    
    lblPlayerName.text = [NSString stringWithFormat:@"%@",objChallanger.player_name];
    lblPlayerStatus.text = [NSString stringWithFormat:@"%@",objChallanger.player_status];
    lblPlayerRanking.text = [NSString stringWithFormat:@"%@",objChallanger.player_position];
    lblPlayerEficiancy.text = [NSString stringWithFormat:@"%@",objChallanger.player_efficiency];
    lblPlayerYear.text = [NSString stringWithFormat:@"%@",objChallanger.player_born_year];
    
    NSString *strHexColor = [objChallanger.player_status_color stringByReplacingOccurrencesOfString:@"#" withString:@""];
    [viewColor setBackgroundColor:[SHARED_APPDELEGATE colorWithHexString:strHexColor]];
}

- (IBAction)onClickCloseBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setPlayerDetailInfoAction:)]) {
        [delegate setPlayerDetailInfoAction:@"Close"];
    }
}

@end
