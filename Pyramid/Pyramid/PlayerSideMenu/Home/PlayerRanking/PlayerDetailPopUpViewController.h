//
//  PlayerDetailPopUpViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MJPopupViewController.h"
#import "ChallangersList.h"
#import "AsyImage.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PlayerDetailPopUpViewControllerDelegate <NSObject>

-(void)setPlayerDetailInfoAction:(NSString *)strAction;

@end

@interface PlayerDetailPopUpViewController : UIViewController {
    
    IBOutlet UIView *viewColor;
    
    IBOutlet UILabel *lblPlayerName;
    IBOutlet UILabel *lblPlayerStatus;
    IBOutlet UILabel *lblPlayerRanking;
    IBOutlet UILabel *lblPlayerEficiancy;
    IBOutlet UILabel *lblPlayerYear;
    
    IBOutlet AsyImage *asyImgPlayer;
}

@property (nonatomic , strong) id<PlayerDetailPopUpViewControllerDelegate> delegate;

@property (nonatomic , strong) ChallangersList *objChallanger;

@end

NS_ASSUME_NONNULL_END
