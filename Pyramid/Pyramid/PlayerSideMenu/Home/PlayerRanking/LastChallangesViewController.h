//
//  LastChallangesViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LastChallangesListCell.h"
#import "LastChallengesList.h"
#import "LastMatchesListCell.h"
#import "GameMediaViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "AFNetworkUtility.h"

NS_ASSUME_NONNULL_BEGIN

@interface LastChallangesViewController : UIViewController <AFNetworkUtilityDelegate,LastMatchesListCellDelegate> {
    
    IBOutlet UIView *viewRecoardNotFound;
    
    IBOutlet UITableView *tblLastChallanges;
    
    IBOutlet UITextField *txtSearchPlayer;
    
    IBOutlet NSLayoutConstraint *constHeightSearch;
    
    NSMutableArray *arrLastChallanges;
    
    AFNetworkUtility *afPyramideLastChallanges;
    AFNetworkUtility *afPyramideLastmatches;
}

@property (nonatomic , strong) NSString *strAction;

@end

NS_ASSUME_NONNULL_END
