//
//  GameMediaViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/9/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface GameMediaViewController : UIViewController {
    
    IBOutlet UIView *viewBack;
    
    IBOutlet AsyImage *asyImg;
}

@property (nonatomic , strong) NSString *strDisplayOtp;
@property (nonatomic , strong) NSString *strMediaLink;

@end

NS_ASSUME_NONNULL_END
