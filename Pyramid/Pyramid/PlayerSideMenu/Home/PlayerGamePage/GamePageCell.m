//
//  GamePageCell.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "GamePageCell.h"

@implementation GamePageCell

@synthesize delegate;
@synthesize index;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    viewBack.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)setGamePageDataList:(GamePageList*)objGamePage {
   
    viewSetPlaye1.layer.borderColor = appSetTextColor.CGColor;
    viewSetPlaye2.layer.borderColor = appSetTextColor.CGColor;
    
    NSDictionary *defaultAttrs = @{NSFontAttributeName : FONT_Semibold(11), NSForegroundColorAttributeName : [UIColor blackColor]};
    NSDictionary *arrowAttrs = @{NSFontAttributeName : FONT_Bold(13), NSForegroundColorAttributeName : [UIColor blackColor]};

    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] init];
        
    NSString *strscore_1_2 = [NSString stringWithFormat:@"%@",objGamePage.score_1_2];
    NSString *strscore_1_1 = [NSString stringWithFormat:@"%@",objGamePage.score_1_1];
    NSString *str1 = @"";
    NSString *str11 = @"";
    
    if ([strscore_1_2 isEqualToString:@""]) {
        str1 = @"\t-\n";
    }else {
        str1 = [NSString stringWithFormat:@"\t%@\n",strscore_1_2];
    }
    
    if ([strscore_1_1 isEqualToString:@""]) {
        str11 = @"-";
    }else {
        str11 = [NSString stringWithFormat:@"%@",strscore_1_1];
    }
    
    NSAttributedString *content = [[NSAttributedString alloc] initWithString:str1 attributes:defaultAttrs];
    NSAttributedString *arrow = [[NSAttributedString alloc] initWithString:str11 attributes:arrowAttrs];

    NSMutableAttributedString *labelText1 = [[NSMutableAttributedString alloc] init];
    
    NSString *strscore_1_4 = [NSString stringWithFormat:@"%@",objGamePage.score_1_4];
    NSString *strscore_1_3 = [NSString stringWithFormat:@"%@",objGamePage.score_1_3];
    NSString *str2 = @"";
    NSString *str12 = @"";
    
    if ([strscore_1_4 isEqualToString:@""]) {
        str2 = @"\t-\n";
    }else {
        str2 = [NSString stringWithFormat:@"\t%@\n",strscore_1_4];
    }

    if ([strscore_1_3 isEqualToString:@""]) {
        str12 = @"-";
    }else {
        str12 = [NSString stringWithFormat:@"%@",strscore_1_3];
    }
    
    NSAttributedString *content1 = [[NSAttributedString alloc] initWithString:str2 attributes:defaultAttrs];
    NSAttributedString *arrow1 = [[NSAttributedString alloc] initWithString:str12 attributes:arrowAttrs];
    
    NSMutableAttributedString *labelText2 = [[NSMutableAttributedString alloc] init];
    
    NSString *strscore_1_6 = [NSString stringWithFormat:@"%@",objGamePage.score_1_6];
    NSString *strscore_1_5 = [NSString stringWithFormat:@"%@",objGamePage.score_1_5];
    NSString *str3 = @"";
    NSString *str13 = @"";
    
    if ([strscore_1_6 isEqualToString:@""]) {
        str3 = @"\t-\n";
    }else {
        str3 = [NSString stringWithFormat:@"\t%@\n",strscore_1_6];
    }

    if ([strscore_1_5 isEqualToString:@""]) {
        str13 = @"-";
    }else {
        str13 = [NSString stringWithFormat:@"%@",strscore_1_5];
    }
    
    NSAttributedString *content2 = [[NSAttributedString alloc] initWithString:str3 attributes:defaultAttrs];
    NSAttributedString *arrow2 = [[NSAttributedString alloc] initWithString:str13 attributes:arrowAttrs];
    
    NSMutableAttributedString *labelText3 = [[NSMutableAttributedString alloc] init];
    
    NSString *strscore_1_8 = [NSString stringWithFormat:@"%@",objGamePage.score_1_8];
    NSString *strscore_1_7 = [NSString stringWithFormat:@"%@",objGamePage.score_1_7];
    NSString *str4 = @"";
    NSString *str14 = @"";
    
    if ([strscore_1_8 isEqualToString:@""]) {
        str4 = @"\t-\n";
    }else {
        str4 = [NSString stringWithFormat:@"\t%@\n",strscore_1_8];
    }

    if ([strscore_1_7 isEqualToString:@""]) {
        str14 = @"-";
    }else {
        str14 = [NSString stringWithFormat:@"%@",strscore_1_7];
    }
    
    NSAttributedString *content3 = [[NSAttributedString alloc] initWithString:str4 attributes:defaultAttrs];
    NSAttributedString *arrow3 = [[NSAttributedString alloc] initWithString:str14 attributes:arrowAttrs];
    
    NSMutableAttributedString *labelText4 = [[NSMutableAttributedString alloc] init];
    
    NSString *strscore_1_10 = [NSString stringWithFormat:@"%@",objGamePage.score_1_10];
    NSString *strscore_1_9 = [NSString stringWithFormat:@"%@",objGamePage.score_1_9];
    NSString *str5 = @"";
    NSString *str15 = @"";
    
    if ([strscore_1_10 isEqualToString:@""]) {
       str5 = @"\t-\n";
    }else {
       str5 = [NSString stringWithFormat:@"\t%@\n",strscore_1_10];
    }
    
    if ([strscore_1_9 isEqualToString:@""]) {
        str15 = @"-";
    }else {
        str15 = [NSString stringWithFormat:@"%@",strscore_1_9];
    }
    
    NSAttributedString *content4 = [[NSAttributedString alloc] initWithString:str5 attributes:defaultAttrs];
    NSAttributedString *arrow4 = [[NSAttributedString alloc] initWithString:str15 attributes:arrowAttrs];
    
    [labelText appendAttributedString:content];
    [labelText appendAttributedString:arrow];
    
    [labelText1 appendAttributedString:content1];
    [labelText1 appendAttributedString:arrow1];
    
    [labelText2 appendAttributedString:content2];
    [labelText2 appendAttributedString:arrow2];
    
    [labelText3 appendAttributedString:content3];
    [labelText3 appendAttributedString:arrow3];
    
    [labelText4 appendAttributedString:content4];
    [labelText4 appendAttributedString:arrow4];
        
    lblPlayer1TotalCount.text = [NSString stringWithFormat:@"%@",objGamePage.challenger_score];
    lblPlayer2TotalCount.text = [NSString stringWithFormat:@"%@",objGamePage.challenged_to_score];
    
    NSMutableAttributedString *labelText5 = [[NSMutableAttributedString alloc] init];
        
    NSString *strscore_2_2 = [NSString stringWithFormat:@"%@",objGamePage.score_2_2];
    NSString *strscore_2_1 = [NSString stringWithFormat:@"%@",objGamePage.score_2_1];
    NSString *str6 = @"";
    NSString *str26 = @"";
    
    if ([strscore_2_2 isEqualToString:@""]) {
       str6 = @"\t-\n";
    }else {
       str6 = [NSString stringWithFormat:@"\t%@\n",strscore_2_2];
    }
    
    if ([strscore_2_1 isEqualToString:@""]) {
       str26 = @"-";
    }else {
       str26 = [NSString stringWithFormat:@"%@",strscore_2_1];
    }
    
    NSAttributedString *content5 = [[NSAttributedString alloc] initWithString:str6 attributes:defaultAttrs];
    NSAttributedString *arrow5 = [[NSAttributedString alloc] initWithString:str26 attributes:arrowAttrs];

    NSMutableAttributedString *labelText6 = [[NSMutableAttributedString alloc] init];
    
    NSString *strscore_2_4 = [NSString stringWithFormat:@"%@",objGamePage.score_2_4];
    NSString *strscore_2_3 = [NSString stringWithFormat:@"%@",objGamePage.score_2_3];
    NSString *str7 = @"";
    NSString *str27 = @"";
    
    if ([strscore_2_4 isEqualToString:@""]) {
       str7 = @"\t-\n";
    }else {
       str7 = [NSString stringWithFormat:@"\t%@\n",strscore_2_4];
    }
    
    if ([strscore_2_3 isEqualToString:@""]) {
       str27 = @"-";
    }else {
       str27 = [NSString stringWithFormat:@"%@",strscore_2_3];
    }
    
    NSAttributedString *content6 = [[NSAttributedString alloc] initWithString:str7 attributes:defaultAttrs];
    NSAttributedString *arrow6 = [[NSAttributedString alloc] initWithString:str27 attributes:arrowAttrs];
    
    NSMutableAttributedString *labelText7 = [[NSMutableAttributedString alloc] init];
    
    NSString *strscore_2_6 = [NSString stringWithFormat:@"%@",objGamePage.score_2_6];
    NSString *strscore_2_5 = [NSString stringWithFormat:@"%@",objGamePage.score_2_5];
    NSString *str8 = @"";
    NSString *str28 = @"";
    
    if ([strscore_2_6 isEqualToString:@""]) {
       str8 = @"\t-\n";
    }else {
       str8 = [NSString stringWithFormat:@"\t%@\n",strscore_2_6];
    }

    if ([strscore_2_5 isEqualToString:@""]) {
       str28 = @"-";
    }else {
       str28 = [NSString stringWithFormat:@"%@",strscore_2_5];
    }
    
    NSAttributedString *content7 = [[NSAttributedString alloc] initWithString:str8 attributes:defaultAttrs];
    NSAttributedString *arrow7 = [[NSAttributedString alloc] initWithString:str28 attributes:arrowAttrs];
    
    NSMutableAttributedString *labelText8 = [[NSMutableAttributedString alloc] init];
    
    NSString *strscore_2_8 = [NSString stringWithFormat:@"%@",objGamePage.score_2_8];
    NSString *strscore_2_7 = [NSString stringWithFormat:@"%@",objGamePage.score_2_7];
    NSString *str9 = @"";
    NSString *str29 = @"";
    
    if ([strscore_2_8 isEqualToString:@""]) {
       str9 = @"\t-\n";
    }else {
       str9 = [NSString stringWithFormat:@"\t%@\n",strscore_2_8];
    }
    
    if ([strscore_2_7 isEqualToString:@""]) {
       str29 = @"-";
    }else {
       str29 = [NSString stringWithFormat:@"%@",strscore_2_7];
    }
    
    NSAttributedString *content8 = [[NSAttributedString alloc] initWithString:str9 attributes:defaultAttrs];
    NSAttributedString *arrow8 = [[NSAttributedString alloc] initWithString:str29 attributes:arrowAttrs];
    
    NSMutableAttributedString *labelText9 = [[NSMutableAttributedString alloc] init];
    
    NSString *strscore_2_10 = [NSString stringWithFormat:@"%@",objGamePage.score_2_10];
    NSString *strscore_2_9 = [NSString stringWithFormat:@"%@",objGamePage.score_2_9];
    NSString *str10 = @"";
    NSString *str210 = @"";
    
    if ([strscore_2_10 isEqualToString:@""]) {
       str10 = @"\t-\n";
    }else {
       str10 = [NSString stringWithFormat:@"\t%@\n",strscore_2_10];
    }
 
    if ([strscore_2_9 isEqualToString:@""]) {
       str210 = @"-";
    }else {
       str210 = [NSString stringWithFormat:@"%@",strscore_2_9];
    }
    
    NSAttributedString *content9 = [[NSAttributedString alloc] initWithString:str10 attributes:defaultAttrs];
    NSAttributedString *arrow9 = [[NSAttributedString alloc] initWithString:str210 attributes:arrowAttrs];
    
    [labelText5 appendAttributedString:content5];
    [labelText5 appendAttributedString:arrow5];
    
    [labelText6 appendAttributedString:content6];
    [labelText6 appendAttributedString:arrow6];
    
    [labelText7 appendAttributedString:content7];
    [labelText7 appendAttributedString:arrow7];
    
    [labelText8 appendAttributedString:content8];
    [labelText8 appendAttributedString:arrow8];
    
    [labelText9 appendAttributedString:content9];
    [labelText9 appendAttributedString:arrow9];
    
    lblPlayer1Set1.attributedText = labelText;
    lblPlayer1Set2.attributedText = labelText1;
    lblPlayer1Set3.attributedText = labelText2;
    lblPlayer1Set4.attributedText = labelText3;
    lblPlayer1Set5.attributedText = labelText4;
    
    lblPlayer2Set1.attributedText = labelText5;
    lblPlayer2Set2.attributedText = labelText6;
    lblPlayer2Set3.attributedText = labelText7;
    lblPlayer2Set4.attributedText = labelText8;
    lblPlayer2Set5.attributedText = labelText9;
    
    lblDateTime.text = [NSString stringWithFormat:@"%@ %@",objGamePage.challengerDate,objGamePage.competition_play_hour];
    lblAddress.text = [NSString stringWithFormat:@"%@",objGamePage.court_name];
    
    lblPlayer1Name.text = [NSString stringWithFormat:@"%@",objGamePage.challenged_PlayerName];
    lblPlayer2Name.text = [NSString stringWithFormat:@"%@",objGamePage.challenged_to_PlayerName];
    
    NSString *imgProfilePicUrl1 = [NSString stringWithFormat:@"%@",objGamePage.challenged_Profile];
    NSString *imgProfilePicUrl2 = [NSString stringWithFormat:@"%@",objGamePage.challenged_to_Profile];

    if (imgProfilePicUrl1.length > 0) {
       [imgPlayer1 loadingImage:imgProfilePicUrl1 placeHolderImage:USER_PLACEHOLDER_IMAGE];
    }else {
       [imgPlayer1 loadingImage:nil placeHolderImage:USER_PLACEHOLDER_IMAGE];
    }
    
    if (imgProfilePicUrl2.length > 0) {
       [imgPlayer2 loadingImage:imgProfilePicUrl2 placeHolderImage:USER_PLACEHOLDER_IMAGE];
    }else {
       [imgPlayer2 loadingImage:nil placeHolderImage:USER_PLACEHOLDER_IMAGE];
    }
    
    NSLog(@"video>>>>> %@",objGamePage.video);
    NSLog(@"pictures>>>>> %@",objGamePage.pictures);
    
    if (![objGamePage.video isEqualToString:@""] || ![objGamePage.pictures isEqualToString:@""]) {
        
        contBtnViewHeight.constant = 46.0f;
        
        if (![objGamePage.video isEqualToString:@""] && ![objGamePage.pictures isEqualToString:@""]) {
            
            btnVidPic.hidden = YES;
            btnVideo.hidden = NO;
            btnPicture.hidden = NO;
        }else if (![objGamePage.video isEqualToString:@""] && [objGamePage.pictures isEqualToString:@""]) {
            
            btnVidPic.hidden = NO;
            btnVideo.hidden = YES;
            btnPicture.hidden = YES;
            
            [btnVidPic setImage:[UIImage imageNamed:@"video.fill"] forState:UIControlStateNormal];
            [btnVidPic setTitle:@"Video" forState:UIControlStateNormal];
        }else if ([objGamePage.video isEqualToString:@""] && ![objGamePage.pictures isEqualToString:@""]) {
            
            btnVidPic.hidden = NO;
            btnVideo.hidden = YES;
            btnPicture.hidden = YES;
            
            [btnVidPic setImage:[UIImage imageNamed:@"photo"] forState:UIControlStateNormal];
            [btnVidPic setTitle:@"Picture" forState:UIControlStateNormal];
        }
            
    }else {
        contBtnViewHeight.constant = 0.0f;
    }
    
    contViewHeight.constant = contBtnViewHeight.constant + 277.0f;
    
    cellHeight = contViewHeight.constant + 12;
}

-(CGFloat )getCellHeight {
    return cellHeight;
}

- (IBAction)onClickPictureBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setGamePageAction:AtIndex:)]) {
       [delegate setGamePageAction:@"Picture" AtIndex:index];
    }
}

- (IBAction)onClickVideoBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setGamePageAction:AtIndex:)]) {
       [delegate setGamePageAction:@"Video" AtIndex:index];
    }
}

- (IBAction)onClickPicVidBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setGamePageAction:AtIndex:)]) {
       [delegate setGamePageAction:@"VidPic" AtIndex:index];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
