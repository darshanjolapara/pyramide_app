//
//  GamePageCell.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GamePageList.h"
#import "AsyImage.h"

NS_ASSUME_NONNULL_BEGIN

@protocol GamePageCellDelegate <NSObject>

-(void)setGamePageAction:(NSString *)strAction AtIndex:(NSInteger)index;

@end

@interface GamePageCell : UITableViewCell {
 
    IBOutlet UIView *viewBack;
    IBOutlet UIView *viewSetPlaye1;
    IBOutlet UIView *viewSetPlaye2;
    
    IBOutlet UILabel *lblDateTime;
    IBOutlet UILabel *lblAddress;
    IBOutlet UILabel *lblPlayer1Name;
    IBOutlet UILabel *lblPlayer2Name;
    IBOutlet UILabel *lblPlayer1Set1;
    IBOutlet UILabel *lblPlayer1Set2;
    IBOutlet UILabel *lblPlayer1Set3;
    IBOutlet UILabel *lblPlayer1Set4;
    IBOutlet UILabel *lblPlayer1Set5;
    IBOutlet UILabel *lblPlayer2Set1;
    IBOutlet UILabel *lblPlayer2Set2;
    IBOutlet UILabel *lblPlayer2Set3;
    IBOutlet UILabel *lblPlayer2Set4;
    IBOutlet UILabel *lblPlayer2Set5;
    IBOutlet UILabel *lblPlayer1TotalCount;
    IBOutlet UILabel *lblPlayer2TotalCount;
    
    IBOutlet AsyImage *imgPlayer1;
    IBOutlet AsyImage *imgPlayer2;
    
    IBOutlet UIButton *btnVideo;
    IBOutlet UIButton *btnPicture;
    IBOutlet UIButton *btnVidPic;
    
    IBOutlet NSLayoutConstraint *contViewHeight;
    IBOutlet NSLayoutConstraint *contBtnViewHeight;
    
    CGFloat cellHeight;
}

//Delegate
@property (nonatomic , strong) id<GamePageCellDelegate> delegate;

//Integer
@property (nonatomic) NSInteger index;

-(CGFloat )getCellHeight;
-(void)setGamePageDataList:(GamePageList*)objGamePage;

@end

NS_ASSUME_NONNULL_END
