//
//  AllGamesListViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GamePageCell.h"
#import "GamePageList.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "GameMediaViewController.h"
#import "AFNetworkUtility.h"

NS_ASSUME_NONNULL_BEGIN

@interface AllGamesListViewController : UIViewController <AFNetworkUtilityDelegate,GamePageCellDelegate> {
    
    IBOutlet UIView *viewRecoardNotFound;
    
    IBOutlet UITextField *txtSearchPlayer;
    
    IBOutlet UITableView *tblAllGames;
    
    NSMutableArray *arrGamePageList;
    
    int intCountPage;
    
    BOOL isLoadMore;
    
    AFNetworkUtility *afPyramideAllGame;
}

@end

NS_ASSUME_NONNULL_END
