//
//  GameMediaViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/9/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "GameMediaViewController.h"
#import "AppDelegate.h"

@interface GameMediaViewController ()

@end

@implementation GameMediaViewController

@synthesize strMediaLink;
@synthesize strDisplayOtp;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.navigationItem.titleView = [SHARED_APPDELEGATE getNavigationCenterWithTitle:NavigationPicture fontSize:18];
    
    UIImage *backButtonImage = [UIImage imageNamed:@"Back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton setImage:backButtonImage
                forState:UIControlStateNormal];
    
    backButton.frame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self
                   action:@selector(popViewController)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
    NSString *imgProfilePicUrl = [NSString stringWithFormat:@"%@",strMediaLink];

    if (imgProfilePicUrl.length > 0) {
       [asyImg loadingImage:imgProfilePicUrl placeHolderImage:USER_PLACEHOLDER_IMAGE];
    }else {
       [asyImg loadingImage:nil placeHolderImage:USER_PLACEHOLDER_IMAGE];
    }
}

#pragma mark -
#pragma mark - Pop To View Mothod

-(void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
