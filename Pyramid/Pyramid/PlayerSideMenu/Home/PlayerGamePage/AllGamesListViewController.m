//
//  AllGamesListViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "AllGamesListViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"

@interface AllGamesListViewController ()

@end

@implementation AllGamesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.titleView = [SHARED_APPDELEGATE getNavigationCenterWithTitle:NavigationMyGames fontSize:18];
    
    UIImage *backButtonImage = [UIImage imageNamed:@"Back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton setImage:backButtonImage
                forState:UIControlStateNormal];
    
    backButton.frame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self
                   action:@selector(popViewController)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
    txtSearchPlayer.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:@"Search for player name.."
                attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    arrGamePageList = [[NSMutableArray alloc] init];
    
    intCountPage = 0;
    [self getPlayerGamePageServiceCall:intCountPage searchText:@""];
}

#pragma mark -
#pragma mark - Pop To View Mothod

-(void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getPlayerGamePageServiceCall:(int)intPage searchText:(NSString *)strFind {
    
    if (afPyramideAllGame) {
       [afPyramideAllGame cancelRequest];
       afPyramideAllGame.delegate = self;
       afPyramideAllGame = nil;
    }

    afPyramideAllGame = [[AFNetworkUtility alloc] init];
    afPyramideAllGame.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];
    
    NSMutableDictionary *dictGamePage = [[NSMutableDictionary alloc] init];
        
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];
    NSString *strCompetitionID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:COMPETITIONID]];
       
    NSString *strCountPage = [NSString stringWithFormat:@"%d",intPage];
    
    [dictGamePage setValue:strCompetitionID forKey:@"competition_id"];
    [dictGamePage setValue:strFind forKey:@"find"];
    [dictGamePage setValue:strCountPage forKey:@"page"];
    
    if ([strSelectLang isEqualToString:@""]) {
        [dictGamePage setValue:@"english" forKey:@"lang"];
    }else {
        [dictGamePage setValue:strSelectLang forKey:@"lang"];
    }
    
    NSLog(@"GamePagePARAM ::: %@",dictGamePage);
    
    [afPyramideAllGame requestWithUrl:GetAllGame_Url param:dictGamePage andToken:strAPIKey];
}

#pragma mark -
#pragma mark - TableView Delegate Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    GamePageCell *cell = (GamePageCell *)[self tableView:tblAllGames cellForRowAtIndexPath:indexPath];
    return [cell getCellHeight];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrGamePageList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"GamePageCell";

    GamePageCell  *cell = (GamePageCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GamePageCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    cell.selectionStyle  = UITableViewCellSelectionStyleNone;

    cell.delegate = self;
    cell.index = indexPath.row;
    
    if ([arrGamePageList count] > 0) {
        GamePageList *objGamePage = [arrGamePageList objectAtIndex:indexPath.row];
        [cell setGamePageDataList:objGamePage];
    }

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark -
#pragma mark - SCROLL_END_DECELERATING_AND_LOAD_MORE_BUTTON_METHODS

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if(scrollView == tblAllGames){
        float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height + 20;
        NSLog(@"ValueScrolling>>> %f",endScrolling);
        
        if (endScrolling >= scrollView.contentSize.height) {
            if(isLoadMore) {
                intCountPage ++;
                if ([[CommonClass trimString:txtSearchPlayer.text] length] == 0) {
                    [self getPlayerGamePageServiceCall:intCountPage searchText:@""];
                }else {
                    [self getPlayerGamePageServiceCall:intCountPage searchText:txtSearchPlayer.text];
                }
                isLoadMore = false;
            }
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if ([[CommonClass trimString:txtSearchPlayer.text] length] != 0) {
        intCountPage = 0;
        
        [arrGamePageList removeAllObjects];
        arrGamePageList = [[NSMutableArray alloc] init];
        
        [self getPlayerGamePageServiceCall:intCountPage searchText:txtSearchPlayer.text];
    }
    return YES;
}

-(void)setGamePageAction:(NSString *)strAction AtIndex:(NSInteger)index {
    
    GamePageList *objGamePage = [arrGamePageList objectAtIndex:index];
    
    if ([strAction isEqualToString:@"Picture"]) {
        GameMediaViewController *viewMediaPlay = [[GameMediaViewController alloc] initWithNibName:@"GameMediaViewController" bundle:nil];
        viewMediaPlay.strMediaLink = objGamePage.pictures;
        [self.navigationController pushViewController:viewMediaPlay animated:YES];
    }else if ([strAction isEqualToString:@"Video"]) {
        AVPlayer *player = [AVPlayer playerWithURL:[[NSURL alloc] initWithString:objGamePage.video]];

        // create a player view controller
        AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
        [self presentViewController:controller animated:YES completion:nil];
        controller.player = player;
        [player play];
    }else if ([strAction isEqualToString:@"VidPic"]) {
        
        if (![objGamePage.video isEqualToString:@""]) {
            AVPlayer *player = [AVPlayer playerWithURL:[[NSURL alloc] initWithString:objGamePage.video]];

            // create a player view controller
            AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
            [self presentViewController:controller animated:YES completion:nil];
            controller.player = player;
            [player play];
        }else if ([objGamePage.pictures isEqualToString:@""]) {
            GameMediaViewController *viewMediaPlay = [[GameMediaViewController alloc] initWithNibName:@"GameMediaViewController" bundle:nil];
            viewMediaPlay.strMediaLink = objGamePage.pictures;
            [self.navigationController pushViewController:viewMediaPlay animated:YES];
        }
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataSuccess:(NSMutableDictionary *)dictResponce {
            
    if (utility == afPyramideAllGame) {
        
        NSLog(@"PLAYERCOMRESPONCE ::: %@", dictResponce);
          
          [SHARED_APPDELEGATE hideLoadingView];
                          
          if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
              
              NSArray *arrGamePageData = [dictResponce safeObjectForKey:@"data"];
              
              if ([arrGamePageData count] > 0) {
                  
                  viewRecoardNotFound.hidden = YES;
                  tblAllGames.hidden = NO;
                  
                  isLoadMore = true;
                  
                  for (NSDictionary *dictGamePage in arrGamePageData) {
                      
                      GamePageList *objGamePage = [[GamePageList alloc] init];
                      
                      objGamePage.score_1_1 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_1"]];
                      objGamePage.score_1_2 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_2"]];
                      objGamePage.score_1_3 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_3"]];
                      objGamePage.score_1_4 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_4"]];
                      objGamePage.score_1_5 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_5"]];
                      objGamePage.score_1_6 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_6"]];
                      objGamePage.score_1_7 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_7"]];
                      objGamePage.score_1_8 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_8"]];
                      objGamePage.score_1_9 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_9"]];
                      objGamePage.score_1_10 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_10"]];
                      objGamePage.score_1_11 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_11"]];
                      objGamePage.score_2_1 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_1"]];
                      objGamePage.score_2_2 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_2"]];
                      objGamePage.score_2_3 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_3"]];
                      objGamePage.score_2_4 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_4"]];
                      objGamePage.score_2_5 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_5"]];
                      objGamePage.score_2_6 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_6"]];
                      objGamePage.score_2_7 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_7"]];
                      objGamePage.score_2_8 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_8"]];
                      objGamePage.score_2_9 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_9"]];
                      objGamePage.score_2_10 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_10"]];
                      objGamePage.score_2_11 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_11"]];
                      objGamePage.challenger_score = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"challenger_score"]];
                      objGamePage.challenged_to_score = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"challenged_to_score"]];
                      objGamePage.challengerDate = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"challengerDate"]];
                      objGamePage.competition_play_hour = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"competition_play_hour"]];
                      objGamePage.court_name = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"court_name"]];
                      objGamePage.video = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"video"]];
                      objGamePage.pictures = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"pictures"]];
                      
                      NSDictionary *dictChallengerDetail = [dictGamePage safeObjectForKey:@"challenger_detail"];
                      NSDictionary *dictChallengerToDetail = [dictGamePage safeObjectForKey:@"challenge_to_detail"];
                      
                      objGamePage.challenged_Profile = [NSString stringWithFormat:@"%@",[dictChallengerDetail safeObjectForKey:@"profile"]];
                      objGamePage.challenged_PlayerName = [NSString stringWithFormat:@"%@ %@",[dictChallengerDetail safeObjectForKey:@"firstname"],[dictChallengerDetail safeObjectForKey:@"lastname"]];
                      
                      objGamePage.challenged_to_Profile = [NSString stringWithFormat:@"%@",[dictChallengerToDetail safeObjectForKey:@"profile"]];
                      objGamePage.challenged_to_PlayerName = [NSString stringWithFormat:@"%@ %@",[dictChallengerToDetail safeObjectForKey:@"firstname"],[dictChallengerToDetail safeObjectForKey:@"lastname"]];
                      
                      
                      [arrGamePageList addObject:objGamePage];
                  }
                  [tblAllGames reloadData];
              }else {
                  
                  if ([arrGamePageList count] > 0) {
                      isLoadMore = true;
                  }else {
                      viewRecoardNotFound.hidden = NO;
                      tblAllGames.hidden = YES;
                  }
              }
          }else {
              if ([arrGamePageList count] > 0) {
                  isLoadMore = true;
              }else {
                  viewRecoardNotFound.hidden = NO;
                  tblAllGames.hidden = YES;
              }
          }
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Fail_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataFail:(NSError *)error {
    [SHARED_APPDELEGATE hideLoadingView];
    NSString *strError = [NSString stringWithFormat:@"%ld",(long)error.code];
    [CommonClass showAlertWithTitle:provideAlert andMessage:strError delegate:self];
}

@end
