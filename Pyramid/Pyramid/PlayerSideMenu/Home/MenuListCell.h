//
//  MenuListCell.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MenuListCell : UICollectionViewCell

@property (nonatomic , strong) IBOutlet UILabel *lblTitle;
@property (nonatomic , strong) IBOutlet UIView *viewLine;

@end

NS_ASSUME_NONNULL_END
