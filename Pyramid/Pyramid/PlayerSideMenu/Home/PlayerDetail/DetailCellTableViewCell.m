//
//  DetailCellTableViewCell.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "DetailCellTableViewCell.h"

@implementation DetailCellTableViewCell

@synthesize btnEditStatus;
@synthesize delegate;
@synthesize index;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)getPlayerDetailInfo:(NSDictionary *)dict {
    
    if ([[dict safeObjectForKey:@"Detail"] isEqualToString:@"Player Note"] || [[dict safeObjectForKey:@"Detail"] isEqualToString:@"No Ranking"] || [[dict safeObjectForKey:@"Detail"] isEqualToString:@"About"]) {
        lblInfo.textColor = [UIColor lightGrayColor];
    }else {
        lblInfo.textColor = [UIColor blackColor];
    }
    
    lblTitle.text = [NSString stringWithFormat:@"%@",[dict safeObjectForKey:@"Title"]];
    lblInfo.text = [NSString stringWithFormat:@"%@",[dict safeObjectForKey:@"Detail"]];
    
    if ([lblTitle.text isEqualToString:@"My Status"] || [lblTitle.text isEqualToString:@"About"]) {
        
        if (([lblTitle.text isEqualToString:@"My Status"] && (![[dict safeObjectForKey:@"Detail"] isEqualToString:@"Available for challenge"])) || [lblTitle.text isEqualToString:@"About"]) {
            
            CGRect expectedLabelSize = [lblInfo.text boundingRectWithSize:CGSizeMake(lblInfo.frame.size.width, CGFLOAT_MAX)
            options:NSStringDrawingUsesLineFragmentOrigin
            attributes:@{NSFontAttributeName: FONT_OpenSans(13)}
            context:nil];
            
            //adjust the label the the new height.
            CGRect newFrame = lblInfo.frame;
            newFrame.size.height = expectedLabelSize.size.height;
            lblInfo.frame = newFrame;

            contLblHeight.constant = lblInfo.frame.size.height;
        }
    }
    
    if (contLblHeight.constant > 37) {
       cellHeight = contLblHeight.constant + 6;
    }else {
       cellHeight = 37.0f;
    }
}

-(CGFloat )getCellHeight {
    return cellHeight;
}

- (IBAction)onClickChangeStatusBtn:(id)sender {
    
    if (delegate && [delegate respondsToSelector:@selector(setChangeStatusAtIndex:)]) {
        [delegate setChangeStatusAtIndex:index];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
