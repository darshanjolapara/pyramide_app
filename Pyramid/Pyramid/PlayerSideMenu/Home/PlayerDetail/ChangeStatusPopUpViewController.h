//
//  ChangeStatusPopUpViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MJPopupViewController.h"
#import "NIDropDown.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ChangeStatusPopUpViewControllerDelegate <NSObject>

-(void)selectStatus:(NSString *)strStatus AndAction:(NSString *)strType;

@end

@interface ChangeStatusPopUpViewController : UIViewController <NIDropDownDelegate> {
    
    IBOutlet UIButton *btnSelectStatus;
    
    NIDropDown *dropDown;
}

@property (nonatomic , strong) NSString *strStatusTitle;

@property (nonatomic , strong) id<ChangeStatusPopUpViewControllerDelegate> delegate;

//@property (nonatomic) NSInteger blogIndex;

@end

NS_ASSUME_NONNULL_END
