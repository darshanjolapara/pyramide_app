//
//  AcceptChallengePopUpViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AcceptChallengePopUpViewControllerDelegate <NSObject>

-(void)setAcceptChallengeAction:(NSString *)strAction atIndex:(NSInteger)index;

@end

@interface AcceptChallengePopUpViewController : UIViewController

//Delegate
@property (nonatomic , strong) id<AcceptChallengePopUpViewControllerDelegate> delegate;

//Integer
@property (nonatomic) NSInteger index;

@end

NS_ASSUME_NONNULL_END
