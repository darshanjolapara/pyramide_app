//
//  ChallengeCanIPopUpViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/11/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ChallengeCanIPopUpViewControllerDelegate <NSObject>

-(void)setChallengeAction:(NSString *)strAction;
-(void)setAcceptChallengeDate:(NSString *)strDate challengeHours:(NSString *)strHour andCourtName:(NSString *)strCourtName;

@end

@interface ChallengeCanIPopUpViewController : UIViewController {
    
    IBOutlet UITextField *txtCourtName;
    
    IBOutlet UIButton *btnDate;
    IBOutlet UIButton *btnHour;
}

@property (nonatomic , strong) id<ChallengeCanIPopUpViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
