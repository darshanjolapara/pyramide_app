//
//  ChallengeCanIPopUpViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/11/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "ChallengeCanIPopUpViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"

@interface ChallengeCanIPopUpViewController ()

@end

@implementation ChallengeCanIPopUpViewController

@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    txtCourtName.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtCourtName.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtCourtName.leftViewMode = UITextFieldViewModeAlways;
    txtCourtName.rightViewMode = UITextFieldViewModeAlways;
    txtCourtName.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtCourtName.attributedPlaceholder = [[NSAttributedString alloc]
    initWithString:provideCourtName
                attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    btnDate.layer.borderColor = appTextFieldBoardColor.CGColor;
    btnHour.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strMatchDate = [NSString stringWithFormat:@"%@",[userDefault objectForKey:MATCHDATE]];

    NSString *strMatcheHour = [NSString stringWithFormat:@"%@",[userDefault objectForKey:MATCHHOUR]];
    
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MMM-yyyy"];
    NSString *dateString = [dateFormat stringFromDate:today];
    
    if ([strMatchDate isEqualToString:@"(null)"] || [strMatcheHour isEqualToString:@""]) {
        [btnDate setTitle:dateString forState:UIControlStateNormal];
    }else {
        [btnDate setTitle:strMatchDate forState:UIControlStateNormal];
    }
    
    if ([strMatcheHour isEqualToString:@"(null)"] || [strMatcheHour isEqualToString:@""]) {
        [btnHour setTitle:@"Hour" forState:UIControlStateNormal];
    }else {
        [btnHour setTitle:strMatcheHour forState:UIControlStateNormal];
    }
}

- (IBAction)onClickSelectDateBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setChallengeAction:)]) {
        [delegate setChallengeAction:@"Date"];
    }
}

- (IBAction)onClickSelectHourBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setChallengeAction:)]) {
        [delegate setChallengeAction:@"Hour"];
    }
}

- (IBAction)onClickAcceptBtn:(id)sender {
    
    if (delegate && [delegate respondsToSelector:@selector(setAcceptChallengeDate:challengeHours:andCourtName:)]) {
        [delegate setAcceptChallengeDate:btnDate.titleLabel.text challengeHours:btnHour.titleLabel.text andCourtName:[CommonClass trimString:txtCourtName.text]];
    }
}

- (IBAction)onClickCancelBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setChallengeAction:)]) {
        [delegate setChallengeAction:@"Cancel"];
    }
}

@end
