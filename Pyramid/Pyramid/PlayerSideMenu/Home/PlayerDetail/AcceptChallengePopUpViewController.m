//
//  AcceptChallengePopUpViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "AcceptChallengePopUpViewController.h"

@interface AcceptChallengePopUpViewController ()

@end

@implementation AcceptChallengePopUpViewController

@synthesize delegate;
@synthesize index;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)onClickChallengeAcceptBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setAcceptChallengeAction:atIndex:)]) {
       [delegate setAcceptChallengeAction:@"Yes" atIndex:index];
    }
}

- (IBAction)onClickChallengeCancelBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setAcceptChallengeAction:atIndex:)]) {
        [delegate setAcceptChallengeAction:@"Cancel" atIndex:index];
    }
}

@end
