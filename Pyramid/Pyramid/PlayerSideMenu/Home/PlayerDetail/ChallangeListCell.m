//
//  ChallangeListCell.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "ChallangeListCell.h"

@implementation ChallangeListCell

@synthesize btnPlayerStatistics;
@synthesize btnChallenge;
@synthesize btnPlayerStatisticsBy;
@synthesize delegate;
@synthesize index;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    viewBack.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)getCanChallangerDataList:(ChallangersList*)objChallanger {
    
    lblPlayerName.text = [NSString stringWithFormat:@"%@",objChallanger.player_name];
    lblPlayerPosition.text = [NSString stringWithFormat:@"%@",objChallanger.player_position];
}

- (IBAction)onClickPlayerStaticsticBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setChallangeInfoAction:AtIndex:)]) {
        [delegate setChallangeInfoAction:@"Staticstic" AtIndex:index];
    }
}

- (IBAction)onClickChallengeBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setChallangeInfoAction:AtIndex:)]) {
        [delegate setChallangeInfoAction:@"Challange" AtIndex:index];
    }
}

- (IBAction)onClickPlayerStaticsticByBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setChallangeInfoAction:AtIndex:)]) {
        [delegate setChallangeInfoAction:@"Staticstic" AtIndex:index];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
