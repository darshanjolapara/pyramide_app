//
//  ChallangeListCell.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChallangersList.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ChallangeListCellDelegate <NSObject>

-(void)setChallangeInfoAction:(NSString *)strAction AtIndex:(NSInteger)index;

@end

@interface ChallangeListCell : UITableViewCell {
    
    IBOutlet UIView *viewBack;
    
    IBOutlet UILabel *lblPlayerPosition;
    IBOutlet UILabel *lblPlayerName;
}

@property (nonatomic , strong) IBOutlet UIButton *btnPlayerStatistics;
@property (nonatomic , strong) IBOutlet UIButton *btnChallenge;
@property (nonatomic , strong) IBOutlet UIButton *btnPlayerStatisticsBy;

//Delegate
@property (nonatomic , strong) id<ChallangeListCellDelegate> delegate;

//Integer
@property (nonatomic) NSInteger index;

-(void)getCanChallangerDataList:(ChallangersList*)objChallanger;

@end

NS_ASSUME_NONNULL_END
