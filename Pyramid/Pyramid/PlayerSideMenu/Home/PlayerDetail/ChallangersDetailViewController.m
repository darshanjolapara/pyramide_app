//
//  ChallangersDetailViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "ChallangersDetailViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"

@interface ChallangersDetailViewController ()

@end

@implementation ChallangersDetailViewController

@synthesize strNavTitle;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strNavName = [userDefault objectForKey:COMPETITIONNAME];
    
    self.navigationItem.titleView = [SHARED_APPDELEGATE getNavigationCenterWithTitle:strNavName fontSize:18];
    
    UIImage *backButtonImage = [UIImage imageNamed:@"Back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton setImage:backButtonImage
                forState:UIControlStateNormal];
    
    backButton.frame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self
                   action:@selector(popViewController)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
    isChallange = YES;
    isChallangeBy = NO;
    
    viewChallange.hidden = NO;
    viewChallangeBy.hidden = YES;
    
    [self getCanChallengeServiceCall:@"CanChallange"];
}

#pragma mark -
#pragma mark - Pop To View Mothod

-(void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getCanChallengeServiceCall:(NSString *)strAction {
    
    [arrCanChallangerList removeAllObjects];
    arrCanChallangerList = [[NSMutableArray alloc] init];
    
    if (afPyramideCanChallange) {
       [afPyramideCanChallange cancelRequest];
       afPyramideCanChallange.delegate = self;
       afPyramideCanChallange = nil;
    }

    afPyramideCanChallange = [[AFNetworkUtility alloc] init];
    afPyramideCanChallange.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];
    
    NSMutableDictionary *dictCanChallange = [[NSMutableDictionary alloc] init];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];
    NSString *strCompetitionID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:COMPETITIONID]];
    
    [dictCanChallange setValue:strCompetitionID forKey:@"competition_id"];
    
    if ([strSelectLang isEqualToString:@""]) {
        [dictCanChallange setValue:@"english" forKey:@"lang"];
    }else {
        [dictCanChallange setValue:strSelectLang forKey:@"lang"];
    }
        
    NSLog(@"CanChallangePARAM ::: %@",dictCanChallange);

    if ([strAction isEqualToString:@"CanChallange"]) {
         [afPyramideCanChallange requestWithUrl:GetICanChallenge_Url param:dictCanChallange andToken:strAPIKey];
    }else {
         [afPyramideCanChallange requestWithUrl:GetCanBeChallenge_Url param:dictCanChallange andToken:strAPIKey];
    }
}

- (IBAction)onClickCanChallangeBtn:(id)sender {
    isChallange = YES;
    isChallangeBy = NO;
    
    viewChallange.hidden = NO;
    viewChallangeBy.hidden = YES;
    
    [self getCanChallengeServiceCall:@"CanChallange"];
}

- (IBAction)onClickCanBeChallangeByBtn:(id)sender {
    isChallange = NO;
    isChallangeBy = YES;
    
    viewChallange.hidden = YES;
    viewChallangeBy.hidden = NO;
    
    [self getCanChallengeServiceCall:@"CanBeChallange"];
}

#pragma mark -
#pragma mark - TableView Delegate Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrCanChallangerList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"ChallangeListCell";

    ChallangeListCell  *cell = (ChallangeListCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChallangeListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    cell.selectionStyle  = UITableViewCellSelectionStyleNone;

    if (isChallange == YES) {
        cell.btnChallenge.hidden = NO;
        cell.btnPlayerStatistics.hidden = NO;
        cell.btnPlayerStatisticsBy.hidden = YES;
    }else {
        cell.btnChallenge.hidden = YES;
        cell.btnPlayerStatistics.hidden = YES;
        cell.btnPlayerStatisticsBy.hidden = NO;
    }
    
    if ([arrCanChallangerList count] > 0) {
        ChallangersList *objChallanger = [arrCanChallangerList objectAtIndex:indexPath.row];
        [cell getCanChallangerDataList:objChallanger];
    }
    
    cell.delegate = self;
    cell.index = indexPath.row;

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark -
#pragma mark - Challenge Action Delegate

-(void)setChallangeInfoAction:(NSString *)strAction AtIndex:(NSInteger)index {
    
    if ([strAction isEqualToString:@"Staticstic"]) {
        
        ChallangersList *objChallanger = [arrCanChallangerList objectAtIndex:index];
        
        PlayerDetailPopUpViewController *viewPlayerDetail = [[PlayerDetailPopUpViewController alloc] initWithNibName:@"PlayerDetailPopUpViewController" bundle:nil];
        viewPlayerDetail.delegate = self;
        viewPlayerDetail.objChallanger = objChallanger;
        [self presentPopupViewController:viewPlayerDetail animationType:MJPopupViewAnimationFade];
    }else {
        AcceptChallengePopUpViewController *viewAcceptChallenge = [[AcceptChallengePopUpViewController alloc] initWithNibName:@"AcceptChallengePopUpViewController" bundle:nil];
        viewAcceptChallenge.delegate = self;
        viewAcceptChallenge.index = index;
        [self presentPopupViewController:viewAcceptChallenge animationType:MJPopupViewAnimationFade];
    }
}

-(void)setPlayerDetailInfoAction:(NSString *)strAction {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)setAcceptChallengeAction:(NSString *)strAction atIndex:(NSInteger)index {
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    if (afPyramideChallenge) {
       [afPyramideChallenge cancelRequest];
       afPyramideChallenge.delegate = self;
       afPyramideChallenge = nil;
    }

    afPyramideChallenge = [[AFNetworkUtility alloc] init];
    afPyramideChallenge.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];
    
    NSMutableDictionary *dictChallange = [[NSMutableDictionary alloc] init];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];
    NSString *strUserID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:USERID]];
    NSString *strCompetitionID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:COMPETITIONID]];
    NSString *strUserRanking = [NSString stringWithFormat:@"%@",[userDefault objectForKey:USERRANKING]];
    
    ChallangersList *objChallanger = [arrCanChallangerList objectAtIndex:index];
    
    [dictChallange setValue:strCompetitionID forKey:@"competition_id"];
    
    [dictChallange setValue:strUserID forKey:@"challenger_id"];
    [dictChallange setValue:strUserRanking forKey:@"challenger_place_before_game"];
    [dictChallange setValue:objChallanger.player_row_id forKey:@"challenged_to"];
    [dictChallange setValue:objChallanger.player_position forKey:@"challenged_to_place_before_game"];
    
    if ([strSelectLang isEqualToString:@""]) {
        [dictChallange setValue:@"english" forKey:@"lang"];
    }else {
        [dictChallange setValue:strSelectLang forKey:@"lang"];
    }
        
    NSLog(@"ChallangePARAM ::: %@",dictChallange);

    [afPyramideChallenge requestWithUrl:ChallengeCreate_Url param:dictChallange andToken:strAPIKey];
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataSuccess:(NSMutableDictionary *)dictResponce {
        
    if (utility == afPyramideCanChallange) {
        
        NSLog(@"CANCHALLANGERESPONCE ::: %@", dictResponce);
        
        [SHARED_APPDELEGATE hideLoadingView];
                        
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            
            NSArray *arrChallCan = [dictResponce safeObjectForKey:@"data"];
            
            if ([arrChallCan count] > 0) {
                
                tblChallangers.hidden = NO;
                viewRecoardNotFound.hidden = YES;
                
                for (NSDictionary *dictCanChallange in arrChallCan) {
                    
                    ChallangersList *objChallanger = [[ChallangersList alloc] init];
                    
                    objChallanger.player_row_id = [dictCanChallange safeObjectForKey:@"player_row_id"];
                    objChallanger.player_name = [dictCanChallange safeObjectForKey:@"player_name"];
                    objChallanger.player_position = [dictCanChallange safeObjectForKey:@"player_position"];
                    objChallanger.player_efficiency = [dictCanChallange safeObjectForKey:@"player_efficiency"];
                    objChallanger.player_is_challenge = [dictCanChallange safeObjectForKey:@"player_is_challenge"];
                    
                    if (isChallange == YES) {
                        objChallanger.player_challenged_stattus = [dictCanChallange safeObjectForKey:@"player_challenged_stattus"];
                    }
                    
                    objChallanger.player_status_color = [dictCanChallange safeObjectForKey:@"player_status_color"];
                    objChallanger.player_status = [dictCanChallange safeObjectForKey:@"player_status"];
                    objChallanger.player_profile = [dictCanChallange safeObjectForKey:@"player_profile"];
                    objChallanger.player_born_year = [dictCanChallange safeObjectForKey:@"player_born_year"];
   
                    [arrCanChallangerList addObject:objChallanger];
                }
                [tblChallangers reloadData];
            }else {
                if ([arrCanChallangerList count] == 0) {
                    tblChallangers.hidden = YES;
                    viewRecoardNotFound.hidden = NO;
                }
            }
        }else {
            if ([arrCanChallangerList count] == 0) {
                tblChallangers.hidden = YES;
                viewRecoardNotFound.hidden = NO;
                //[CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
            }
        }
    }else if (utility == afPyramideChallenge) {
        
        NSLog(@"CHALLENGERESPONCE ::: %@", dictResponce);
        
        [SHARED_APPDELEGATE hideLoadingView];
                        
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            [CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
        }
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Fail_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataFail:(NSError *)error {
    [SHARED_APPDELEGATE hideLoadingView];
    NSString *strError = [NSString stringWithFormat:@"%ld",(long)error.code];
    [CommonClass showAlertWithTitle:provideAlert andMessage:strError delegate:self];
}

@end
