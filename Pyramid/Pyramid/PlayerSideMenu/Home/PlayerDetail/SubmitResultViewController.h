//
//  SubmitResultViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/12/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyImage.h"
#import "AFNetworkUtility.h"

NS_ASSUME_NONNULL_BEGIN

@interface SubmitResultViewController : UIViewController <AFNetworkUtilityDelegate> {
    
    IBOutlet UIView *viewMedia;
    
    IBOutlet UITextField *txtPOneSbSetOne1;
    IBOutlet UITextField *txtPOneSbSetOne2;
    IBOutlet UITextField *txtPOneSbSetTwo1;
    IBOutlet UITextField *txtPOneSbSetTwo2;
    IBOutlet UITextField *txtPOneSbSetThree1;
    IBOutlet UITextField *txtPOneSbSetThree2;
    IBOutlet UITextField *txtPOneSbSetFour1;
    IBOutlet UITextField *txtPOneSbSetFour2;
    IBOutlet UITextField *txtPOneSbSetFifth1;
    IBOutlet UITextField *txtPOneSbSetFifth2;
    IBOutlet UITextField *txtPTwoSbSetOne1;
    IBOutlet UITextField *txtPTwoSbSetOne2;
    IBOutlet UITextField *txtPTwoSbSetTwo1;
    IBOutlet UITextField *txtPTwoSbSetTwo2;
    IBOutlet UITextField *txtPTwoSbSetThree1;
    IBOutlet UITextField *txtPTwoSbSetThree2;
    IBOutlet UITextField *txtPTwoSbSetFour1;
    IBOutlet UITextField *txtPTwoSbSetFour2;
    IBOutlet UITextField *txtPTwoSbSetFifth1;
    IBOutlet UITextField *txtPTwoSbSetFifth2;
    IBOutlet UITextField *txtPOneFinalScore;
    IBOutlet UITextField *txtPTwoFinalScore;
    
    IBOutlet AsyImage *imgMedia;
    
    IBOutlet UILabel *lblPOne;
    IBOutlet UILabel *lblPTwo;
    IBOutlet UILabel *lblSet1;
    IBOutlet UILabel *lblSet2;
    IBOutlet UILabel *lblSet3;
    IBOutlet UILabel *lblSet4;
    IBOutlet UILabel *lblSet5;
    IBOutlet UILabel *lblNote;
    
    AFNetworkUtility *afPyramideSubmit;
}

@property (nonatomic , strong) NSString *strPOneName;
@property (nonatomic , strong) NSString *strPTwoName;

@end

NS_ASSUME_NONNULL_END
