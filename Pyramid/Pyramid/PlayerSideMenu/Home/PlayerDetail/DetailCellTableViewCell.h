//
//  DetailCellTableViewCell.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol DetailCellTableViewCellDelegate <NSObject>

-(void)setChangeStatusAtIndex:(NSInteger)index;

@end

@interface DetailCellTableViewCell : UITableViewCell {
    
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblInfo;
    
    IBOutlet NSLayoutConstraint *contLblHeight;
    
    CGFloat cellHeight;
}

@property (nonatomic , strong) IBOutlet UIButton *btnEditStatus;

//Delegate
@property (nonatomic , strong) id<DetailCellTableViewCellDelegate> delegate;

//Integer
@property (nonatomic) NSInteger index;

-(CGFloat )getCellHeight;

-(void)getPlayerDetailInfo:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
