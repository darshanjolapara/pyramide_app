//
//  SubmitResultViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/12/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "SubmitResultViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"

@interface SubmitResultViewController ()

@end

@implementation SubmitResultViewController

@synthesize strPOneName;
@synthesize strPTwoName;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.titleView = [SHARED_APPDELEGATE getNavigationCenterWithTitle:NavigationSubmitResult fontSize:18];
    
    UIImage *backButtonImage = [UIImage imageNamed:@"Back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton setImage:backButtonImage
                forState:UIControlStateNormal];
    
    backButton.frame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self
                   action:@selector(popViewController)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
    txtPOneSbSetOne1.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetOne1.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetOne1.leftViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetOne1.rightViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetOne1.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPOneSbSetOne2.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetOne2.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetOne2.leftViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetOne2.rightViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetOne2.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPOneSbSetTwo1.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetTwo1.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetTwo1.leftViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetTwo1.rightViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetTwo1.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPOneSbSetTwo2.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetTwo2.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetTwo2.leftViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetTwo2.rightViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetTwo2.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPOneSbSetThree1.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetThree1.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetThree1.leftViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetThree1.rightViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetThree1.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPOneSbSetThree2.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetThree2.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetThree2.leftViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetThree2.rightViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetThree2.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPOneSbSetFour1.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetFour1.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetFour1.leftViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetFour1.rightViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetFour1.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPOneSbSetFour2.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetFour2.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetFour2.leftViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetFour2.rightViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetFour2.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPOneSbSetFifth1.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetFifth1.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetFifth1.leftViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetFifth1.rightViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetFifth1.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPOneSbSetFifth2.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetFifth2.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneSbSetFifth2.leftViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetFifth2.rightViewMode = UITextFieldViewModeAlways;
    txtPOneSbSetFifth2.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    //Secound_Player
    
    txtPTwoSbSetOne1.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetOne1.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetOne1.leftViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetOne1.rightViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetOne1.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPTwoSbSetOne2.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetOne2.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetOne2.leftViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetOne2.rightViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetOne2.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPTwoSbSetTwo1.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetTwo1.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetTwo1.leftViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetTwo1.rightViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetTwo1.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPTwoSbSetTwo2.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetTwo2.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetTwo2.leftViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetTwo2.rightViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetTwo2.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPTwoSbSetThree1.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetThree1.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetThree1.leftViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetThree1.rightViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetThree1.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPTwoSbSetThree2.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetThree2.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetThree2.leftViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetThree2.rightViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetThree2.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPTwoSbSetFour1.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetFour1.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetFour1.leftViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetFour1.rightViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetFour1.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPTwoSbSetFour2.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetFour2.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetFour2.leftViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetFour2.rightViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetFour2.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPTwoSbSetFifth1.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetFifth1.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetFifth1.leftViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetFifth1.rightViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetFifth1.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    txtPTwoSbSetFifth2.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetFifth2.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoSbSetFifth2.leftViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetFifth2.rightViewMode = UITextFieldViewModeAlways;
    txtPTwoSbSetFifth2.layer.borderColor = appTextFieldBoardColor.CGColor;

    txtPOneFinalScore.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneFinalScore.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPOneFinalScore.leftViewMode = UITextFieldViewModeAlways;
    txtPOneFinalScore.rightViewMode = UITextFieldViewModeAlways;
    txtPOneFinalScore.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtPOneFinalScore.attributedPlaceholder = [[NSAttributedString alloc]
       initWithString:[NSString stringWithFormat:providePOneScore,strPOneName]
                   attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    txtPTwoFinalScore.leftView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoFinalScore.rightView = [SHARED_APPDELEGATE getTextFieldLeftAndRightView];
    txtPTwoFinalScore.leftViewMode = UITextFieldViewModeAlways;
    txtPTwoFinalScore.rightViewMode = UITextFieldViewModeAlways;
    txtPTwoFinalScore.layer.borderColor = appTextFieldBoardColor.CGColor;
    txtPTwoFinalScore.attributedPlaceholder = [[NSAttributedString alloc]
       initWithString:[NSString stringWithFormat:providePOneScore,strPTwoName]
                   attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    viewMedia.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    lblPOne.text = [NSString stringWithFormat:@"%@'s Scoreboard",strPOneName];
    lblPTwo.text = [NSString stringWithFormat:@"%@'s Scoreboard",strPTwoName];
    
    lblSet1.text = @"1 Set";
    NSRange range = [lblSet1.text rangeOfString:@"Set"];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:lblSet1.text];
    [attributedText setAttributes:@{NSFontAttributeName:FONT_Bold(12)} range:range];
    lblSet1.attributedText = attributedText;
    
    lblSet2.text = @"2 Set";
    NSRange range1 = [lblSet2.text rangeOfString:@"Set"];
    NSMutableAttributedString *attributedText1 = [[NSMutableAttributedString alloc] initWithString:lblSet2.text];
    [attributedText1 setAttributes:@{NSFontAttributeName:FONT_Bold(12)} range:range1];
    lblSet2.attributedText = attributedText1;
    
    lblSet3.text = @"3 Set";
    NSRange range2 = [lblSet3.text rangeOfString:@"Set"];
    NSMutableAttributedString *attributedText2 = [[NSMutableAttributedString alloc] initWithString:lblSet3.text];
    [attributedText2 setAttributes:@{NSFontAttributeName:FONT_Bold(12)} range:range2];
    lblSet3.attributedText = attributedText2;
    
    lblSet4.text = @"4 Set";
    NSRange range3 = [lblSet4.text rangeOfString:@"Set"];
    NSMutableAttributedString *attributedText3 = [[NSMutableAttributedString alloc] initWithString:lblSet4.text];
    [attributedText3 setAttributes:@{NSFontAttributeName:FONT_Bold(12)} range:range3];
    lblSet4.attributedText = attributedText3;
    
    lblSet5.text = @"5 Set";
    NSRange range4 = [lblSet5.text rangeOfString:@"Set"];
    NSMutableAttributedString *attributedText4 = [[NSMutableAttributedString alloc] initWithString:lblSet5.text];
    [attributedText4 setAttributes:@{NSFontAttributeName:FONT_Bold(12)} range:range4];
    lblSet5.attributedText = attributedText4;
    
    imgMedia.hidden = YES;
}

#pragma mark -
#pragma mark - Pop To View Mothod

-(void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickUploadPictureBtn:(id)sender {
    
}

- (IBAction)onClickUploadVideoBtn:(id)sender {
    
}

- (IBAction)onClickSubmitBtn:(id)sender {
 
    if (afPyramideSubmit) {
       [afPyramideSubmit cancelRequest];
       afPyramideSubmit.delegate = self;
       afPyramideSubmit = nil;
    }

    afPyramideSubmit = [[AFNetworkUtility alloc] init];
    afPyramideSubmit.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];
    
    NSMutableDictionary *dictSubmit = [[NSMutableDictionary alloc] init];
        
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];

    if ([[CommonClass trimString:txtPOneSbSetOne1.text] length] == 0) {
        [dictSubmit setValue:@"" forKey:@"score_1_1"];
        [dictSubmit setValue:@"" forKey:@"score_1_2"];
        [dictSubmit setValue:@"" forKey:@"score_1_3"];
        [dictSubmit setValue:@"" forKey:@"score_1_4"];
        [dictSubmit setValue:@"" forKey:@"score_1_5"];
        [dictSubmit setValue:@"" forKey:@"score_1_6"];
        [dictSubmit setValue:@"" forKey:@"score_1_7"];
        [dictSubmit setValue:@"" forKey:@"score_1_8"];
        [dictSubmit setValue:@"" forKey:@"score_1_9"];
        [dictSubmit setValue:@"" forKey:@"score_1_10"];
    }else {
        
    }
    
    if ([[CommonClass trimString:txtPOneSbSetOne2.text] length] == 0) {
        
    }else {
        
    }
    
    if ([[CommonClass trimString:txtPOneSbSetTwo1.text] length] == 0) {
       
    }else {
        
    }
    
    if ([[CommonClass trimString:txtPOneSbSetTwo2.text] length] == 0) {
       
    }else {
        
    }
    
    if ([[CommonClass trimString:txtPOneSbSetThree1.text] length] == 0) {
       
    }else {
        
    }
    
    if ([[CommonClass trimString:txtPOneSbSetThree2.text] length] == 0) {
       
    }else {
        
    }
    
    if ([[CommonClass trimString:txtPOneSbSetFour1.text] length] == 0) {
       
    }else {
        
    }
    
    if ([[CommonClass trimString:txtPOneSbSetFour2.text] length] == 0) {
       
    }else {
        
    }
    
    if ([[CommonClass trimString:txtPOneSbSetFifth1.text] length] == 0) {
       
    }else {
        
    }
    
    if ([[CommonClass trimString:txtPOneSbSetFifth2.text] length] == 0) {
       
    }else {
        
    }
    

    [dictSubmit setValue:@"" forKey:@"score_2_1"];
    [dictSubmit setValue:@"" forKey:@"score_2_2"];
    [dictSubmit setValue:@"" forKey:@"score_2_3"];
    [dictSubmit setValue:@"" forKey:@"score_2_4"];
    [dictSubmit setValue:@"" forKey:@"score_2_5"];
    [dictSubmit setValue:@"" forKey:@"score_2_6"];
    [dictSubmit setValue:@"" forKey:@"score_2_7"];
    [dictSubmit setValue:@"" forKey:@"score_2_8"];
    [dictSubmit setValue:@"" forKey:@"score_2_9"];
    [dictSubmit setValue:@"" forKey:@"score_2_10"];
    
    if ([strSelectLang isEqualToString:@""]) {
        [dictSubmit setValue:@"english" forKey:@"lang"];
    }else {
        [dictSubmit setValue:strSelectLang forKey:@"lang"];
    }
    
    NSLog(@"SubmitPARAM ::: %@",dictSubmit);

    [afPyramideSubmit requestWithUrl:UpdateChallenge_Url param:dictSubmit andToken:strAPIKey];
}

- (IBAction)onClickCancelBtn:(id)sender {
    [self popViewController];
}

@end
