//
//  ChangeStatusPopUpViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "ChangeStatusPopUpViewController.h"
#import "AppDelegate.h"

@interface ChangeStatusPopUpViewController ()

@end

@implementation ChangeStatusPopUpViewController

@synthesize delegate;
@synthesize strStatusTitle;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    btnSelectStatus.layer.borderColor = appTextFieldBoardColor.CGColor;
    
    [btnSelectStatus setTitle:strStatusTitle forState:UIControlStateNormal];
}

- (IBAction)onClickSelectStatusBtn:(id)sender {
        
    NSMutableArray *arrStatusList = [[NSMutableArray alloc] initWithObjects:@"Available for challenge",@"Not available (Vacation, injury)", nil];
    
    if (dropDown == nil) {
        
        CGFloat f = 0;
        
        if ([arrStatusList count] <= 5) {
            f = [arrStatusList count] * 40;
        }else {
            f = 200.0f;
        }

        self.view.frame = CGRectMake((SHARED_APPDELEGATE.window.frame.size.width - 320)/2, (SHARED_APPDELEGATE.window.frame.size.height/2) - ((f/2) + 49), 320, f + 98);
        
       dropDown = [[NIDropDown alloc] showDropDown:sender :&f :arrStatusList :nil :@"down" atAction:@""];
       dropDown.delegate = self;
    }else{
      [dropDown hideDropDown:sender];
      [self rel];
    }
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender atIndexPath:(NSInteger)index {
        
    NSLog(@"LanguageName: %@",btnSelectStatus.titleLabel.text);
    [self rel];
    
    self.view.frame = CGRectMake((SHARED_APPDELEGATE.window.frame.size.width - 320)/2, (SHARED_APPDELEGATE.window.frame.size.height/2) - 49, 320, 98);
    
    NSString *strStatus = @"";
    
    if ([btnSelectStatus.titleLabel.text isEqualToString:@"Available for challenge"]) {
        strStatus = @"available";
    }else if ([btnSelectStatus.titleLabel.text isEqualToString:@"Not available (Vacation, injury)"]) {
        strStatus = @"not_available";
    }
    
    if (delegate && [delegate respondsToSelector:@selector(selectStatus:AndAction:)]) {
        [delegate selectStatus:strStatus AndAction:@"ChangeStatus"];
    }
}

-(void)rel {
    dropDown = nil;
}

- (IBAction)onClickCloseBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(selectStatus:AndAction:)]) {
        [delegate selectStatus:@"" AndAction:@"Close"];
    }
}

@end
