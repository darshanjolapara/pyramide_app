//
//  ChallangersDetailViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChallangeListCell.h"
#import "PlayerDetailPopUpViewController.h"
#import "AcceptChallengePopUpViewController.h"
#import "ChallangersList.h"
#import "AFNetworkUtility.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChallangersDetailViewController : UIViewController <ChallangeListCellDelegate,PlayerDetailPopUpViewControllerDelegate,AcceptChallengePopUpViewControllerDelegate,AFNetworkUtilityDelegate> {
    
    IBOutlet UIView *viewRecoardNotFound;
    
    IBOutlet UITableView *tblChallangers;
    
    IBOutlet UIView *viewChallange;
    IBOutlet UIView *viewChallangeBy;
    
    BOOL isChallange;
    BOOL isChallangeBy;
    
    NSMutableArray *arrCanChallangerList;
    
    AFNetworkUtility *afPyramideCanChallange;
    AFNetworkUtility *afPyramideChallenge;
}

@property (nonatomic , strong) NSString *strNavTitle;

@end

NS_ASSUME_NONNULL_END
