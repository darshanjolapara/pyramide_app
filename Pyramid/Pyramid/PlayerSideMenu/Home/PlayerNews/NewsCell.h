//
//  NewsCell.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsList.h"
#import "AsyImage.h"
#import "MarqueeLabel.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewsCell : UITableViewCell {
    
    IBOutlet UIView *viewBack;
    
    IBOutlet UILabel *lblDate;
    IBOutlet UILabel *lblDetail;
    IBOutlet MarqueeLabel *lblTitle;
    
    IBOutlet AsyImage *imgPic;
    
    IBOutlet NSLayoutConstraint *contViewHeight;
    IBOutlet NSLayoutConstraint *contLblHeight;
    
    CGFloat cellHeight;
}

-(CGFloat )getCellHeight;
-(void)getPlayerNewsDataList:(NewsList *)objNewsList;

@end

NS_ASSUME_NONNULL_END
