//
//  NewsCell.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "NewsCell.h"

@implementation NewsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    viewBack.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)getPlayerNewsDataList:(NewsList *)objNewsList {
    
    if ([objNewsList.video_name isEqualToString:@""]) {
        NSString *imgProPic = [NSString stringWithFormat:@"%@",objNewsList.photo];

           if (imgProPic.length > 0) {
              [imgPic loadingImage:imgProPic placeHolderImage:USER_PLACEHOLDER_IMAGE];
           }else {
              [imgPic loadingImage:nil placeHolderImage:USER_PLACEHOLDER_IMAGE];
           }
    }else {
        [imgPic loadingImage:nil placeHolderImage:objNewsList.imgThumbnail];
    }
    
    lblDetail.text = [NSString stringWithFormat:@"%@",objNewsList.NewsDescription];
    
    lblTitle.text = [NSString stringWithFormat:@"%@",objNewsList.title];
    lblTitle.marqueeType = MLContinuous;
    lblTitle.textAlignment = NSTextAlignmentLeft;
    lblTitle.lineBreakMode = NSLineBreakByTruncatingHead;
    lblTitle.scrollDuration = 30.0f;
    lblTitle.fadeLength = 0.0f;
    
    lblDate.text = [NSString stringWithFormat:@"%@",objNewsList.displayDate];
    
    CGRect expectedLabelSize = [lblDetail.text boundingRectWithSize:CGSizeMake(lblDetail.frame.size.width, CGFLOAT_MAX)
    options:NSStringDrawingUsesLineFragmentOrigin
    attributes:@{NSFontAttributeName: FONT_OpenSans(13)}
    context:nil];
    
    //adjust the label the the new height.
    CGRect newFrame = lblDetail.frame;
    newFrame.size.height = expectedLabelSize.size.height;
    lblDetail.frame = newFrame;

    contLblHeight.constant = lblDetail.frame.size.height;
    
    NSLog(@"%f",contLblHeight.constant);
    
    if (contLblHeight.constant > 20) {
        contViewHeight.constant = contLblHeight.constant + 71;
        
        NSLog(@"%f",contViewHeight.constant);
        
        cellHeight = contViewHeight.constant + 10;
        
        NSLog(@"%f",cellHeight);
    }else {
        contLblHeight.constant = 20.0f;
        contViewHeight.constant = 91.0f;
        
        cellHeight = 101.0f;
    }
}

-(CGFloat )getCellHeight {
    return cellHeight;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
