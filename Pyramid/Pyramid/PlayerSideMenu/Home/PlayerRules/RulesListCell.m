//
//  RulesListCell.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "RulesListCell.h"

@implementation RulesListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)getPlayerRulesDataList:(RulesList *)objRulesList {
    
    NSString *aux = [NSString stringWithFormat:@"<span style=\"font-family: OpenSans; font-size: 14\">%@</span>", objRulesList.ruleDescription];
    
    NSDictionary *options = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
    NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)};

    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithData:[aux dataUsingEncoding:NSUTF8StringEncoding] options:options documentAttributes:nil error:nil];
    
    lblRuleDetail.attributedText = attrString;
    
//    lblRuleDetail.text = [NSString stringWithFormat:@"%@",objRulesList.ruleDescription];
    lblRuleCount.text = [NSString stringWithFormat:@"%@)",objRulesList.ruleCount];
    
    CGRect expectedLabelSize = [lblRuleDetail.text boundingRectWithSize:CGSizeMake(lblRuleDetail.frame.size.width, CGFLOAT_MAX)
    options:NSStringDrawingUsesLineFragmentOrigin
    attributes:@{NSFontAttributeName: FONT_OpenSans(14)}
    context:nil];
    
    //adjust the label the the new height.
    CGRect newFrame = lblRuleDetail.frame;
    newFrame.size.height = expectedLabelSize.size.height;
    lblRuleDetail.frame = newFrame;

    contLblHeight.constant = lblRuleDetail.frame.size.height;
    
    NSLog(@"%f",contLblHeight.constant);
    
    if (contLblHeight.constant > 20) {
        cellHeight = contLblHeight.constant + 30;
        NSLog(@"%f",cellHeight);
    }else {
        contLblHeight.constant = 20.0f;
        cellHeight = 44.0f;
    }
}

-(CGFloat )getCellHeight {
    return cellHeight;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
