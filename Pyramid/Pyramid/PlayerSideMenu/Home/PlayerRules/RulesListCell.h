//
//  RulesListCell.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/7/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RulesList.h"

NS_ASSUME_NONNULL_BEGIN

@interface RulesListCell : UITableViewCell {
    
    IBOutlet UILabel *lblRuleCount;
    IBOutlet UILabel *lblRuleDetail;
    
    IBOutlet NSLayoutConstraint *contLblHeight;
    
    CGFloat cellHeight;
}

-(CGFloat )getCellHeight;
-(void)getPlayerRulesDataList:(RulesList *)objRulesList;

@end

NS_ASSUME_NONNULL_END
