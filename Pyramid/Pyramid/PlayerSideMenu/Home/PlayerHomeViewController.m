
//
//  PlayerHomeViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "PlayerHomeViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"

@interface PlayerHomeViewController ()

@end

@implementation PlayerHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
        
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.navigationItem.titleView = [SHARED_APPDELEGATE getNavigationCenterWithTitle:NavigationHome fontSize:18];
    
    btnSelectCompetition.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [collectionMenu registerNib:[UINib nibWithNibName:@"MenuListCell" bundle:nil] forCellWithReuseIdentifier:@"MenuListCell"];

    arrMenuList = [[NSMutableArray alloc] initWithObjects:@"Details",@"Ranking",@"News",@"Game page",@"Staticstic",@" Rules", nil];
    
    intIndex = 0;
    
    isRanking = NO;
    isNews = NO;
    isGamePage = NO;
    isStaticstic = NO;
    isRules = NO;
    isDetailShow = NO;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strCompetitionName = [userDefault objectForKey:COMPETITIONNAME];
    
    if (strCompetitionName == nil) {
        [btnSelectCompetition setTitle:@"Select Competition" forState:UIControlStateNormal];
    }else {
        [btnSelectCompetition setTitle:strCompetitionName forState:UIControlStateNormal];
    }
    
    constHeightSearchView.constant = 0.0f;
    
    arrDetailList = [[NSMutableArray alloc] init];
        
    [self getPlayerCompetitionsServiceCall];
}

- (IBAction)onClickSelectCompetitionBtn:(id)sender {

    if (dropDown == nil) {
        
        CGFloat f = 0;
        if ([arrCompetitionList count] <= 5) {
            f = [arrCompetitionList count] * 40;
        }else {
            f = 200;
        }
       dropDown = [[NIDropDown alloc] showDropDown:sender :&f :arrCompetitionList :nil :@"down" atAction:@"Compatation"];
       dropDown.delegate = self;
    }else{
       [dropDown hideDropDown:sender];
       [self rel];
    }
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender atIndexPath:(NSInteger)index {
    NSLog(@"LanguageName: %@",btnSelectCompetition.titleLabel.text);
    NSLog(@"%ld",(long)index);
    
    intIndex = 0;
    
    tblDetailList.hidden = YES;
    tblOtherMenu.hidden = YES;
    
    CompetitionsList *objCompetition = [arrCompetitionList objectAtIndex:index];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:objCompetition.CompetitionsID forKey:COMPETITIONID];
    [userDefault setObject:objCompetition.name forKey:COMPETITIONNAME];
        
    [self setDetailViewInfo];
    [self getPlayerCompetitionsServiceCall];
    
    [collectionMenu reloadData];
    
    [collectionMenu setContentOffset:CGPointZero animated:YES];

    [self rel];
}

-(void)rel {
    dropDown = nil;
}

#pragma mark -
#pragma mark - COLLECTION_DELEGATE_AND_DATASOURCE_METHODS

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [arrMenuList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    MenuListCell * cell = (MenuListCell *)[cv dequeueReusableCellWithReuseIdentifier:@"MenuListCell" forIndexPath:indexPath];
        
    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[arrMenuList objectAtIndex:indexPath.row]];
    
    if (indexPath.row == intIndex) {
        cell.viewLine.hidden = NO;
        cell.lblTitle.textColor = appNavBarColor;
    }else {
        cell.viewLine.hidden = YES;
        cell.lblTitle.textColor = [UIColor blackColor];
    }
    
    if (indexPath.row == 5) {
        cell.lblTitle.textAlignment = NSTextAlignmentLeft;
    }else {
        cell.lblTitle.textAlignment = NSTextAlignmentCenter;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    intIndex = indexPath.row;
    
    tblDetailList.hidden = YES;
    tblOtherMenu.hidden = YES;
    
    viewAdd.hidden = NO;
    viewRecoardNotFound.hidden = YES;
    
    if (indexPath.row == 0) {
        [self setDetailViewInfo];
        [self getPlayerCompetitionDetailServiceCall];
    }else if (indexPath.row == 1) {
        //Ranking
        txtSearchPlayer.attributedPlaceholder = [[NSAttributedString alloc]
        initWithString:@"Search player name here.."
                    attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        
        isRanking = YES;
        isNews = NO;
        isGamePage = NO;
        isStaticstic = NO;
        isRules = NO;
        isDetailShow = NO;

        viewDisplayBtn.hidden = NO;
                
        [btnMoreDetail setTitle:@"Last Challanges" forState:UIControlStateNormal];
        [btnViewChallangers setTitle:@"Last Matches" forState:UIControlStateNormal];
                    
        btnAllGames.hidden = YES;
        btnMoreDetail.hidden = NO;
        btnViewChallangers.hidden = NO;
        
        constHeightBtnView.constant = 60.0f;
        constHeightSearchView.constant = 40.0f;
                
        [self getRankingAndNewsCompetitionPlayerServiceCall:@"Ranking" searchText:@""];
    }else if (indexPath.row == 2) {
        //News
        txtSearchPlayer.attributedPlaceholder = [[NSAttributedString alloc]
            initWithString:@"Search news here.."
                        attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        isRanking = NO;
        isNews = YES;
        isGamePage = NO;
        isStaticstic = NO;
        isRules = NO;
        isDetailShow = NO;

        viewDisplayBtn.hidden = YES;

        constHeightBtnView.constant = 0.0f;
        constHeightSearchView.constant = 40.0f;
        
        [self getRankingAndNewsCompetitionPlayerServiceCall:@"News" searchText:@""];
    }else if (indexPath.row == 3) {
        //Game Page
        txtSearchPlayer.attributedPlaceholder = [[NSAttributedString alloc]
        initWithString:@"Search for player name.."
                    attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        
        isRanking = NO;
        isNews = NO;
        isGamePage = YES;
        isStaticstic = NO;
        isRules = NO;
        isDetailShow = NO;
        
        btnAllGames.hidden = NO;
        btnMoreDetail.hidden = YES;
        btnViewChallangers.hidden = YES;
            
        [btnAllGames setTitle:@"All Games" forState:UIControlStateNormal];
        
        viewDisplayBtn.hidden = NO;
                    
        constHeightBtnView.constant = 60.0f;
        constHeightSearchView.constant = 40.0f;
        
        intCountPage = 1;
        
        [arrGamePageList removeAllObjects];
        arrGamePageList = [[NSMutableArray alloc] init];
        
        [self getPlayerGamePageServiceCall:intCountPage atAction:@"GamePage" searchText:@""];
    }else if (indexPath.row == 4) {
        //Staticstic
        txtSearchPlayer.attributedPlaceholder = [[NSAttributedString alloc]
        initWithString:@"Search for player name.."
                    attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        
        isRanking = NO;
        isNews = NO;
        isGamePage = NO;
        isStaticstic = YES;
        isRules = NO;
        isDetailShow = NO;
                
        viewDisplayBtn.hidden = NO;
            
        constHeightBtnView.constant = 0.0f;
        constHeightSearchView.constant = 40.0f;

        intCountPage = 0;
        
        [arrStaticsticList removeAllObjects];
        arrStaticsticList = [[NSMutableArray alloc] init];
        
        [self getPlayerGamePageServiceCall:intCountPage atAction:@"Staticstic" searchText:@""];
    }else if (indexPath.row == 5) {
        //Rules
        isRanking = NO;
        isNews = NO;
        isGamePage = NO;
        isStaticstic = NO;
        isRules = YES;
        isDetailShow = NO;
                
        viewDisplayBtn.hidden = NO;
            
        constHeightBtnView.constant = 0.0f;
        constHeightSearchView.constant = 0.0f;

        [arrRulesList removeAllObjects];
        arrRulesList = [[NSMutableArray alloc] init];
        
        [self getPlayerRulesServiceCall];
    }
    
    [collectionView reloadData];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeMake(0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

//    return CGSizeMake(CGRectGetWidth(collectionView.frame), (CGRectGetHeight(collectionView.frame)));
    if (indexPath.row == 4) {
        return CGSizeMake(100, 41);
    }else if (indexPath.row == 5) {
        return CGSizeMake(55, 41);
    }else {
        return CGSizeMake(80, 41);
    }
}

//#pragma mark -
//#pragma mark - COLLECTION_FLOWLAYOUT_LANDSCAPE_AND_PORTRAIT
//
//- (void)viewWillLayoutSubviews {
//
//    [super viewWillLayoutSubviews];
//    UICollectionViewFlowLayout *flowLayout = (id)collectionMenu.collectionViewLayout;
//
//    float width = 80.0f;
//    float height =  41.0f;
//    NSLog(@"HEIGHT %f WIDTH %f", height , width);
//    flowLayout.itemSize = CGSizeMake(width, height);
//
//    [flowLayout invalidateLayout];
//}

-(void)setDetailViewInfo {
    //Details
    isRanking = NO;
    isNews = NO;
    isGamePage = NO;
    isStaticstic = NO;
    isRules = NO;
    isDetailShow = NO;
    
    viewDisplayBtn.hidden = NO;
    
    [btnMoreDetail setTitle:@"More Details" forState:UIControlStateNormal];
    [btnViewChallangers setTitle:@"View Challangers" forState:UIControlStateNormal];
    
    if (isChallangeRecoard == YES) {
        btnAllGames.hidden = NO;
        btnMoreDetail.hidden = YES;
        btnViewChallangers.hidden = YES;
    }else {
        btnAllGames.hidden = YES;
        btnMoreDetail.hidden = NO;
        btnViewChallangers.hidden = NO;
    }
    
    constHeightBtnView.constant = 60.0f;
    constHeightSearchView.constant = 0.0f;
}

-(void)getPlayerCompetitionsServiceCall {

    if (afPyramidePlayerCompetitions) {
       [afPyramidePlayerCompetitions cancelRequest];
       afPyramidePlayerCompetitions.delegate = self;
       afPyramidePlayerCompetitions = nil;
    }

    afPyramidePlayerCompetitions = [[AFNetworkUtility alloc] init];
    afPyramidePlayerCompetitions.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];
    
    NSMutableDictionary *dictPlayerComp = [[NSMutableDictionary alloc] init];
        
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];
    
    if ([strSelectLang isEqualToString:@""]) {
        [dictPlayerComp setValue:@"english" forKey:@"lang"];
    }else {
        [dictPlayerComp setValue:strSelectLang forKey:@"lang"];
    }
    
    NSLog(@"PlayerComPARAM ::: %@",dictPlayerComp);

    [afPyramidePlayerCompetitions requestWithUrl:GetPlayerCompetitions_Url param:dictPlayerComp andToken:strAPIKey];
}

-(void)getIAmInChallengeServiceCall {
    
    if (afPyramideInChallenge) {
       [afPyramideInChallenge cancelRequest];
       afPyramideInChallenge.delegate = self;
       afPyramideInChallenge = nil;
    }

    afPyramideInChallenge = [[AFNetworkUtility alloc] init];
    afPyramideInChallenge.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];
    
    NSMutableDictionary *dictComPlayerDetail = [[NSMutableDictionary alloc] init];
        
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];
    NSString *strCompetitionID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:COMPETITIONID]];
    
    if (![strCompetitionID isEqualToString:@"(null)"]) {
         [dictComPlayerDetail setValue:strCompetitionID forKey:@"competition_id"];
    }else {
        if ([arrCompetitionList count] > 0) {
            CompetitionsList *objCompetitions = [arrCompetitionList firstObject];
            
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:objCompetitions.CompetitionsID forKey:COMPETITIONID];
            [userDefault setObject:objCompetitions.name forKey:COMPETITIONNAME];
            
            [btnSelectCompetition setTitle:objCompetitions.name forState:UIControlStateNormal];
            [dictComPlayerDetail setValue:objCompetitions.CompetitionsID forKey:@"competition_id"];
        }
    }
    
    if ([strSelectLang isEqualToString:@""]) {
        [dictComPlayerDetail setValue:@"english" forKey:@"lang"];
    }else {
        [dictComPlayerDetail setValue:strSelectLang forKey:@"lang"];
    }
    
    NSLog(@"ComPlayerPARAM ::: %@",dictComPlayerDetail);

    [afPyramideInChallenge requestWithUrl:GetIAmChallenge_Url param:dictComPlayerDetail andToken:strAPIKey];
}

-(void)getPlayerCompetitionDetailServiceCall {
    
    [arrDetailList removeAllObjects];
    arrDetailList = [[NSMutableArray alloc] init];
    
    if (afPyramideCompetitionPlayerDetail) {
       [afPyramideCompetitionPlayerDetail cancelRequest];
       afPyramideCompetitionPlayerDetail.delegate = self;
       afPyramideCompetitionPlayerDetail = nil;
    }

    afPyramideCompetitionPlayerDetail = [[AFNetworkUtility alloc] init];
    afPyramideCompetitionPlayerDetail.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];
    
    NSMutableDictionary *dictComPlayerDetail = [[NSMutableDictionary alloc] init];
        
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];
    NSString *strPlayerID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:USERID]];
    NSString *strCompetitionID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:COMPETITIONID]];
    
    if (![strCompetitionID isEqualToString:@"(null)"]) {
         [dictComPlayerDetail setValue:strCompetitionID forKey:@"competition_id"];
    }else {
        if ([arrCompetitionList count] > 0) {
            CompetitionsList *objCompetitions = [arrCompetitionList firstObject];
            
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:objCompetitions.CompetitionsID forKey:COMPETITIONID];
            [userDefault setObject:objCompetitions.name forKey:COMPETITIONNAME];
            
            [btnSelectCompetition setTitle:objCompetitions.name forState:UIControlStateNormal];
            [dictComPlayerDetail setValue:objCompetitions.CompetitionsID forKey:@"competition_id"];
        }

    }
    [dictComPlayerDetail setValue:strPlayerID forKey:@"player_id"];
    
    if ([strSelectLang isEqualToString:@""]) {
        [dictComPlayerDetail setValue:@"english" forKey:@"lang"];
    }else {
        [dictComPlayerDetail setValue:strSelectLang forKey:@"lang"];
    }
    
    NSLog(@"ComPlayerPARAM ::: %@",dictComPlayerDetail);

    [afPyramideCompetitionPlayerDetail requestWithUrl:GetPlayerCompetition_Url param:dictComPlayerDetail andToken:strAPIKey];
}

-(void)UpdatePlayerCompatitionStatusServiceCall:(NSString *)strStatus {
    
    if (afPyramideUpdatePlayerStatus) {
       [afPyramideUpdatePlayerStatus cancelRequest];
       afPyramideUpdatePlayerStatus.delegate = self;
       afPyramideUpdatePlayerStatus = nil;
    }

    afPyramideUpdatePlayerStatus = [[AFNetworkUtility alloc] init];
    afPyramideUpdatePlayerStatus.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];
    
    NSMutableDictionary *dictStatusUpdate = [[NSMutableDictionary alloc] init];
        
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];
    NSString *strCompetitionID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:COMPETITIONID]];
    NSString *strUserID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:USERID]];
        
    [dictStatusUpdate setValue:strCompetitionID forKey:@"competition_id"];
    [dictStatusUpdate setValue:strUserID forKey:@"player_id"];
    [dictStatusUpdate setValue:strStatus forKey:@"competition_player_status"];
    
    if ([strSelectLang isEqualToString:@""]) {
        [dictStatusUpdate setValue:@"english" forKey:@"lang"];
    }else {
        [dictStatusUpdate setValue:strSelectLang forKey:@"lang"];
    }
    
    NSLog(@"StatusUpdatePARAM ::: %@",dictStatusUpdate);

    [afPyramideUpdatePlayerStatus requestWithUrl:UpdatePlayerStatus_Url param:dictStatusUpdate andToken:strAPIKey];
}

- (void)getRankingAndNewsCompetitionPlayerServiceCall:(NSString *)strAction searchText:(NSString *)strFind {
    
    if (afPyramidePlayerRanking) {
       [afPyramidePlayerRanking cancelRequest];
       afPyramidePlayerRanking.delegate = self;
       afPyramidePlayerRanking = nil;
    }

    afPyramidePlayerRanking = [[AFNetworkUtility alloc] init];
    afPyramidePlayerRanking.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];
    
    NSMutableDictionary *dictRanking = [[NSMutableDictionary alloc] init];
        
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];
    NSString *strCompetitionID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:COMPETITIONID]];
    
    [dictRanking setValue:strCompetitionID forKey:@"competition_id"];
    [dictRanking setValue:strFind forKey:@"find"];
    
    if ([strSelectLang isEqualToString:@""]) {
        [dictRanking setValue:@"english" forKey:@"lang"];
    }else {
        [dictRanking setValue:strSelectLang forKey:@"lang"];
    }
    
    NSLog(@"RankingPARAM ::: %@",dictRanking);

    if ([strAction isEqualToString:@"Ranking"]) {
        [arrRankingList removeAllObjects];
        arrRankingList = [[NSMutableArray alloc] init];
        
        [dictRanking setValue:@"1" forKey:@"skip_limit"];
        
        [afPyramidePlayerRanking requestWithUrl:GetRankingCompetitionPlayer_Url param:dictRanking andToken:strAPIKey];
    }else {
        [arrNewsList removeAllObjects];
        arrNewsList = [[NSMutableArray alloc] init];
        
        [afPyramidePlayerRanking requestWithUrl:GetPlayerNews_Url param:dictRanking andToken:strAPIKey];
    }
}

-(void)getPlayerGamePageServiceCall:(int)intPage atAction:(NSString *)strAction searchText:(NSString *)strFind {
    
    if (afPyramidePlayerGamePage) {
       [afPyramidePlayerGamePage cancelRequest];
       afPyramidePlayerGamePage.delegate = self;
       afPyramidePlayerGamePage = nil;
    }

    afPyramidePlayerGamePage = [[AFNetworkUtility alloc] init];
    afPyramidePlayerGamePage.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];
    
    NSMutableDictionary *dictGamePage = [[NSMutableDictionary alloc] init];
        
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];
    NSString *strCompetitionID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:COMPETITIONID]];
       
    NSString *strCountPage = [NSString stringWithFormat:@"%d",intPage];
    
    [dictGamePage setValue:strCompetitionID forKey:@"competition_id"];
    [dictGamePage setValue:strFind forKey:@"find"];
    [dictGamePage setValue:strCountPage forKey:@"page"];
    
    if ([strSelectLang isEqualToString:@""]) {
        [dictGamePage setValue:@"english" forKey:@"lang"];
    }else {
        [dictGamePage setValue:strSelectLang forKey:@"lang"];
    }
    
    NSLog(@"GamePagePARAM ::: %@",dictGamePage);

    if ([strAction isEqualToString:@"GamePage"]) {
        [afPyramidePlayerGamePage requestWithUrl:GetPlayerGamePage_Url param:dictGamePage andToken:strAPIKey];
    }else {
        [afPyramidePlayerGamePage requestWithUrl:GetPlayerStatistics_Url param:dictGamePage andToken:strAPIKey];
    }
}

-(void)getPlayerRulesServiceCall {

    if (afPyramidePlayerRules) {
       [afPyramidePlayerRules cancelRequest];
       afPyramidePlayerRules.delegate = self;
       afPyramidePlayerRules = nil;
    }

    afPyramidePlayerRules = [[AFNetworkUtility alloc] init];
    afPyramidePlayerRules.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];
    
    NSMutableDictionary *dictRules = [[NSMutableDictionary alloc] init];
        
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];
    NSString *strCompetitionID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:COMPETITIONID]];
               
    [dictRules setValue:strCompetitionID forKey:@"clause[id]"];
    [dictRules setValue:@"DESC" forKey:@"sortby[id]"];

    if ([strSelectLang isEqualToString:@""]) {
        [dictRules setValue:@"english" forKey:@"lang"];
    }else {
        [dictRules setValue:strSelectLang forKey:@"lang"];
    }
    
    NSLog(@"RulesPARAM ::: %@",dictRules);

    [afPyramidePlayerRules requestWithUrl:GetPlayerRules_Url param:dictRules andToken:strAPIKey];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if ([[CommonClass trimString:txtSearchPlayer.text] length] != 0) {
        if (isRanking == YES) {
            [self getRankingAndNewsCompetitionPlayerServiceCall:@"Ranking" searchText:txtSearchPlayer.text];
        }else if (isNews == YES) {
            [self getRankingAndNewsCompetitionPlayerServiceCall:@"News" searchText:txtSearchPlayer.text];
        }else if (isGamePage == YES) {
            intCountPage = 1;
            
            [arrGamePageList removeAllObjects];
            arrGamePageList = [[NSMutableArray alloc] init];
            
            [self getPlayerGamePageServiceCall:intCountPage atAction:@"GamePage" searchText:txtSearchPlayer.text];
        }else if (isStaticstic == YES) {
            intCountPage = 0;
            
            [arrStaticsticList removeAllObjects];
            arrStaticsticList = [[NSMutableArray alloc] init];
            
            [self getPlayerGamePageServiceCall:intCountPage atAction:@"Staticstic" searchText:txtSearchPlayer.text];
        }
    }
    return YES;
}

#pragma mark -
#pragma mark - TableView Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (isRanking == YES || isNews == YES || isGamePage == YES || isStaticstic == YES || isRules == YES) {
        return 0.0f;
    }else {
        if (constHeightInChalleng.constant != 0) {
            return 165.0f + constHeightInChalleng.constant;
        }else {
            return 165.0f;
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (isRanking == YES || isNews == YES || isGamePage == YES || isStaticstic == YES || isRules == YES) {
        return nil;
    }else {
        return viewHeader;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (isRanking == YES) {
        return 58.0f;
    }else if (isNews == YES) {
        NewsCell *cell = (NewsCell *)[self tableView:tblDetailList cellForRowAtIndexPath:indexPath];
        return [cell getCellHeight];
    }else if (isGamePage == YES) {
        GamePageCell *cell = (GamePageCell *)[self tableView:tblDetailList cellForRowAtIndexPath:indexPath];
        return [cell getCellHeight];
    }else if (isStaticstic == YES) {
        return 50.0f;
    }else if (isRules == YES) {
        RulesListCell *cell = (RulesListCell *)[self tableView:tblDetailList cellForRowAtIndexPath:indexPath];
        return [cell getCellHeight];
    }else {
        DetailCellTableViewCell *cell = (DetailCellTableViewCell *)[self tableView:tblOtherMenu cellForRowAtIndexPath:indexPath];
        return [cell getCellHeight];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (isRanking == YES) {
        return  [arrRankingList count];
    }else if (isNews == YES) {
        return  [arrNewsList count];
    }else if (isGamePage == YES) {
        return  [arrGamePageList count];
    }else if (isStaticstic == YES) {
        return  [arrStaticsticList count];
    }else if (isRules == YES) {
        return  [arrRulesList count];
    }else {
        if (isDetailShow == YES) {
            return  10.0f;
        }else {
            return  6.0f;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (isRanking == YES) {
        static NSString *simpleTableIdentifier = @"RankingCell";
        
        RankingCell  *cell = (RankingCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RankingCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.selectionStyle  = UITableViewCellSelectionStyleNone;
        
        if ([arrRankingList count] > 0) {
            ChallangersList *objRanking = [arrRankingList objectAtIndex:indexPath.row];
            [cell getRankingPlayerDataList:objRanking];
        }
        
        return cell;
    }else if (isNews == YES) {
        static NSString *simpleTableIdentifier = @"NewsCell";

        NewsCell  *cell = (NewsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NewsCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }

        cell.selectionStyle  = UITableViewCellSelectionStyleNone;

        if ([arrNewsList count] > 0) {
            NewsList *objNewsList = [arrNewsList objectAtIndex:indexPath.row];
            [cell getPlayerNewsDataList:objNewsList];
        }
        return cell;
    }else if (isGamePage == YES) {
        static NSString *simpleTableIdentifier = @"GamePageCell";

        GamePageCell  *cell = (GamePageCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GamePageCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }

        cell.selectionStyle  = UITableViewCellSelectionStyleNone;

        cell.delegate = self;
        cell.index = indexPath.row;
        
        if ([arrGamePageList count] > 0) {
            GamePageList *objGamePage = [arrGamePageList objectAtIndex:indexPath.row];
            [cell setGamePageDataList:objGamePage];
        }

        return cell;
    }else if (isStaticstic == YES) {
        static NSString *simpleTableIdentifier = @"StaticsticCell";

        StaticsticCell  *cell = (StaticsticCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StaticsticCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }

        cell.selectionStyle  = UITableViewCellSelectionStyleNone;

        cell.delegate = self;
        cell.index = indexPath.row;
        
        if ([arrStaticsticList count] > 0) {
            StatisticsList *objStatistics = [arrStaticsticList objectAtIndex:indexPath.row];
            [cell getStatisticsDataList:objStatistics];
        }

        return cell;
    }else if (isRules == YES) {
        static NSString *simpleTableIdentifier = @"RulesListCell";

        RulesListCell  *cell = (RulesListCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RulesListCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }

        cell.selectionStyle  = UITableViewCellSelectionStyleNone;

        if ([arrRulesList count] > 0) {
            RulesList *objRulesList = [arrRulesList objectAtIndex:indexPath.row];
            [cell getPlayerRulesDataList:objRulesList];
        }
        
        return cell;
    }else {
        static NSString *simpleTableIdentifier = @"DetailCellTableViewCell";
        
        DetailCellTableViewCell  *cell = (DetailCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DetailCellTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.selectionStyle  = UITableViewCellSelectionStyleNone;
        
        cell.delegate = self;
        cell.index = indexPath.row;
                
        if ([arrDetailList count] > 0) {
            NSDictionary *dictDetail = [arrDetailList objectAtIndex:indexPath.row];
            
            if (indexPath.row == 2 && ([[dictDetail safeObjectForKey:@"Detail"] isEqualToString:@"Available for challenge"] || [[dictDetail safeObjectForKey:@"Detail"] isEqualToString:@"Not available (Vacation, injury)"]) && isEndDate == NO) {
                cell.btnEditStatus.hidden = NO;
            }else {
                cell.btnEditStatus.hidden = YES;
            }
            
            [cell getPlayerDetailInfo:dictDetail];
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (isRanking == YES) {
        
        ChallangersList *objRanking = [arrRankingList objectAtIndex:indexPath.row];
        
        PlayerDetailPopUpViewController *viewPlayerDetail = [[PlayerDetailPopUpViewController alloc] initWithNibName:@"PlayerDetailPopUpViewController" bundle:nil];
        viewPlayerDetail.delegate = self;
        viewPlayerDetail.objChallanger = objRanking;
        [self presentPopupViewController:viewPlayerDetail animationType:MJPopupViewAnimationFade];
    }else if (isNews == YES) {
        
        NewsList *objNews = [arrNewsList objectAtIndex:indexPath.row];
        
        if (![objNews.video_name isEqualToString:@""]) {
            AVPlayer *player = [AVPlayer playerWithURL:[[NSURL alloc] initWithString:objNews.video]];

            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setVideoIsNotPlay) name:AVPlayerItemDidPlayToEndTimeNotification object:player.currentItem];
                        
            // create a player view controller
            AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
            [self presentViewController:controller animated:YES completion:nil];
            controller.player = player;
            [player play];
        }else if ([objNews.video_name isEqualToString:@""]) {
            if (![objNews.photo isEqualToString:@""]) {
                GameMediaViewController *viewMediaPlay = [[GameMediaViewController alloc] initWithNibName:@"GameMediaViewController" bundle:nil];
                viewMediaPlay.strMediaLink = objNews.photo;
                [self.navigationController pushViewController:viewMediaPlay animated:YES];
            }else {
                [CommonClass showAlertWithTitle:provideAlert andMessage:provideVideoNotAvailable delegate:self];
            }
        }
    }else {
        
    }
}

-(void)setVideoIsNotPlay {
    
}

#pragma mark -
#pragma mark - SCROLL_END_DECELERATING_AND_LOAD_MORE_BUTTON_METHODS

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if(scrollView == tblDetailList){
        float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height + 20;
        NSLog(@"ValueScrolling>>> %f",endScrolling);
        
        if (endScrolling >= scrollView.contentSize.height) {
            if (isGamePage == YES) {
                if(isLoadMore) {
                    intCountPage ++;
                    if ([[CommonClass trimString:txtSearchPlayer.text] length] == 0) {
                        [self getPlayerGamePageServiceCall:intCountPage atAction:@"GamePage" searchText:@""];
                    }else {
                        [self getPlayerGamePageServiceCall:intCountPage atAction:@"GamePage" searchText:txtSearchPlayer.text];
                    }
                    isLoadMore = false;
                }
            }else if (isStaticstic == YES) {
                if(isLoadMore) {
                    intCountPage ++;
                    if ([[CommonClass trimString:txtSearchPlayer.text] length] == 0) {
                        [self getPlayerGamePageServiceCall:intCountPage atAction:@"Staticstic" searchText:@""];
                    }else {
                        [self getPlayerGamePageServiceCall:intCountPage atAction:@"Staticstic" searchText:txtSearchPlayer.text];
                    }
                    isLoadMore = false;
                }
            }
        }
    }
}

#pragma mark -
#pragma mark - Change Status Delegate Method

-(void)setChangeStatusAtIndex:(NSInteger)index {
    
    ChangeStatusPopUpViewController *viewChangeStatus = [[ChangeStatusPopUpViewController alloc] initWithNibName:@"ChangeStatusPopUpViewController" bundle:nil];
    viewChangeStatus.delegate = self;
    viewChangeStatus.strStatusTitle = strStatusPlayer;
    [self presentPopupViewController:viewChangeStatus animationType:MJPopupViewAnimationFade];
}

-(void)selectStatus:(NSString *)strStatus AndAction:(NSString *)strType {
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    if ([strType isEqualToString:@"ChangeStatus"]) {
        [self UpdatePlayerCompatitionStatusServiceCall:strStatus];
    }
}

-(void)setPlayerDetailInfoAction:(NSString *)strAction {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)setStaticsticInfoAtIndex:(NSInteger)index {
    
    StatisticsList *objStatistics = [arrStaticsticList objectAtIndex:index];
        
    StaticsticDetailViewController *viewStaticsticDetail = [[StaticsticDetailViewController alloc] initWithNibName:@"StaticsticDetailViewController" bundle:nil];
    viewStaticsticDetail.objStatistics = objStatistics;
    [self.navigationController pushViewController:viewStaticsticDetail animated:YES];
}

-(void)setGamePageAction:(NSString *)strAction AtIndex:(NSInteger)index {
    
    GamePageList *objGamePage = [arrGamePageList objectAtIndex:index];
    
    if ([strAction isEqualToString:@"Picture"]) {
        GameMediaViewController *viewMediaPlay = [[GameMediaViewController alloc] initWithNibName:@"GameMediaViewController" bundle:nil];
        viewMediaPlay.strMediaLink = objGamePage.pictures;
        [self.navigationController pushViewController:viewMediaPlay animated:YES];
    }else if ([strAction isEqualToString:@"Video"]) {
        AVPlayer *player = [AVPlayer playerWithURL:[[NSURL alloc] initWithString:objGamePage.video]];

        // create a player view controller
        AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
        [self presentViewController:controller animated:YES completion:nil];
        controller.player = player;
        [player play];
    }else if ([strAction isEqualToString:@"VidPic"]) {
        
        if (![objGamePage.video isEqualToString:@""]) {
            AVPlayer *player = [AVPlayer playerWithURL:[[NSURL alloc] initWithString:objGamePage.video]];

            // create a player view controller
            AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
            [self presentViewController:controller animated:YES completion:nil];
            controller.player = player;
            [player play];
        }else if ([objGamePage.pictures isEqualToString:@""]) {
            GameMediaViewController *viewMediaPlay = [[GameMediaViewController alloc] initWithNibName:@"GameMediaViewController" bundle:nil];
            viewMediaPlay.strMediaLink = objGamePage.pictures;
            [self.navigationController pushViewController:viewMediaPlay animated:YES];
        }
    }
}

- (IBAction)onClickChallengeAcceptBtn:(id)sender {
    [self setChallengeCanIPopUpView];
}

-(void)setChallengeCanIPopUpView {
    ChallengeCanIPopUpViewController *viewAcceptChallenge = [[ChallengeCanIPopUpViewController alloc] initWithNibName:@"ChallengeCanIPopUpViewController" bundle:nil];
    viewAcceptChallenge.delegate = self;
    [self presentPopupViewController:viewAcceptChallenge animationType:MJPopupViewAnimationFade];
}

-(void)setChallengeAction:(NSString *)strAction {
    
    if ([strAction isEqualToString:@"Date"]) {
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self setSelectTimeDate:@"Date"];
        });
    }else if ([strAction isEqualToString:@"Hour"]) {
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self setSelectTimeDate:@"Hour"];
        });
    }
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    strActionType = @"accept";
}

-(void)setSelectTimeDate:(NSString *)strTitleAction {

    BirthdayPopUpViewController *viewBDate = [[BirthdayPopUpViewController alloc] initWithNibName:@"BirthdayPopUpViewController" bundle:nil];
    viewBDate.delegate = self;
    viewBDate.strAction = strTitleAction;
    [self presentPopupViewController:viewBDate animationType:MJPopupViewAnimationFade];
}

-(void)setBirthdayDetail:(NSString *_Nullable)strBirthDate andSendField:(NSString *_Nonnull)strSendDate atAction:(NSString *)strPickerType {
    
    NSString *strSendBDate = [NSString stringWithFormat:@"%@",strSendDate];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if ([strPickerType isEqualToString:@"Hour"]) {
        [userDefault setObject:strSendBDate forKey:MATCHHOUR];
    }else {
        [userDefault setObject:strSendBDate forKey:MATCHDATE];
    }
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self setChallengeCanIPopUpView];
    });
}

- (IBAction)onClickChallengeRejectBtn:(id)sender {
    strActionType = @"decline";
    
    if (afPyramideAcceptRejectSubmit) {
        [afPyramideAcceptRejectSubmit cancelRequest];
        afPyramideAcceptRejectSubmit.delegate = self;
        afPyramideAcceptRejectSubmit = nil;
    }

    afPyramideAcceptRejectSubmit = [[AFNetworkUtility alloc] init];
    afPyramideAcceptRejectSubmit.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];

    NSMutableDictionary *dictAcpDecliSub = [[NSMutableDictionary alloc] init];
      
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];

    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];

    [dictAcpDecliSub setValue:strChaAcceptID forKey:@"existing_row_id"];
    [dictAcpDecliSub setValue:strActionType forKey:@"challenge_status"];

    if ([strSelectLang isEqualToString:@""]) {
      [dictAcpDecliSub setValue:@"english" forKey:@"lang"];
    }else {
      [dictAcpDecliSub setValue:strSelectLang forKey:@"lang"];
    }

    NSLog(@"ACPDECLIPARAM ::: %@",dictAcpDecliSub);

    [afPyramideAcceptRejectSubmit requestWithUrl:AcceptRejectChallenge_Url param:dictAcpDecliSub andToken:strAPIKey];
}

- (IBAction)onClickChallengeSubmitBtn:(id)sender {
    
    if ([btnSubmitResult.titleLabel.text isEqualToString:@"Accept"]) {
        [self setChallengeCanIPopUpView];
        strActionType = @"accept";
    }else {
        strActionType = @"SubmitResult";
        
        SubmitResultViewController *viewSubmitResult = [[SubmitResultViewController alloc] initWithNibName:@"SubmitResultViewController" bundle:nil];
        viewSubmitResult.strPOneName = strPlayerName;
        viewSubmitResult.strPTwoName = lblPlayerChallange.text;
        [self.navigationController pushViewController:viewSubmitResult animated:YES];
    }
}

-(void)setAcceptChallengeDate:(NSString *)strDate challengeHours:(NSString *)strHour andCourtName:(NSString *)strCourtName {
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    if (afPyramideAcceptRejectSubmit) {
        [afPyramideAcceptRejectSubmit cancelRequest];
        afPyramideAcceptRejectSubmit.delegate = self;
        afPyramideAcceptRejectSubmit = nil;
    }

    afPyramideAcceptRejectSubmit = [[AFNetworkUtility alloc] init];
    afPyramideAcceptRejectSubmit.delegate = self;

    [SHARED_APPDELEGATE showLoadingView];

    NSMutableDictionary *dictAcpDecliSub = [[NSMutableDictionary alloc] init];
      
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];

    NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
    NSString *strSelectLang = [NSString stringWithFormat:@"%@",[userDefault objectForKey:SELECTLANG]];

    [dictAcpDecliSub setValue:strChaAcceptID forKey:@"existing_row_id"];
    [dictAcpDecliSub setValue:strDate forKey:@"challenger_date"];
    [dictAcpDecliSub setValue:strHour forKey:@"hour_of_game"];
    [dictAcpDecliSub setValue:strCourtName forKey:@"court_name"];
    [dictAcpDecliSub setValue:strActionType forKey:@"challenge_status"];

    if ([strSelectLang isEqualToString:@""]) {
      [dictAcpDecliSub setValue:@"english" forKey:@"lang"];
    }else {
      [dictAcpDecliSub setValue:strSelectLang forKey:@"lang"];
    }

    NSLog(@"ACPDECLIPARAM ::: %@",dictAcpDecliSub);

    [afPyramideAcceptRejectSubmit requestWithUrl:AcceptRejectChallenge_Url param:dictAcpDecliSub andToken:strAPIKey];
}

-(void)setChallengeAcceptRejectServiceCall:(NSString *)strAction {
    
}

- (IBAction)onClickMoreDetailBtn:(id)sender {
    
    if (isRanking == YES) {
        LastChallangesViewController *viewLastChallangers = [[LastChallangesViewController alloc] initWithNibName:@"LastChallangesViewController" bundle:nil];
        viewLastChallangers.strAction = @"LastChallanges";
        [self.navigationController pushViewController:viewLastChallangers animated:YES];
    }else {
        if ([btnMoreDetail.titleLabel.text isEqualToString:@"More Details"]) {
            isDetailShow = YES;
            [btnMoreDetail setTitle:@"Hide Details" forState:UIControlStateNormal];
        }else {
            isDetailShow = NO;
            [btnMoreDetail setTitle:@"More Details" forState:UIControlStateNormal];
        }
        
        [tblOtherMenu reloadData];
    }
}

- (IBAction)onClickViewChallangersBtn:(id)sender {
    
    if (isRanking == YES) {
        LastChallangesViewController *viewLastChallangers = [[LastChallangesViewController alloc] initWithNibName:@"LastChallangesViewController" bundle:nil];
        viewLastChallangers.strAction = @"LastMatches";
        [self.navigationController pushViewController:viewLastChallangers animated:YES];
    }else {
        ChallangersDetailViewController *viewChallangersDetail = [[ChallangersDetailViewController alloc] initWithNibName:@"ChallangersDetailViewController" bundle:nil];
        viewChallangersDetail.strNavTitle = @"Test Title";
        [self.navigationController pushViewController:viewChallangersDetail animated:YES];
    }
}

- (IBAction)onClickAllGamesBtn:(id)sender {

    if ([btnAllGames.titleLabel.text isEqualToString:@"More Details"]) {
        isDetailShow = YES;
        [btnAllGames setTitle:@"Hide Details" forState:UIControlStateNormal];
        
        [tblOtherMenu reloadData];
    }else if ([btnAllGames.titleLabel.text isEqualToString:@"Hide Details"]) {
        isDetailShow = NO;
        [btnAllGames setTitle:@"More Details" forState:UIControlStateNormal];
        
        [tblOtherMenu reloadData];
    }else {
        AllGamesListViewController *viewAllGame = [[AllGamesListViewController alloc] initWithNibName:@"AllGamesListViewController" bundle:nil];
        [self.navigationController pushViewController:viewAllGame animated:YES];
    }
}

-(NSMutableDictionary *)getPlayerInfoTitle:(NSString *)title playerDetail:(NSString *)strDetail {
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:title forKey:@"Title"];
    [dict setObject:strDetail forKey:@"Detail"];
    
    return dict;
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataSuccess:(NSMutableDictionary *)dictResponce {
            
    if (utility == afPyramidePlayerCompetitions) {
        
        NSLog(@"PLAYERCOMRESPONCE ::: %@", dictResponce);
          
          [SHARED_APPDELEGATE hideLoadingView];
                          
          if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
              
              NSArray *arrCompetitions = [dictResponce safeObjectForKey:@"data"];
              
              if ([arrCompetitions count] > 0) {
                  
                  tblDetailList.hidden = YES;
                  tblOtherMenu.hidden = NO;
                  
                  [arrCompetitionList removeAllObjects];
                  arrCompetitionList = [[NSMutableArray alloc] init];
                  
                  [arrCompeDropList removeAllObjects];
                  arrCompeDropList = [[NSMutableArray alloc] init];
                  
                  for (NSDictionary *dictCompetitions in arrCompetitions) {
                      
                      CompetitionsList *objCompetitions = [[CompetitionsList alloc] init];
                      
                      objCompetitions.CompetitionsID = [NSString stringWithFormat:@"%@",[dictCompetitions safeObjectForKey:@"id"]];
                      objCompetitions.profile = [NSString stringWithFormat:@"%@",[dictCompetitions safeObjectForKey:@"profile"]];
                      objCompetitions.name = [NSString stringWithFormat:@"%@",[dictCompetitions safeObjectForKey:@"name"]];
                      objCompetitions.is_cmpt_over = [NSString stringWithFormat:@"%@",[dictCompetitions safeObjectForKey:@"is_cmpt_over"]];
                      objCompetitions.slug = [NSString stringWithFormat:@"%@",[dictCompetitions safeObjectForKey:@"slug"]];
                      objCompetitions.club_id = [NSString stringWithFormat:@"%@",[dictCompetitions safeObjectForKey:@"club_id"]];
                      objCompetitions.ComPetDescription = [NSString stringWithFormat:@"%@",[dictCompetitions safeObjectForKey:@"description"]];
                      objCompetitions.add_uid = [NSString stringWithFormat:@"%@",[dictCompetitions safeObjectForKey:@"add_uid"]];
                      objCompetitions.startDate = [NSString stringWithFormat:@"%@",[dictCompetitions safeObjectForKey:@"startDate"]];
                      objCompetitions.endDate = [NSString stringWithFormat:@"%@",[dictCompetitions safeObjectForKey:@"endDate"]];
                      objCompetitions.LastSignUpDate = [NSString stringWithFormat:@"%@",[dictCompetitions safeObjectForKey:@"LastSignUpDate"]];
                      
                      strDetailAbout = [NSString stringWithFormat:@"%@",[dictCompetitions safeObjectForKey:@"description"]];
                      
                      [arrCompeDropList addObject:objCompetitions.name];
                      [arrCompetitionList addObject:objCompetitions];
                  }
              }
        
              [self getIAmInChallengeServiceCall];
          }else {
              viewAdd.hidden = YES;
              viewRecoardNotFound.hidden = NO;
          }
    }else if (utility == afPyramideInChallenge) {
        
        NSLog(@"PLAYERDETAILRESPONCE ::: %@", dictResponce);
        
        [SHARED_APPDELEGATE hideLoadingView];
                        
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            
            NSDictionary *dictData = [[dictResponce safeObjectForKey:@"data"] valueForKey:@"challenge_detail"];
            
            strChaAcceptID = [NSString stringWithFormat:@"%@",[dictData safeObjectForKey:@"id"]];
            
            NSString *strChallengerId = [NSString stringWithFormat:@"%@",[dictData safeObjectForKey:@"challenger_id"]];
            NSString *strChallengerStatus = [NSString stringWithFormat:@"%@",[[dictResponce safeObjectForKey:@"data"] valueForKey:@"competition_play_game_status"]];
            NSString *strCanRefuse = [NSString stringWithFormat:@"%@",[[dictResponce safeObjectForKey:@"data"] valueForKey:@"can_refuse_challeng"]];
                        
            NSString *strPlayerID = [NSString stringWithFormat:@"%@",[userDefault objectForKey:USERID]];
            
            //can_refuse_challeng == 0 "display accept button" (khali accept)
            //can_refuse_challeng == 1 display "Reject button"(bane button)
            
            if (![strChallengerId isEqualToString:strPlayerID] && [strChallengerStatus isEqualToString:@"pending"]) {
                
                if ([strCanRefuse isEqualToString:@"1"]) {
                    btnAccept.hidden = NO;
                    btnReject.hidden = NO;
                    btnSubmitResult.hidden = YES;
                    
                    lblTitle.text = @"Challange Action";
                }else {
                    btnAccept.hidden = YES;
                    btnReject.hidden = YES;
                    btnSubmitResult.hidden = NO;
                    
                    lblTitle.text = @"Challange Action";
                    [btnSubmitResult setTitle:@"Accept" forState:UIControlStateNormal];
                }
                
                constHeightInChalleng.constant = 376.0f;
                constHeightSubmitBtn.constant = 66.0f;
            }else if ([strChallengerId isEqualToString:strPlayerID] && [strChallengerStatus isEqualToString:@"accept"]) {
                //1 button
                btnAccept.hidden = YES;
                btnReject.hidden = YES;
                btnSubmitResult.hidden = NO;
                
                lblTitle.text = @"Submit Result";
                
                constHeightInChalleng.constant = 376.0f;
                constHeightSubmitBtn.constant = 66.0f;
            }else {
                constHeightInChalleng.constant = 300.0f;
                constHeightSubmitBtn.constant = 0.0f;
            }
            
            isChallangeRecoard = YES;
            
            NSString *strPlayerNa = [NSString stringWithFormat:@"%@",[[dictResponce safeObjectForKey:@"data"] valueForKey:@"challenged_to_name"]];
            NSString *strPlayerGDate = [NSString stringWithFormat:@"%@",[[dictResponce safeObjectForKey:@"data"] valueForKey:@"game_confirm_by_date"]];
            NSString *strPlayerPUDate = [NSString stringWithFormat:@"%@",[[dictResponce safeObjectForKey:@"data"] valueForKey:@"game_played_untill_date"]];
            NSString *strPlayerPDate = [NSString stringWithFormat:@"%@",[[dictResponce safeObjectForKey:@"data"] valueForKey:@"competition_play_date"]];
            NSString *strPlayerHour = [NSString stringWithFormat:@"%@",[[dictResponce safeObjectForKey:@"data"] valueForKey:@"competition_play_hour"]];
            NSString *strPlayerCourtN = [NSString stringWithFormat:@"%@",[[dictResponce safeObjectForKey:@"data"] valueForKey:@"competition_play_court_name"]];
            
            if ([strPlayerNa isEqualToString:@""]) {
                lblPlayerChallange.text = @"You in Challenge";
            }else {
                lblPlayerChallange.text = [NSString stringWithFormat:@"%@",strPlayerNa];
            }
            
            if ([strPlayerGDate isEqualToString:@""]) {
                lblPlayerCDate.text = @"Confirm Date";
            }else {
                lblPlayerCDate.text = [NSString stringWithFormat:@"%@",strPlayerGDate];
            }
            
            if ([strPlayerPUDate isEqualToString:@""]) {
                lblPlayerMatchPlayed.text = @"Match Played";
            }else {
                lblPlayerMatchPlayed.text = [NSString stringWithFormat:@"%@",strPlayerPUDate];
            }
            
            if ([strPlayerPDate isEqualToString:@""]) {
                lblPlayerDate.text = @"Date";
            }else {
                lblPlayerDate.text = [NSString stringWithFormat:@"%@",strPlayerPDate];
            }
            
            if ([strPlayerHour isEqualToString:@""]) {
                lblPlayerHour.text = @"Hour";
            }else {
                lblPlayerHour.text = [NSString stringWithFormat:@"%@",strPlayerHour];
            }
            
            if ([strPlayerCourtN isEqualToString:@""]) {
                lblPlayerCountryName.text = @"Court Name";
            }else {
                lblPlayerCountryName.text = [NSString stringWithFormat:@"%@",strPlayerCourtN];
            }
            
            if ([strChallengerStatus isEqualToString:@""]) {
                lblPlayerChallangeStatus.text = @"Challange Status";
            }else {
                lblPlayerChallangeStatus.text = [NSString stringWithFormat:@"%@",strChallengerStatus];
            }
                        
            [self getPlayerCompetitionDetailServiceCall];
        }else {
            NSArray *arrData = [dictResponce safeObjectForKey:@"data"];
            
            if ([arrData count] == 0) {
                constHeightInChalleng.constant = 0.0f;
                constHeightSubmitBtn.constant = 0.0f;
                isChallangeRecoard = NO;
            }else {
                constHeightInChalleng.constant = 300.0f;
                constHeightSubmitBtn.constant = 0.0f;
                isChallangeRecoard = YES;
            }
            
            [self getPlayerCompetitionDetailServiceCall];
        }
    }else if (utility == afPyramideCompetitionPlayerDetail) {
        
        NSLog(@"PLAYERDETAILRESPONCE ::: %@", dictResponce);
        
        [SHARED_APPDELEGATE hideLoadingView];
                        
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            
            tblDetailList.hidden = YES;
            tblOtherMenu.hidden = NO;
                        
            if (isChallangeRecoard == YES) {
                btnAllGames.hidden = NO;
                btnMoreDetail.hidden = YES;
                btnViewChallangers.hidden = YES;
                
                [btnAllGames setTitle:@"More Details" forState:UIControlStateNormal];
            }
                
            NSDictionary *dictPlayerDetail = [dictResponce safeObjectForKey:@"data"];
            
            NSString *imgProfilePicUrl = [NSString stringWithFormat:@"%@",[dictPlayerDetail safeObjectForKey:@"profile"]];

            if (imgProfilePicUrl.length > 0) {
               [asyImgPlayer loadingImage:imgProfilePicUrl placeHolderImage:USER_PLACEHOLDER_IMAGE];
            }else {
               [asyImgPlayer loadingImage:nil placeHolderImage:USER_PLACEHOLDER_IMAGE];
            }
            
            strPlayerName = [NSString stringWithFormat:@"%@ %@",[dictPlayerDetail safeObjectForKey:@"firstname"],[dictPlayerDetail safeObjectForKey:@"lastname"]];
            [arrDetailList addObject:[self getPlayerInfoTitle:@"Player Name" playerDetail:strPlayerName]];
            
            NSString *strPlayeNote = [NSString stringWithFormat:@"%@",[dictPlayerDetail safeObjectForKey:@"player_note1"]];
            
            if ([strPlayeNote isEqualToString:@""]) {
                [arrDetailList addObject:[self getPlayerInfoTitle:@"Player Note" playerDetail:@"Player Note"]];
            }else {
                [arrDetailList addObject:[self getPlayerInfoTitle:@"Player Note" playerDetail:[dictPlayerDetail safeObjectForKey:@"player_note1"]]];
            }
            
            strStatusPlayer = [NSString stringWithFormat:@"%@",[dictPlayerDetail safeObjectForKey:@"competition_player_status_text"]];
            
            [arrDetailList addObject:[self getPlayerInfoTitle:@"My Status" playerDetail:[dictPlayerDetail safeObjectForKey:@"competition_player_status_text"]]];
            [arrDetailList addObject:[self getPlayerInfoTitle:@"Born Year" playerDetail:[dictPlayerDetail safeObjectForKey:@"born_year"]]];
            
            NSString *strRanking = [NSString stringWithFormat:@"%@",[dictPlayerDetail safeObjectForKey:@"competition_player_position"]];
            
            if (![strRanking isEqualToString:@""]) {
                [arrDetailList addObject:[self getPlayerInfoTitle:@"Ranking" playerDetail:strRanking]];
            }else {
                [arrDetailList addObject:[self getPlayerInfoTitle:@"Ranking" playerDetail:@"-"]];
            }
            
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            
            [userDefault setObject:strRanking forKey:USERRANKING];
                        
            [arrDetailList addObject:[self getPlayerInfoTitle:@"Efficiency" playerDetail:[dictPlayerDetail safeObjectForKey:@"player_efficiency"]]];
            [arrDetailList addObject:[self getPlayerInfoTitle:@"Start Date" playerDetail:[dictPlayerDetail safeObjectForKey:@"startDate"]]];
            [arrDetailList addObject:[self getPlayerInfoTitle:@"End Date" playerDetail:[dictPlayerDetail safeObjectForKey:@"endDate"]]];
            [arrDetailList addObject:[self getPlayerInfoTitle:@"Last Signup Date" playerDetail:[dictPlayerDetail safeObjectForKey:@"lastSignupDate"]]];
            
            NSString *strAbout = [NSString stringWithFormat:@"%@",strDetailAbout];
            
            if ([strAbout isEqualToString:@""]) {
                [arrDetailList addObject:[self getPlayerInfoTitle:@"About" playerDetail:@"About"]];
            }else {
                [arrDetailList addObject:[self getPlayerInfoTitle:@"About" playerDetail:strAbout]];
            }
            
            NSString *strEndDate = [NSString stringWithFormat:@"%@",[dictPlayerDetail safeObjectForKey:@"endDate"]];
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"dd/MMM/yyyy"];
            NSDate *date = [format dateFromString:strEndDate];
            [format setDateFormat:@"dd/MM/yyyy"];
            
            NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"EEE, MMM dd, yyyy"];
            NSDate *date1 = [NSDate date];
            NSComparisonResult result = [date compare:date1];

            if(result == NSOrderedDescending) {
                isEndDate = NO;
            }else {
                isEndDate = YES;
            }

            [tblOtherMenu reloadData];
        }else {
            viewAdd.hidden = YES;
            viewRecoardNotFound.hidden = NO;
            [CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
        }
    }else if (utility == afPyramidePlayerRanking) {
        
        NSLog(@"RANKINGRESPONCE ::: %@", dictResponce);
        
        [SHARED_APPDELEGATE hideLoadingView];
                        
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            
            NSArray *arrRankingData = [dictResponce safeObjectForKey:@"data"];
            
            if ([arrRankingData count] > 0) {
                
                if (isNews == YES) {
                    
                    tblDetailList.hidden = NO;
                    tblOtherMenu.hidden = YES;
                    
                    for (NSDictionary *dictNews in arrRankingData) {
                    
                        NewsList *objNewsList = [[NewsList alloc] init];
                        
                        objNewsList.add_date = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"add_date"]];
                        objNewsList.add_uid = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"add_uid"]];
                        objNewsList.competition_id = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"competition_id"]];
                        objNewsList.date = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"date"]];
                        objNewsList.del_date = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"del_date"]];
                        objNewsList.del_uid = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"del_uid"]];
                        objNewsList.NewsDescription = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"description"]];
                        objNewsList.displayDate = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"displayDate"]];
                        objNewsList.edit_date = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"edit_date"]];
                        objNewsList.edit_uid = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"edit_uid"]];
                        objNewsList.Newsid = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"id"]];
                        objNewsList.lang = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"lang"]];
                        objNewsList.photo = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"photo"]];
                        objNewsList.status = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"status"]];
                        objNewsList.title = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"title"]];
                        objNewsList.video = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"video"]];
                        objNewsList.video_name = [NSString stringWithFormat:@"%@",[dictNews safeObjectForKey:@"video_name"]];
                        
                        if (![objNewsList.video_name isEqualToString:@""]) {
                            AVPlayer *player = [AVPlayer playerWithURL:[NSURL URLWithString:objNewsList.video]];
                            
                            AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:objNewsList.video]];
                            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
                            
                            float tempTime = CMTimeGetSeconds(player.currentItem.duration);
                            CMTime time = CMTimeMake(tempTime, 1);  // (1,1)
                            CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
                            objNewsList.imgThumbnail = [UIImage imageWithCGImage:imageRef];
                        }
                        
                        [arrNewsList addObject:objNewsList];
                    }
                }else {
                    tblDetailList.hidden = NO;
                    tblOtherMenu.hidden = YES;
                    
                    for (NSDictionary *dictRanking in arrRankingData) {
                        
                        ChallangersList *objRanking = [[ChallangersList alloc] init];
                        
                        objRanking.player_name = [NSString stringWithFormat:@"%@ %@",[dictRanking safeObjectForKey:@"firstname"],[dictRanking safeObjectForKey:@"lastname"]];
                        objRanking.player_position = [NSString stringWithFormat:@"%@",[dictRanking safeObjectForKey:@"player_position_at_competition"]];
                        objRanking.player_born_year = [NSString stringWithFormat:@"%@",[dictRanking safeObjectForKey:@"born_year"]];
                        objRanking.player_status = [NSString stringWithFormat:@"%@",[dictRanking safeObjectForKey:@"player_status"]];
                        objRanking.player_status_color = [NSString stringWithFormat:@"%@",[dictRanking safeObjectForKey:@"player_status_color"]];
                        objRanking.player_profile = [NSString stringWithFormat:@"%@",[dictRanking safeObjectForKey:@"profile"]];
                        objRanking.player_efficiency = @"-";
                        
                        [arrRankingList addObject:objRanking];
                    }
                }
            
                [tblDetailList reloadData];
            }
        }else {
            viewAdd.hidden = YES;
            viewRecoardNotFound.hidden = NO;
        }
    }else if (utility == afPyramidePlayerGamePage) {
        
        NSLog(@"GAMEPAGERESPONCE ::: %@", dictResponce);
        
        [SHARED_APPDELEGATE hideLoadingView];
                        
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            
            NSArray *arrGamePageData = [dictResponce safeObjectForKey:@"data"];
            
            if ([arrGamePageData count] > 0) {
                
                if (isGamePage == YES) {
                    
                    isLoadMore = true;
                    
                    tblDetailList.hidden = NO;
                    tblOtherMenu.hidden = YES;
                    
                    for (NSDictionary *dictGamePage in arrGamePageData) {
                        
                        GamePageList *objGamePage = [[GamePageList alloc] init];
                        
                        objGamePage.score_1_1 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_1"]];
                        objGamePage.score_1_2 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_2"]];
                        objGamePage.score_1_3 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_3"]];
                        objGamePage.score_1_4 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_4"]];
                        objGamePage.score_1_5 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_5"]];
                        objGamePage.score_1_6 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_6"]];
                        objGamePage.score_1_7 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_7"]];
                        objGamePage.score_1_8 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_8"]];
                        objGamePage.score_1_9 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_9"]];
                        objGamePage.score_1_10 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_10"]];
                        objGamePage.score_1_11 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_1_11"]];
                        objGamePage.score_2_1 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_1"]];
                        objGamePage.score_2_2 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_2"]];
                        objGamePage.score_2_3 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_3"]];
                        objGamePage.score_2_4 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_4"]];
                        objGamePage.score_2_5 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_5"]];
                        objGamePage.score_2_6 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_6"]];
                        objGamePage.score_2_7 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_7"]];
                        objGamePage.score_2_8 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_8"]];
                        objGamePage.score_2_9 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_9"]];
                        objGamePage.score_2_10 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_10"]];
                        objGamePage.score_2_11 = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"score_2_11"]];
                        objGamePage.challenger_score = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"challenger_score"]];
                        objGamePage.challenged_to_score = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"challenged_to_score"]];
                        objGamePage.challengerDate = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"challengerDate"]];
                        objGamePage.competition_play_hour = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"competition_play_hour"]];
                        objGamePage.court_name = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"court_name"]];
                        objGamePage.video = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"video"]];
                        objGamePage.pictures = [NSString stringWithFormat:@"%@",[dictGamePage safeObjectForKey:@"pictures"]];
                        
                        
                        NSDictionary *dictChallengerDetail = [dictGamePage safeObjectForKey:@"challenger_detail"];
                        NSDictionary *dictChallengerToDetail = [dictGamePage safeObjectForKey:@"challenge_to_detail"];
                        
                        objGamePage.challenged_Profile = [NSString stringWithFormat:@"%@",[dictChallengerDetail safeObjectForKey:@"profile"]];
                        objGamePage.challenged_PlayerName = [NSString stringWithFormat:@"%@ %@",[dictChallengerDetail safeObjectForKey:@"firstname"],[dictChallengerDetail safeObjectForKey:@"lastname"]];
                        
                        objGamePage.challenged_to_Profile = [NSString stringWithFormat:@"%@",[dictChallengerToDetail safeObjectForKey:@"profile"]];
                        objGamePage.challenged_to_PlayerName = [NSString stringWithFormat:@"%@ %@",[dictChallengerToDetail safeObjectForKey:@"firstname"],[dictChallengerToDetail safeObjectForKey:@"lastname"]];
                        
                        
                        [arrGamePageList addObject:objGamePage];
                    }
                }else {
                    
                    isLoadMore = true;
                    
                    tblDetailList.hidden = NO;
                    tblOtherMenu.hidden = YES;
                    
                    for (NSDictionary *dictStaticstic in arrGamePageData) {
                     
                        StatisticsList *objStatistics = [[StatisticsList alloc] init];
                        
                        objStatistics.month_index = [NSString stringWithFormat:@"%@",[dictStaticstic safeObjectForKey:@"month_index"]];
                        objStatistics.player_efficiency = [NSString stringWithFormat:@"%@",[dictStaticstic safeObjectForKey:@"player_efficiency"]];
                        objStatistics.player_id = [NSString stringWithFormat:@"%@",[dictStaticstic safeObjectForKey:@"player_id"]];
                        objStatistics.player_name = [NSString stringWithFormat:@"%@",[dictStaticstic safeObjectForKey:@"player_name"]];
                        objStatistics.player_o_s = [NSString stringWithFormat:@"%@",[dictStaticstic safeObjectForKey:@"player_o_s"]];
                        objStatistics.player_total_games = [NSString stringWithFormat:@"%@",[dictStaticstic safeObjectForKey:@"player_total_games"]];
                        objStatistics.player_total_lose_games = [NSString stringWithFormat:@"%@",[dictStaticstic safeObjectForKey:@"player_total_lose_games"]];
                        objStatistics.player_total_win_games = [NSString stringWithFormat:@"%@",[dictStaticstic safeObjectForKey:@"player_total_win_games"]];
                        objStatistics.total_months = [NSString stringWithFormat:@"%@",[dictStaticstic safeObjectForKey:@"total_months"]];
                                                
                        objStatistics.arrMonthsList = [[NSMutableArray alloc] init];
                        [objStatistics.arrMonthsList addObjectsFromArray:[dictStaticstic safeObjectForKey:@"month"]];
                                                
                        [arrStaticsticList addObject:objStatistics];
                    }
                }
            }else {
                if (isGamePage == YES) {
                    if ([arrGamePageList count] > 0) {
                       isLoadMore = false;
                    }else {
                       viewAdd.hidden = YES;
                       viewRecoardNotFound.hidden = NO;
                    }
                }else {
                    if ([arrStaticsticList count] > 0) {
                        isLoadMore = false;
                    }else {
                        viewAdd.hidden = YES;
                        viewRecoardNotFound.hidden = NO;
                    }
                }
            }
            
            [tblDetailList reloadData];
        }else {
            if (isGamePage == YES) {
                if ([arrGamePageList count] > 0) {
                    isLoadMore = false;
                }else {
                    viewAdd.hidden = YES;
                    viewRecoardNotFound.hidden = NO;
                }
            }else {
                if ([arrStaticsticList count] > 0) {
                    isLoadMore = false;
                }else {
                    viewAdd.hidden = YES;
                    viewRecoardNotFound.hidden = NO;
                }
            }
        }
    }else if (utility == afPyramidePlayerRules) {
        
        NSLog(@"RULESRESPONCE ::: %@", dictResponce);

        [SHARED_APPDELEGATE hideLoadingView];
                       
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
           
            NSArray *arrRuleData = [dictResponce safeObjectForKey:@"data"];
            
            int intRuleCount = 0;
            
            if ([arrRuleData count] > 0) {
                
                tblDetailList.hidden = NO;
                tblOtherMenu.hidden = YES;
                
                NSDictionary *dictDataRule = [arrRuleData firstObject];
                
                NSMutableArray *arrRules = [[NSMutableArray alloc] init];
                [arrRules addObjectsFromArray:[dictDataRule safeObjectForKey:@"rules_arr"]];
                            
                if ([arrRules count] > 0) {
                    
                    for (NSDictionary *dictRules in arrRules) {
                                                 
                        intRuleCount++;
                        
                        RulesList *objRulesList = [[RulesList alloc] init];

                        objRulesList.ConfigID = [NSString stringWithFormat:@"%@",[dictRules safeObjectForKey:@"id"]];
                        objRulesList.lang = [NSString stringWithFormat:@"%@",[dictRules safeObjectForKey:@"lang"]];
                        objRulesList.rule_id = [NSString stringWithFormat:@"%@",[dictRules safeObjectForKey:@"rule_id"]];
                        objRulesList.ruleCount = [NSString stringWithFormat:@"%d",intRuleCount];
                        
                        NSString *strDescription = [NSString stringWithFormat:@"%@",[dictRules safeObjectForKey:@"description"]];
                          
                        if (([strDescription rangeOfString:@"Config1"].location != NSNotFound) && ([strDescription rangeOfString:@"Config2"].location != NSNotFound) && ([strDescription rangeOfString:@"Config3"].location != NSNotFound) && ([strDescription rangeOfString:@"Config4"].location != NSNotFound)) {
                            
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config1" withString:[dictCofig safeObjectForKey:@"config1"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config2" withString:[dictCofig safeObjectForKey:@"config2"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config3" withString:[dictCofig safeObjectForKey:@"config3"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config4" withString:[dictCofig safeObjectForKey:@"config4"]];
                            
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else if (([strDescription rangeOfString:@"Config1"].location != NSNotFound) && ([strDescription rangeOfString:@"Config2"].location != NSNotFound) && ([strDescription rangeOfString:@"Config3"].location != NSNotFound)) {
                            
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config1" withString:[dictCofig safeObjectForKey:@"config1"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config2" withString:[dictCofig safeObjectForKey:@"config2"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config3" withString:[dictCofig safeObjectForKey:@"config3"]];
                                                        
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else if (([strDescription rangeOfString:@"Config2"].location != NSNotFound) && ([strDescription rangeOfString:@"Config3"].location != NSNotFound) && ([strDescription rangeOfString:@"Config4"].location != NSNotFound)) {
                            
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config2" withString:[dictCofig safeObjectForKey:@"config2"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config3" withString:[dictCofig safeObjectForKey:@"config3"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config4" withString:[dictCofig safeObjectForKey:@"config4"]];
                                                        
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else if (([strDescription rangeOfString:@"Config1"].location != NSNotFound) && ([strDescription rangeOfString:@"Config3"].location != NSNotFound) && ([strDescription rangeOfString:@"Config4"].location != NSNotFound)) {
                            
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config1" withString:[dictCofig safeObjectForKey:@"config1"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config3" withString:[dictCofig safeObjectForKey:@"config3"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config4" withString:[dictCofig safeObjectForKey:@"config4"]];
                                                        
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else if (([strDescription rangeOfString:@"Config1"].location != NSNotFound) && ([strDescription rangeOfString:@"Config2"].location != NSNotFound) && ([strDescription rangeOfString:@"Config4"].location != NSNotFound)) {
                              
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config1" withString:[dictCofig safeObjectForKey:@"config1"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config2" withString:[dictCofig safeObjectForKey:@"config2"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config4" withString:[dictCofig safeObjectForKey:@"config4"]];
                                                        
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else if (([strDescription rangeOfString:@"Config1"].location != NSNotFound) && ([strDescription rangeOfString:@"Config2"].location != NSNotFound)) {
                            
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config1" withString:[dictCofig safeObjectForKey:@"config1"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config2" withString:[dictCofig safeObjectForKey:@"config2"]];
                            
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else if (([strDescription rangeOfString:@"Config2"].location != NSNotFound) && ([strDescription rangeOfString:@"Config3"].location != NSNotFound)) {
                                                   
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config2" withString:[dictCofig safeObjectForKey:@"config2"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config3" withString:[dictCofig safeObjectForKey:@"config3"]];
                                                                                    
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else if (([strDescription rangeOfString:@"Config3"].location != NSNotFound) && ([strDescription rangeOfString:@"Config4"].location != NSNotFound)) {
                                         
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config3" withString:[dictCofig safeObjectForKey:@"config3"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config4" withString:[dictCofig safeObjectForKey:@"config4"]];
                                                                                    
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else if (([strDescription rangeOfString:@"Config1"].location != NSNotFound) && ([strDescription rangeOfString:@"Config3"].location != NSNotFound)) {
                                 
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config1" withString:[dictCofig safeObjectForKey:@"config1"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config3" withString:[dictCofig safeObjectForKey:@"config3"]];
                                                                                    
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else if (([strDescription rangeOfString:@"Config1"].location != NSNotFound) && ([strDescription rangeOfString:@"Config4"].location != NSNotFound)) {
                              
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config1" withString:[dictCofig safeObjectForKey:@"config1"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config4" withString:[dictCofig safeObjectForKey:@"config4"]];
                                                                                    
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else if (([strDescription rangeOfString:@"Config2"].location != NSNotFound) && ([strDescription rangeOfString:@"Config4"].location != NSNotFound)) {
                                
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config2" withString:[dictCofig safeObjectForKey:@"config2"]];
                            
                            strConfig1 = [strConfig1 stringByReplacingOccurrencesOfString:@"Config4" withString:[dictCofig safeObjectForKey:@"config4"]];
                                                                                    
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else if (([strDescription rangeOfString:@"Config1"].location != NSNotFound)) {
                              
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config1" withString:[dictCofig safeObjectForKey:@"config1"]];
                            
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else if (([strDescription rangeOfString:@"Config2"].location != NSNotFound)) {
                             
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config2" withString:[dictCofig safeObjectForKey:@"config2"]];
                            
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else if (([strDescription rangeOfString:@"Config3"].location != NSNotFound)) {
                            
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config3" withString:[dictCofig safeObjectForKey:@"config3"]];
                            
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else if (([strDescription rangeOfString:@"Config4"].location != NSNotFound)) {
                             
                            NSDictionary *dictRuleWithConfig = [dictDataRule safeObjectForKey:@"rule_with_config"];

                            NSDictionary *dictCofig = [dictRuleWithConfig safeObjectForKey:objRulesList.ConfigID];
                            
                            NSString *strConfig1 = [strDescription stringByReplacingOccurrencesOfString:@"Config4" withString:[dictCofig safeObjectForKey:@"config4"]];
                            
                            objRulesList.ruleDescription = strConfig1;
                            
                            NSLog(@"objRulesList.ruleDescription>>>> %@",objRulesList.ruleDescription);
                        }else {
                            objRulesList.ruleDescription = [NSString stringWithFormat:@"%@",strDescription];
                        }
                        
                        [arrRulesList addObject:objRulesList];
                    }
                }
            }
            [tblDetailList reloadData];
        }else {
            viewAdd.hidden = YES;
            viewRecoardNotFound.hidden = NO;
        }
    }else if (utility == afPyramideAcceptRejectSubmit) {
        
        NSLog(@"ACCPDECLINESUBRESPONCE ::: %@", dictResponce);

        [SHARED_APPDELEGATE hideLoadingView];
                       
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {

            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            
            [userDefault removeObjectForKey:MATCHDATE];
            [userDefault removeObjectForKey:MATCHHOUR];
        
            UIAlertController *alert = [UIAlertController
                                       alertControllerWithTitle:provideAlert
                                       message:[dictResponce safeObjectForKey:@"message"]
                                       preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *Ok = [UIAlertAction
                              actionWithTitle:provideOk
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action) {
                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                  self->arrDetailList = [[NSMutableArray alloc] init];
                                  [self getPlayerCompetitionsServiceCall];
                              }];

            [alert addAction:Ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }else if (utility == afPyramideUpdatePlayerStatus) {
        
        NSLog(@"CHANGESTATUSRESPONCE ::: %@", dictResponce);

        [SHARED_APPDELEGATE hideLoadingView];
                       
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            [self getPlayerCompetitionDetailServiceCall];
        }
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Fail_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataFail:(NSError *)error {
    [SHARED_APPDELEGATE hideLoadingView];
    NSString *strError = [NSString stringWithFormat:@"%ld",(long)error.code];
    [CommonClass showAlertWithTitle:provideAlert andMessage:strError delegate:self];
}


@end
