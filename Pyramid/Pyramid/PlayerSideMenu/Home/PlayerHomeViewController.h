//
//  PlayerHomeViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailCellTableViewCell.h"
#import "RankingCell.h"
#import "NewsCell.h"
#import "GamePageCell.h"
#import "StaticsticCell.h"
#import "MenuListCell.h"
#import "RulesListCell.h"
#import "NIDropDown.h"
#import "AsyImage.h"
#import "ChangeStatusPopUpViewController.h"
#import "PlayerDetailPopUpViewController.h"
#import "StaticsticDetailViewController.h"
#import "AllGamesListViewController.h"
#import "ChallangersDetailViewController.h"
#import "LastChallangesViewController.h"
#import "GameMediaViewController.h"
#import "AFNetworkUtility.h"
#import "CompetitionsList.h"
#import "NewsList.h"
#import "GamePageList.h"
#import "StatisticsList.h"
#import "RulesList.h"
#import "ChallengeCanIPopUpViewController.h"
#import "BirthdayPopUpViewController.h"
#import "SubmitResultViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PlayerHomeViewController : MasterViewController <UITableViewDelegate,UITableViewDataSource,NIDropDownDelegate,ChangeStatusPopUpViewControllerDelegate,DetailCellTableViewCellDelegate,PlayerDetailPopUpViewControllerDelegate,StaticsticCellDelegate,AFNetworkUtilityDelegate,GamePageCellDelegate,BirthdayPopUpViewControllerDelegate,ChallengeCanIPopUpViewControllerDelegate> {
    
    IBOutlet UICollectionView *collectionMenu;
    
    IBOutlet UIButton *btnSelectCompetition;

    IBOutlet UIView *viewAdd;
    IBOutlet UIView *viewDisplayBtn;
    IBOutlet UIView *viewRecoardNotFound;
        
    IBOutlet UIView *viewHeader;
    IBOutlet UIView *viewFooter;
    
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblPlayerChallange;
    IBOutlet UILabel *lblPlayerCDate;
    IBOutlet UILabel *lblPlayerMatchPlayed;
    IBOutlet UILabel *lblPlayerDate;
    IBOutlet UILabel *lblPlayerHour;
    IBOutlet UILabel *lblPlayerCountryName;
    IBOutlet UILabel *lblPlayerChallangeStatus;
    
    IBOutlet UIButton *btnMoreDetail;
    IBOutlet UIButton *btnViewChallangers;
    IBOutlet UIButton *btnAllGames;
    IBOutlet UIButton *btnAccept;
    IBOutlet UIButton *btnReject;
    IBOutlet UIButton *btnSubmitResult;
    
    IBOutlet UITableView *tblDetailList;
    IBOutlet UITableView *tblOtherMenu;
    
    IBOutlet UITextField *txtSearchPlayer;
    
    IBOutlet NSLayoutConstraint *constHeightSearchView;
    IBOutlet NSLayoutConstraint *constHeightBtnView;
    IBOutlet NSLayoutConstraint *constHeightInChalleng;
    IBOutlet NSLayoutConstraint *constHeightSubmitBtn;
    
    IBOutlet AsyImage *asyImgPlayer;
    
    NSMutableArray *arrDetailList;
    NSMutableArray *arrRankingList;
    NSMutableArray *arrNewsList;
    NSMutableArray *arrGamePageList;
    NSMutableArray *arrStaticsticList;
    NSMutableArray *arrRulesList;
    NSMutableArray *arrCompetitionList;
    NSMutableArray *arrCompeDropList;
    NSMutableArray *arrMenuList;
    
    NIDropDown *dropDown;
    
    NSInteger intIndex;
    
    BOOL isLoadMore;
    BOOL isRanking;
    BOOL isNews;
    BOOL isGamePage;
    BOOL isStaticstic;
    BOOL isRules;
    BOOL isDetailShow;
    BOOL isChallangeRecoard;
    BOOL isEndDate;
    
    int intCountPage;
    
    NSString *strActionType;
    NSString *strChaAcceptID;
    NSString *strDetailAbout;
    NSString *strStatusPlayer;
    NSString *strPlayerName;
    
    AFNetworkUtility *afPyramidePlayerCompetitions;
    AFNetworkUtility *afPyramideCompetitionPlayerDetail;
    AFNetworkUtility *afPyramidePlayerRanking;
    AFNetworkUtility *afPyramidePlayerGamePage;
    AFNetworkUtility *afPyramidePlayerRules;
    AFNetworkUtility *afPyramideInChallenge;
    AFNetworkUtility *afPyramideAcceptRejectSubmit;
    AFNetworkUtility *afPyramideUpdatePlayerStatus;
}

@end

NS_ASSUME_NONNULL_END
