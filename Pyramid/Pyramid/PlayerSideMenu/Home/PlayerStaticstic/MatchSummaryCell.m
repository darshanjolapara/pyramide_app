//
//  MatchSummaryCell.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "MatchSummaryCell.h"
#import "AppDelegate.h"

@implementation MatchSummaryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    viewBack.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)setMatchSummaryDataList:(NSDictionary *)dictMatch {
    
    lblYear.text = [NSString stringWithFormat:@"%@",[dictMatch safeObjectForKey:@"month"]];
    lblPlayedCount.text = [NSString stringWithFormat:@"%@",[dictMatch safeObjectForKey:@"palyed"]];
    
    NSString *strHexColor = [[dictMatch safeObjectForKey:@"color"] stringByReplacingOccurrencesOfString:@"#" withString:@""];
    [viewPlayed setBackgroundColor:[SHARED_APPDELEGATE colorWithHexString:strHexColor]];
}

@end
