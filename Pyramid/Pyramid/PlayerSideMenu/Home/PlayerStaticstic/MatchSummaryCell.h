//
//  MatchSummaryCell.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MatchSummaryCell : UICollectionViewCell {
    
    IBOutlet UIView *viewBack;
    IBOutlet UIView *viewPlayed;
    
    IBOutlet UILabel *lblYear;
    IBOutlet UILabel *lblPlayedCount;
}

-(void)setMatchSummaryDataList:(NSDictionary *)dictMatch;

@end

NS_ASSUME_NONNULL_END
