//
//  StaticsticDetailViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MatchSummaryCell.h"
#import "StatisticsList.h"

NS_ASSUME_NONNULL_BEGIN

@interface StaticsticDetailViewController : UIViewController {
    
    IBOutlet UICollectionView *collectionMatchSummary;
    
    IBOutlet UILabel *lblPM;
    IBOutlet UILabel *lblWin;
    IBOutlet UILabel *lblDrow;
    IBOutlet UILabel *lblOS;
    IBOutlet UILabel *lblEfficiency;
    
    NSMutableArray *arrMatchSummaryList;
}

@property (nonatomic , strong) StatisticsList *objStatistics;

@end

NS_ASSUME_NONNULL_END
