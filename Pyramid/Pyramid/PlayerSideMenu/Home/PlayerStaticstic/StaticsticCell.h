//
//  StaticsticCell.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StatisticsList.h"

NS_ASSUME_NONNULL_BEGIN

@protocol StaticsticCellDelegate <NSObject>

-(void)setStaticsticInfoAtIndex:(NSInteger)index;

@end

@interface StaticsticCell : UITableViewCell {
    
    IBOutlet UIView *viewBack;
    
    IBOutlet UILabel *lblPlayerName;
}

//Delegate
@property (nonatomic , strong) id<StaticsticCellDelegate> delegate;

//Integer
@property (nonatomic) NSInteger index;

-(void)getStatisticsDataList:(StatisticsList *)objStatistics;

@end

NS_ASSUME_NONNULL_END
