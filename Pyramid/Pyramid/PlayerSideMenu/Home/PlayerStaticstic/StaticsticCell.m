//
//  StaticsticCell.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "StaticsticCell.h"

@implementation StaticsticCell

@synthesize delegate;
@synthesize index;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    viewBack.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)getStatisticsDataList:(StatisticsList *)objStatistics {
    
    lblPlayerName.text = [NSString stringWithFormat:@"%@",objStatistics.player_name];
}

- (IBAction)onClickStaticsticInfoBtn:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(setStaticsticInfoAtIndex:)]) {
        [delegate setStaticsticInfoAtIndex:index];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
