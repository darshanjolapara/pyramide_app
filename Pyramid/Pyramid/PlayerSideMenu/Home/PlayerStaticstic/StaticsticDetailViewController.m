//
//  StaticsticDetailViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/6/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "StaticsticDetailViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"

@interface StaticsticDetailViewController ()

@end

@implementation StaticsticDetailViewController

@synthesize objStatistics;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.titleView = [SHARED_APPDELEGATE getNavigationCenterWithTitle:NavigationStaticsticDetails fontSize:18];
    
    UIImage *backButtonImage = [UIImage imageNamed:@"Back"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton setImage:backButtonImage
                forState:UIControlStateNormal];
    
    backButton.frame = CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
    
    [backButton addTarget:self
                   action:@selector(popViewController)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    
    lblPM.text = [NSString stringWithFormat:@"%@",objStatistics.player_total_games];
    lblWin.text = [NSString stringWithFormat:@"%@",objStatistics.player_total_win_games];
    lblDrow.text = [NSString stringWithFormat:@"%@",objStatistics.player_total_lose_games];
    lblOS.text = [NSString stringWithFormat:@"%@",objStatistics.player_o_s];
    lblEfficiency.text = [NSString stringWithFormat:@"%@",objStatistics.player_efficiency];
    
    NSLog(@"%@",objStatistics.arrMonthsList);
    
    arrMatchSummaryList = [[NSMutableArray alloc] init];
    
    [arrMatchSummaryList addObjectsFromArray:objStatistics.arrMonthsList];
    
    [collectionMatchSummary registerNib:[UINib nibWithNibName:@"MatchSummaryCell" bundle:nil] forCellWithReuseIdentifier:@"MatchSummaryCell"];
}

#pragma mark -
#pragma mark - Pop To View Mothod

-(void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - COLLECTION_DELEGATE_AND_DATASOURCE_METHODS

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [arrMatchSummaryList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    MatchSummaryCell * cell = (MatchSummaryCell *)[cv dequeueReusableCellWithReuseIdentifier:@"MatchSummaryCell" forIndexPath:indexPath];
    
    if ([arrMatchSummaryList count] > 0) {
        NSDictionary *dictmatchSummary = [arrMatchSummaryList objectAtIndex:indexPath.row];
        [cell setMatchSummaryDataList:dictmatchSummary];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeMake(0, 0);
}

#pragma mark -
#pragma mark - COLLECTION_FLOWLAYOUT_LANDSCAPE_AND_PORTRAIT

- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    UICollectionViewFlowLayout *flowLayout = (id)collectionMatchSummary.collectionViewLayout;
    
    float width = (SHARED_APPDELEGATE.window.frame.size.width - 32)/2;
    float height =  50.0f;
    NSLog(@"HEIGHT %f WIDTH %f", height , width);
    flowLayout.itemSize = CGSizeMake(width, height);
    
    [flowLayout invalidateLayout];
}

@end
