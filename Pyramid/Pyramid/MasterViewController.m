//
//  MasterViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "MasterViewController.h"
#import "MFSideMenu.h"
#import "PyramidSideMenuViewController.h"

@interface MasterViewController ()

@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SideMenuBar" object:self];
      
    [self setBackButton];
}

-(void)setBackButton {
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"SideMenu"] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 0, 40, 40);
    [backButton addTarget:self action:@selector(onClickMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
}

-(void)onClickMenu {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    PyramidSideMenuViewController *objLFBetSideMenu = [[PyramidSideMenuViewController alloc] initWithNibName:@"PyramidSideMenuViewController" bundle:nil];
    }];
}

#pragma mark -
#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
