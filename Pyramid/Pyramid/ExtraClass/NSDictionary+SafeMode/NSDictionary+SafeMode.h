//
//  NSDictionary+SafeMode.h
//  FlexyBox
//
//  Created by ZheZhun Xuan on 10/31/12.
//
//

#import <Foundation/Foundation.h>

@interface NSDictionary (SafeMode)
- (id) safeObjectForKey: (NSString*) key;
@end
