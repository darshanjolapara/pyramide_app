//
//  NSDictionary+SafeMode.m
//  FlexyBox
//
//  Created by ZheZhun Xuan on 10/31/12.
//
//

#import "NSDictionary+SafeMode.h"

@implementation NSDictionary (SafeMode)

- (id) safeObjectForKey: (NSString*) key
{
    NSObject* ret = [self objectForKey: key];
    if(ret == nil) {
        return @"";
    }
    if ([ret isKindOfClass: [NSNull class]]) {
        return @"";
    }

    if ([ret isKindOfClass: [NSMutableArray class]] || [ret isKindOfClass: [NSArray class]] || [ret isKindOfClass: [NSDictionary class]] || [ret isKindOfClass: [NSMutableDictionary class]]) {
        return ret;
    }
    
    NSString *strData = [NSString stringWithFormat:@"%@",ret];
    return strData;
}

@end
