//
//  AsyImage.m
//  Zobaze
//
//  Created by  on 18/02/16.
//  Copyright © 2016 com.abc. All rights reserved.
//

#import "AsyImage.h"
#import "UIImageView+Letters.h"

@implementation AsyImage

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@synthesize delegate;
@synthesize onAsyncTouchSelector;
@synthesize imgUrl;

-(id)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame:frame]){
        
        self.clipsToBounds = YES;
        [self setUserInteractionEnabled:YES];
        [self setMultipleTouchEnabled:YES];
        UITapGestureRecognizer *singleFingerDTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        [self addGestureRecognizer:singleFingerDTap];
        [self setSpinView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if(self =[super initWithCoder:aDecoder])
    {
        self.clipsToBounds = YES;
        [self setUserInteractionEnabled:YES];
        [self setMultipleTouchEnabled:YES];
        UITapGestureRecognizer *singleFingerDTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        [self addGestureRecognizer:singleFingerDTap];
        [self setSpinView];
    }
    return self;
}

-(void)setSpinView {
    loadingVew =[[BLMultiColorLoader alloc]initWithFrame:CGRectMake((self.frame.size.width-30)/2,(self.frame.size.height-30)/2, 30, 30)];
    
    loadingVew.lineWidth = 3.0;
    loadingVew.colorArray = [NSArray arrayWithObjects:appNavBarColor,
                          [UIColor blueColor],
                          [UIColor greenColor],
                          [UIColor grayColor], nil];
    
    [loadingVew startAnimation];
    [self addSubview:loadingVew];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
}

-(void) handleSingleTap:(id)sender
{
    if([delegate respondsToSelector:onAsyncTouchSelector]){
        [delegate performSelector:onAsyncTouchSelector withObject:self];
    }
}


-(void)loadingImage:(NSString *)url placeHolderImage:(UIImage *)imageName {
    
    if(url != nil && [url isEqualToString:@""]) {
        if(imageName != nil) {
            self.image = imageName;
        }
        [self setContentMode:UIViewContentModeScaleAspectFit];
    }else {
        self.imgUrl  = [NSURL URLWithString:url];
        
        if(imageName != nil){
            self.image = imageName;
        }
       //[self setContentMode:UIViewContentModeScaleToFill];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:url]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    //[self setContentMode:UIViewContentModeScaleAspectFill];
                                    self.image = image;
                                    [self->loadingVew stopAnimation];
                                }else {
                                    [self->loadingVew stopAnimation];
                                }
                            }];
        [loadingVew stopAnimation];
    }
}



-(void)loadImageWithURl:(NSString *)url
{
    self.imgUrl  = [NSURL URLWithString:url];
    [self loadImageWithURl:url andPlaceHolderImage:nil];
}

-(void)loadImageWithURl:(NSString *)url andPlaceHolderImage:(UIImage *)placeHolderImage
{
    if(url == nil || [url isEqualToString:@""]){
        
      //  [self setImageWithString:@"tt" color:COLOR_DARK_YELLOW_CUST circular:NO textAttributes:@{NSFontAttributeName:FONT_FIRA_MEDIUM(20), NSForegroundColorAttributeName:[UIColor whiteColor]}];
        //[loadingVew stopAnimation];
        if(placeHolderImage != nil){
            self.image = placeHolderImage;
        }
    }else{
        
        self.imgUrl  = [NSURL URLWithString:url];
        
       // [loadingVew startAnimation];
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:url]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    self.image = image;
                                }
                                [loadingVew stopAnimation];
                            }];
    }
}

-(void)loadImageBlogWithURl:(NSString *)url andPlaceHolderImage:(UIImage *)placeHolderImage
{
    if(url == nil || [url isEqualToString:@""]){
        
        //  [self setImageWithString:@"tt" color:COLOR_DARK_YELLOW_CUST circular:NO textAttributes:@{NSFontAttributeName:FONT_FIRA_MEDIUM(20), NSForegroundColorAttributeName:[UIColor whiteColor]}];
        //[loadingVew stopAnimation];
        if(placeHolderImage != nil){
            self.image = placeHolderImage;
        }
    }else{
        
        self.imgUrl  = [NSURL URLWithString:url];
        
        // [loadingVew startAnimation];
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:url]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    self.image = image;
                                }
                                [loadingVew stopAnimation];
                            }];
    }
}

-(void)loadImageWithURl:(NSString *)imageURL andPlaceholderName:(NSString *)strName
{
    if(imageURL == nil || [imageURL isEqualToString:@""]){
        
        [self setImageWithString:strName color:[UIColor lightGrayColor] circular:NO textAttributes:@{NSFontAttributeName:FONT_Semibold(20), NSForegroundColorAttributeName:[UIColor whiteColor]}];

        [loadingVew stopAnimation];
        
    }else{
        self.imgUrl  = [NSURL URLWithString:imageURL];
        
        [loadingVew startAnimation];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:imageURL]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    self.image = image;
                                }
                                [loadingVew stopAnimation];
                            }];
    }
}

@end
