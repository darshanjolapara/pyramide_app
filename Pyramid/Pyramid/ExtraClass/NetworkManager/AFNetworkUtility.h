//
//  AFNetworkUtility.h
//  LFBet
//
//  Created by Darshan Jolapara on 1/15/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AFNetworking.h"

@class AFNetworkUtility;

NS_ASSUME_NONNULL_BEGIN


@protocol AFNetworkUtilityDelegate <NSObject>

@optional

-(void)afUtility:(AFNetworkUtility *)utility fetchDataSuccess:(NSMutableDictionary *)dicsResponce;
-(void)afUtility:(AFNetworkUtility *)utility fetchDataFail:(NSError *)error;

@end

@interface AFNetworkUtility : NSObject {
    id<AFNetworkUtilityDelegate> delegate;
}

@property (strong , nonatomic) id<AFNetworkUtilityDelegate> delegate;

-(void) requestWithUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam;
-(void)requestWithUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam withProImage:(UIImage *)uploadImg imageProName:(NSString *)strImageName imgParamProName:(NSString *)strImgProParam;
-(void) requestWithTestUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam;
-(void) requestWithUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam andToken:(NSString *)strToken;
-(void)requestWithGETUrl:(NSString*)strUrl tokenID:(NSString*)strToken;
-(void)requestWithUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam withProImage:(UIImage *)uploadImg imageProName:(NSString *)strImageName imgParamProName:(NSString *)strImgProParam andIDProofImage:(UIImage *)uploadIDProofImg imageIDProofName:(NSString *)strImageIDProofName imgParamIDProofName:(NSString *)strImgIDProofParam strKeyToken:(NSString *)strToken;

-(void)requestWithUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam withProImage:(UIImage *)uploadImg imageProName:(NSString *)strImageName imgParamProName:(NSString *)strImgProParam andToken:(NSString *)strToken;
-(void)requestWithUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam withProImage:(UIImage *)uploadImg imageProName:(NSString *)strImageName imgParamProName:(NSString *)strImgProParam andIDProofImage:(UIImage *)uploadIDProofImg imageIDProofName:(NSString *)strImageIDProofName imgParamIDProofName:(NSString *)strImgIDProofParam andToken:(NSString *)strToken;
-(void)requestWithGETUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam tokenID:(NSString*)strToken;
-(void)requestWithUrl123:(NSString*)strUrl param:(NSMutableDictionary*)dictParam withProImage:(NSString *)uploadImg imageProName:(NSString *)strImageName imgParamProName:(NSString *)strImgProParam andIDProofImage:(NSString *)uploadIDProofImg imageIDProofName:(NSString *)strImageIDProofName imgParamIDProofName:(NSString *)strImgIDProofParam strKeyToken:(NSString *)strToken;

-(void)requestWithAllfreshRegisteUrl:(NSString *)strUrl param:(NSMutableDictionary *)dictParam withUserImg:(UIImage *)imgUploadUse imgName:(NSString *)strImgName imgPrameName:(NSString *)strImgParam;

-(void)requestWithGETAppImageUrl:(NSString*)strUrl;
-(void)requestWithGETUrl:(NSString*)strUrl;
-(void) requestPostWithUrl:(NSString*)strUrl andToken:(NSString *)strToken;

-(void)cancelRequest;

@end

NS_ASSUME_NONNULL_END
