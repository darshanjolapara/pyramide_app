//
//  AFNetworkUtility.m
//  LFBet
//
//  Created by Darshan Jolapara on 1/15/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "AFNetworkUtility.h"

@implementation AFNetworkUtility

@synthesize delegate;

-(void) requestWithUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam {
        
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];
    manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];

//    NSDictionary *headers = @{@"Content-Type":@"text/html"};
    
    [manager POST:strUrl parameters:dictParam headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject>>> %@",responseObject);

//        NSMutableDictionary *dictJson = [NSJSONSerialization JSONObjectWithData: responseObject options:NSJSONReadingMutableContainers error:nil];
//        [self getSuccessResponce:dictJson];
        
        [self getSuccessResponce:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"ErrorJsonData>>> %@",error);
        if([self->delegate respondsToSelector:@selector(afUtility:fetchDataFail:)]){
            [self getFeailResponce:error];
        }
    }];
}

-(void) requestWithUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam andToken:(NSString *)strToken {
    //robert.jancigaj@gmail.com
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];
    manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];

//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [manager.requestSerializer setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
//    [manager.requestSerializer setValue:strToken forHTTPHeaderField:@"Authorization"];

//    NSDictionary *headers = @{@"Content-Type":@"application/json",@"Accept":@"application/json",@"X-Requested-With":@"XMLHttpRequest",@"Authorization":strToken};
    
    NSDictionary *headers = @{@"api-key":strToken};

    
    [manager POST:strUrl parameters:dictParam headers:headers progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject>>> %@",responseObject);

        [self getSuccessResponce:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"ErrorJsonData>>> %@",error);
        if([self->delegate respondsToSelector:@selector(afUtility:fetchDataFail:)]){
            [self getFeailResponce:error];
        }
    }];
}

-(void) requestPostWithUrl:(NSString*)strUrl andToken:(NSString *)strToken {
    //robert.jancigaj@gmail.com
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];
    manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    NSDictionary *headers = @{@"api-key":strToken};

    
    [manager POST:strUrl parameters:nil headers:headers progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject>>> %@",responseObject);

        [self getSuccessResponce:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"ErrorJsonData>>> %@",error);
        if([self->delegate respondsToSelector:@selector(afUtility:fetchDataFail:)]){
            [self getFeailResponce:error];
        }
    }];
}

-(void)requestWithGETUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam tokenID:(NSString*)strToken {
        
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];
    manager = [AFHTTPSessionManager manager];

//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [manager.requestSerializer setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
//    [manager.requestSerializer setValue:strToken forHTTPHeaderField:@"Authorization"];

     NSDictionary *headers = @{@"Content-Type":@"application/json",@"Accept":@"application/json",@"X-Requested-With":@"XMLHttpRequest",@"Authorization":strToken};
    
    [manager GET:strUrl parameters:dictParam headers:headers progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject>>> %@",responseObject);

        [self getSuccessResponce:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"ErrorJsonData>>> %@",error);
        if([self->delegate respondsToSelector:@selector(afUtility:fetchDataFail:)]){
            [self getFeailResponce:error];
        }
    }];
}


-(void)requestWithGETUrl:(NSString*)strUrl tokenID:(NSString*)strToken {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];
    manager = [AFHTTPSessionManager manager];

//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [manager.requestSerializer setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
//    [manager.requestSerializer setValue:strToken forHTTPHeaderField:@"Authorization"];

    NSDictionary *headers = @{@"Content-Type":@"application/json",@"Accept":@"application/json",@"X-Requested-With":@"XMLHttpRequest",@"Authorization":strToken};
    
    [manager GET:strUrl parameters:nil headers:headers progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject>>> %@",responseObject);

        [self getSuccessResponce:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"ErrorJsonData>>> %@",error);
        if([self->delegate respondsToSelector:@selector(afUtility:fetchDataFail:)]){
            [self getFeailResponce:error];
        }
    }];
}

-(void)requestWithGETUrl:(NSString*)strUrl {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];
        manager = [AFHTTPSessionManager manager];

    //    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //    [manager.requestSerializer setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    //    [manager.requestSerializer setValue:strToken forHTTPHeaderField:@"Authorization"];
        
    [manager GET:strUrl parameters:nil headers:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject>>> %@",responseObject);

        [self getSuccessResponce:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"ErrorJsonData>>> %@",error);
        if([self->delegate respondsToSelector:@selector(afUtility:fetchDataFail:)]){
            [self getFeailResponce:error];
        }
    }];
}

-(void)requestWithAllfreshRegisteUrl:(NSString *)strUrl param:(NSMutableDictionary *)dictParam withUserImg:(UIImage *)imgUploadUse imgName:(NSString *)strImgName imgPrameName:(NSString *)strImgParam {
    
     AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];
    
    [manager POST:strUrl parameters:dictParam headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        NSData *imageProfileData = UIImageJPEGRepresentation(imgUploadUse, 0.8);

        [formData appendPartWithFileData:imageProfileData
            name:strImgParam
        fileName:strImgName mimeType:@"image/jpg"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject>>> %@",responseObject);

//        NSMutableDictionary *dictJson = [NSJSONSerialization JSONObjectWithData: responseObject options:NSJSONReadingMutableContainers error:nil];
//
//        NSLog(@"%@",dictJson);
        
        [self getSuccessResponce:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"ErrorJsonData>>> %@",error);
        if([self->delegate respondsToSelector:@selector(afUtility:fetchDataFail:)]){
            [self getFeailResponce:error];
        }
    }];
}

-(void)requestWithUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam withProImage:(UIImage *)uploadImg imageProName:(NSString *)strImageName imgParamProName:(NSString *)strImgProParam andToken:(NSString *)strToken {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];

//    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
//    [manager.requestSerializer setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [manager.requestSerializer setValue:strToken forHTTPHeaderField:@"Authorization"];

//    NSDictionary *headers = @{@"Content-Type":@"application/json",@"Accept":@"application/json",@"X-Requested-With":@"XMLHttpRequest",@"Authorization":strToken};
    
    NSDictionary *headers = @{@"api-key":strToken};
    
    [manager POST:strUrl parameters:dictParam headers:headers constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        NSData *imageProfileData = UIImageJPEGRepresentation(uploadImg, 0.8);

        [formData appendPartWithFileData:imageProfileData
            name:strImgProParam
        fileName:strImageName mimeType:@"image/jpeg"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject>>> %@",responseObject);

        NSMutableDictionary *dictJson = [NSJSONSerialization JSONObjectWithData: responseObject options:NSJSONReadingMutableContainers error:nil];
        
        NSLog(@"%@",dictJson);
        
        [self getSuccessResponce:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"ErrorJsonData>>> %@",error);
        if([self->delegate respondsToSelector:@selector(afUtility:fetchDataFail:)]){
            [self getFeailResponce:error];
        }
    }];
}

-(void) requestWithUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam withImage:(UIImage *)uploadImg imageName:(NSString *)strImageName {
    
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];
//
////    [manager POST:strUrl parameters:dictParam constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
////
////        NSData *imageData = UIImageJPEGRepresentation(uploadImg, 0.8);
////
////        [formData appendPartWithFileData:imageData
////                                    name:strImageName
////                                fileName:@"tmpName.jpg" mimeType:@"image/jpeg"];
////
////    } progress:^(NSProgress * _Nonnull uploadProgress) {
////
////    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
////
////        NSLog(@"responseObject>>> %@",responseObject);
////
////        [self getSuccessResponce:responseObject];
////
////    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
////
////        NSLog(@"ErrorJsonData>>> %@",error);
////        if([self->delegate respondsToSelector:@selector(afUtility:fetchDataFail:)]){
////            [self getFeailResponce:error];
////        }
////    }];
}

-(void)requestWithUrl123:(NSString*)strUrl param:(NSMutableDictionary*)dictParam withProImage:(NSString *)uploadImg imageProName:(NSString *)strImageName imgParamProName:(NSString *)strImgProParam andIDProofImage:(NSString *)uploadIDProofImg imageIDProofName:(NSString *)strImageIDProofName imgParamIDProofName:(NSString *)strImgIDProofParam strKeyToken:(NSString *)strToken {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];
    manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        
    NSDictionary *headers = @{@"api-key":strToken};
    
    //NSDictionary *headers = @{@"Content-Type":@"application/json",@"Accept":@"application/json",@"X-Requested-With":@"XMLHttpRequest"};
    
    [manager POST:strUrl parameters:dictParam headers:headers constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
    
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:uploadImg] name:@"profile_pic" fileName:strImageName mimeType:@"image/jpeg" error:nil];
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:uploadIDProofImg] name:@"club_logo" fileName:strImageIDProofName mimeType:@"image/jpeg" error:nil];
                
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject>>> %@",responseObject);

        [self getSuccessResponce:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"ErrorJsonData>>> %@",error);
        if([self->delegate respondsToSelector:@selector(afUtility:fetchDataFail:)]){
            [self getFeailResponce:error];
        }
    }];
}

-(void)requestWithUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam withProImage:(UIImage *)uploadImg imageProName:(NSString *)strImageName imgParamProName:(NSString *)strImgProParam andIDProofImage:(UIImage *)uploadIDProofImg imageIDProofName:(NSString *)strImageIDProofName imgParamIDProofName:(NSString *)strImgIDProofParam strKeyToken:(NSString *)strToken {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];
    manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        
    NSDictionary *headers = @{@"api-key":strToken};
    
    //NSDictionary *headers = @{@"Content-Type":@"application/json",@"Accept":@"application/json",@"X-Requested-With":@"XMLHttpRequest"};
    
    [manager POST:strUrl parameters:dictParam headers:headers constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        NSData *imageProData = UIImageJPEGRepresentation(uploadImg, 0.8);
        NSData *imageIDProofData = UIImageJPEGRepresentation(uploadIDProofImg, 0.8);

        [formData appendPartWithFileData:imageProData
            name:strImgProParam
        fileName:strImageName mimeType:@"png"];

        [formData appendPartWithFileData:imageIDProofData
            name:strImgIDProofParam
        fileName:strImageIDProofName mimeType:@"png"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject>>> %@",responseObject);

        [self getSuccessResponce:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"ErrorJsonData>>> %@",error);
        if([self->delegate respondsToSelector:@selector(afUtility:fetchDataFail:)]){
            [self getFeailResponce:error];
        }
    }];
}

-(void)requestWithUrl:(NSString*)strUrl param:(NSMutableDictionary*)dictParam withProImage:(UIImage *)uploadImg imageProName:(NSString *)strImageName imgParamProName:(NSString *)strImgProParam andIDProofImage:(UIImage *)uploadIDProofImg imageIDProofName:(NSString *)strImageIDProofName imgParamIDProofName:(NSString *)strImgIDProofParam andToken:(NSString *)strToken {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];

//    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [manager.requestSerializer setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
//    [manager.requestSerializer setValue:strToken forHTTPHeaderField:@"Authorization"];

    NSDictionary *headers = @{@"Content-Type":@"application/json",@"Accept":@"application/json",@"X-Requested-With":@"XMLHttpRequest",@"Authorization":strToken};
    
    [manager POST:strUrl parameters:dictParam headers:headers constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        NSData *imageProData = UIImageJPEGRepresentation(uploadImg, 0.8);
        NSData *imageIDProofData = UIImageJPEGRepresentation(uploadIDProofImg, 0.8);

        [formData appendPartWithFileData:imageProData
            name:strImgProParam
        fileName:strImageName mimeType:@"image/jpeg"];

        [formData appendPartWithFileData:imageIDProofData
            name:strImgIDProofParam
        fileName:strImageIDProofName mimeType:@"image/jpeg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject>>> %@",responseObject);

        [self getSuccessResponce:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"ErrorJsonData>>> %@",error);
        if([self->delegate respondsToSelector:@selector(afUtility:fetchDataFail:)]){
            [self getFeailResponce:error];
        }
    }];
}

-(void)requestWithGETAppImageUrl:(NSString*)strUrl {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];
    manager = [AFHTTPSessionManager manager];

//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [manager.requestSerializer setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    
    NSDictionary *headers = @{@"Content-Type":@"application/json",@"Accept":@"application/json",@"X-Requested-With":@"XMLHttpRequest"};
    
    [manager GET:strUrl parameters:nil headers:headers progress:^(NSProgress * _Nonnull downloadProgress) {
       
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
       NSLog(@"responseObject>>> %@",responseObject);
                    
       [self getSuccessResponce:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       NSLog(@"ErrorJsonData>>> %@",error);
       if([self->delegate respondsToSelector:@selector(afUtility:fetchDataFail:)]){
           [self getFeailResponce:error];
       }
    }];
}

-(void)getSuccessResponce:(NSMutableDictionary *)dictResponce {
    [delegate afUtility:self fetchDataSuccess:dictResponce];
}

-(void)getFeailResponce:(NSError *)error {
    [delegate afUtility:self fetchDataFail:error];
}

-(void)cancelRequest {
    
}

@end
