//
//  AppDelegate.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "PyramidSideMenuViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
    UIView *loadView;
    UIView *viewBack;
    UILabel *lblLoading;
    UIActivityIndicatorView *spinningWheel;
}

@property (nonatomic , strong) UIWindow *window;

@property (nonatomic , strong) UINavigationController *navigation;

+(AppDelegate*)sharedAppDelegate;
-(UIView*)getTextFieldLeftAndRightView;
-(UIView*)getTextFieldLeftAndRightViewInEditProfile;
-(void)setPyramidViewScreen;
-(UILabel *)getNavigationCenterWithTitle:(NSString *)title fontSize:(int)size;
-(void)setAppProperty;
-(void)setLoginViewScreenDisplay;
-(void)showLoadingView;
-(void) hideLoadingView;
-(UIColor*)colorWithHexString:(NSString*)hex;
-(void)saveDefaultDataRemove;
-(NSString *)getfileUniqueNameWithextention:(NSString *)extention;
-(NSString *)getNutritionImagePathwithImageName:(NSString *)imgName;
-(NSString *)applicationCacheDirectory;

@end

