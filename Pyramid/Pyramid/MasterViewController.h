//
//  MasterViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MasterViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
