//
//  PyramidSideMenuViewController.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PyramidSideMenuCell.h"
#import "LogoutPopUpViewController.h"
#import "AsyImage.h"
#import "AFNetworkUtility.h"

NS_ASSUME_NONNULL_BEGIN

@interface PyramidSideMenuViewController : UIViewController <LogoutPopUpViewControllerDelegate,AFNetworkUtilityDelegate> {
    
    IBOutlet UITableView *tblPyramidSideMenu;
    
    IBOutlet AsyImage *asyImgProfile;
    
    IBOutlet UILabel *lblUserName;
    IBOutlet UILabel *lblEmaiID;
    
    IBOutlet UIView *viewHeader;
    
    NSMutableArray *arrMenu;
    
    AFNetworkUtility *afPyramidePlayerLogout;
}

@end

NS_ASSUME_NONNULL_END
