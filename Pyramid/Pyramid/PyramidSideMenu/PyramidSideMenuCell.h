//
//  PyramidSideMenuCell.h
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PyramidSideMenuCell : UITableViewCell {
    
    IBOutlet UILabel *lblTitle;
    
    IBOutlet UIImageView *imgIcon;
}

-(void)setMenuData:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
