//
//  PyramidSideMenuViewController.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "PyramidSideMenuViewController.h"
#import "AppDelegate.h"
#import "CommonClass.h"
#import "MFSideMenu.h"
#import "PlayerHomeViewController.h"
#import "SettingViewController.h"
#import "ChangePasswordViewController.h"
#import "MyProfileViewController.h"

@interface PyramidSideMenuViewController ()

@end

@implementation PyramidSideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setSideMenuList:) name:@"SideMenuBar" object:nil];
}

-(void)setSideMenuList:(NSNotification *)notification {
    
    arrMenu = [[NSMutableArray alloc] init];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    [arrMenu addObject:[self getMainDictWithTitle:@"Home" imgName:@"Home"]];
    [arrMenu addObject:[self getMainDictWithTitle:@"Update Profile" imgName:@"MyProfile"]];
    [arrMenu addObject:[self getMainDictWithTitle:@"Setting" imgName:@"Setting"]];
    [arrMenu addObject:[self getMainDictWithTitle:@"Change Password" imgName:@"ChangePassword"]];
    [arrMenu addObject:[self getMainDictWithTitle:@"Log Out" imgName:@"LogOut"]];
     
    lblUserName.text = [NSString stringWithFormat:@"%@",[userDefault objectForKey:USERNAME]];
    lblEmaiID.text = [NSString stringWithFormat:@"%@",[userDefault objectForKey:USEREMAIL]];
    
    NSString *imgProfilePicUrl = [NSString stringWithFormat:@"%@",[userDefault objectForKey:PROFILEPIC]];

    if (imgProfilePicUrl.length > 0) {
       [asyImgProfile loadingImage:imgProfilePicUrl placeHolderImage:USER_PLACEHOLDER_IMAGE];
    }else {
       [asyImgProfile loadingImage:nil placeHolderImage:USER_PLACEHOLDER_IMAGE];
    }
        
    [tblPyramidSideMenu reloadData];
}

-(NSMutableDictionary *)getMainDictWithTitle:(NSString *)title imgName:(NSString *)imgname {
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:title forKey:@"title"];
    [dict setObject:imgname forKey:@"image"];
    
    return dict;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 182.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return viewHeader;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [arrMenu count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"PyramidSideMenuCell";
    
    PyramidSideMenuCell  *cell = (PyramidSideMenuCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PyramidSideMenuCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;

    [cell setMenuData:[arrMenu objectAtIndex:indexPath.row]];
    
//    if (SHARED_APPDELEGATE.selectedMenu == indexPath.row) {
//        [cell setMenuData:[arrMenu objectAtIndex:indexPath.row] selected:NO];
//    }else {
//        [cell setMenuData:[arrMenu objectAtIndex:indexPath.row] selected:YES];
//    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        
    if (indexPath.row == 0) {
       [self setHomeViewController];
    }else if (indexPath.row == 1) {
       [self setMyProfileViewController];
    }else if (indexPath.row == 2) {
       [self setSettingViewController];
    }else if (indexPath.row == 3) {
       [self setChangePasswordViewController];
    }else if (indexPath.row == 4) {
       [self setLogOutPopUpViewController];
    }
        
    [tblPyramidSideMenu reloadData];
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}

-(void)setHomeViewController {
    PlayerHomeViewController *viewUserProfile = [[PlayerHomeViewController alloc] initWithNibName:@"PlayerHomeViewController" bundle:nil];
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:viewUserProfile];
    navigationController.viewControllers = controllers;
}

-(void)setMyProfileViewController {
    MyProfileViewController *viewUserProfile = [[MyProfileViewController alloc] initWithNibName:@"MyProfileViewController" bundle:nil];
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:viewUserProfile];
    navigationController.viewControllers = controllers;
}

-(void)setSettingViewController {
    SettingViewController *viewUserProfile = [[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:viewUserProfile];
    navigationController.viewControllers = controllers;
}

-(void)setChangePasswordViewController {
    ChangePasswordViewController *viewUserProfile = [[ChangePasswordViewController alloc] initWithNibName:@"ChangePasswordViewController" bundle:nil];
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:viewUserProfile];
    navigationController.viewControllers = controllers;
}

-(void)setLogOutPopUpViewController {
    LogoutPopUpViewController *viewPlayerDetail = [[LogoutPopUpViewController alloc] initWithNibName:@"LogoutPopUpViewController" bundle:nil];
    viewPlayerDetail.delegate = self;
    [self presentPopupViewController:viewPlayerDetail animationType:MJPopupViewAnimationFade];
}

-(void)setPyramidePlayerLogoutAction:(NSString *)strAction {
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    if ([strAction isEqualToString:@"Okay"]) {
        
        if (afPyramidePlayerLogout) {
           [afPyramidePlayerLogout cancelRequest];
           afPyramidePlayerLogout.delegate = self;
           afPyramidePlayerLogout = nil;
        }

        afPyramidePlayerLogout = [[AFNetworkUtility alloc] init];
        afPyramidePlayerLogout.delegate = self;

        [SHARED_APPDELEGATE showLoadingView];
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *strAPIKey = [NSString stringWithFormat:@"%@",[userDefault objectForKey:APIKEY]];
        
        [afPyramidePlayerLogout requestPostWithUrl:LogOut_Url andToken:strAPIKey];
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataSuccess:(NSMutableDictionary *)dictResponce {
        
    if (utility == afPyramidePlayerLogout) {
        
        NSLog(@"LOGOUTRESPONCE ::: %@", dictResponce);
        
        [SHARED_APPDELEGATE hideLoadingView];
                        
        if ([CommonClass complareTwoString:[dictResponce safeObjectForKey:@"error"] :@"0"]) {
            [SHARED_APPDELEGATE saveDefaultDataRemove];
            [SHARED_APPDELEGATE setLoginViewScreenDisplay];
        }else {
            [CommonClass showAlertWithTitle:provideAlert andMessage:[dictResponce safeObjectForKey:@"message"] delegate:self];
        }
    }
}

#pragma mark -
#pragma mark - AfNetworking_Delegate_Fail_Response_Method

-(void)afUtility:(AFNetworkUtility *)utility fetchDataFail:(NSError *)error {
    [SHARED_APPDELEGATE hideLoadingView];
    NSString *strError = [NSString stringWithFormat:@"%ld",(long)error.code];
    [CommonClass showAlertWithTitle:provideAlert andMessage:strError delegate:self];
}

@end
