//
//  PyramidSideMenuCell.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "PyramidSideMenuCell.h"

@implementation PyramidSideMenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setMenuData:(NSDictionary *)dict {
    
    lblTitle.text = [dict objectForKey:@"title"];
    lblTitle.textColor = appNavBarColor;

    NSString *imgName = [dict objectForKey:@"image"];

    if(imgName.length > 0) {
        imgIcon.image =[UIImage imageNamed:imgName];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
