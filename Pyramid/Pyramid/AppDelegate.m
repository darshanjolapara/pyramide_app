//
//  AppDelegate.m
//  Pyramid
//
//  Created by Darshan Jolapara on 6/5/20.
//  Copyright © 2020 Darshan Jolapara. All rights reserved.
//

#import "AppDelegate.h"
#import "MFSideMenuContainerViewController.h"
#import "PlayerHomeViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize window;
@synthesize navigation;

+(AppDelegate*)sharedAppDelegate {
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *strUserID = [userDefault objectForKey:USERID];
    
    if((strUserID != nil) || [strUserID isEqualToString:@""]) {
        [self setPyramidViewScreen];
    }else {
        [self setLoginViewScreenDisplay];
    }
    
    // Create Dir for save Images
    NSString *filePath = [[self applicationCacheDirectory] stringByAppendingPathComponent:IMAGE_UserPic];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSLog(@"FilePath %@",filePath);

    if(![fileManager fileExistsAtPath:filePath]) {
      [fileManager createDirectoryAtPath:filePath withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
   [self setAppProperty];
    
    return YES;
}

#pragma mark -
#pragma mark - LoginView

-(void)setLoginViewScreenDisplay {
    LoginViewController *viewSelectUser = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    navigation = [[UINavigationController alloc] initWithRootViewController:viewSelectUser];
    window.rootViewController = navigation;

    [window makeKeyAndVisible];
}

#pragma mark -
#pragma mark - Side Menu Open

-(void)setPyramidViewScreen {
    
    PyramidSideMenuViewController *menuView = [[PyramidSideMenuViewController alloc]initWithNibName:@"PyramidSideMenuViewController" bundle:nil];
    
    PlayerHomeViewController *viewHome = [[PlayerHomeViewController alloc] initWithNibName:@"PlayerHomeViewController" bundle:nil];
    
    navigation = [[UINavigationController alloc] initWithRootViewController:viewHome];
    
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:navigation
                                                    leftMenuViewController:menuView
                                                    rightMenuViewController:nil];
//    selectedMenu = 0;
    
    self.window.rootViewController = container;
    [window makeKeyAndVisible];
}

#pragma mark -
#pragma mark - TextField Padding Method

-(UIView*)getTextFieldLeftAndRightView {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 10)];
    return paddingView;
}

-(UIView*)getTextFieldLeftAndRightViewInEditProfile {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 5)];
    return paddingView;
}

#pragma mark - NavigationTitle_Method

-(UILabel *)getNavigationCenterWithTitle:(NSString *)title fontSize:(int)size {
    UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, SHARED_APPDELEGATE.window.frame.size.width-140 , 40)];
    lblTitle.font = FONT_Bold(size);
    lblTitle.text = title;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.textColor = [UIColor whiteColor];
    
    return lblTitle;
}

#pragma mark -
#pragma mark - NavigationBarColor

-(void)setAppProperty {
    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance]setBackgroundImage:[self imageFromColor:appNavBarColor] forBarMetrics:UIBarMetricsDefault];
}

-(UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark -
#pragma mark - Convert_Hex_TO_RGB

-(UIColor*)colorWithHexString:(NSString*)hex {
    
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];

    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];

    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];

    if ([cString length] != 6) return  [UIColor grayColor];

    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];

    range.location = 2;
    NSString *gString = [cString substringWithRange:range];

    range.location = 4;
    NSString *bString = [cString substringWithRange:range];

    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];

    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

#pragma mark -
#pragma mark - Loading View

-(void)showLoadingView {
    
    if (loadView == nil) {
        loadView = [[UIView alloc] initWithFrame:self.window.frame];
        loadView.opaque = NO;
        loadView.backgroundColor = [UIColor clearColor];
        
        viewBack = [[UIView alloc] initWithFrame:CGRectMake(80, 230, 160, 50)];
        viewBack.backgroundColor = [UIColor blackColor];
        viewBack.alpha = 0.7f;
        viewBack.layer.masksToBounds = NO;
        viewBack.layer.cornerRadius = 5;
        
        lblLoading = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 110, 50)];
        lblLoading.backgroundColor = [UIColor clearColor];
        lblLoading.textAlignment = NSTextAlignmentCenter;
        lblLoading.text = @"Please Wait...";
        lblLoading.numberOfLines = 2;
        
        spinningWheel = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(5.0, 10.0, 30.0, 30.0)];
        spinningWheel.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
        lblLoading.textColor = [UIColor whiteColor];
        [spinningWheel startAnimating];
        [viewBack addSubview:spinningWheel];
        
        [viewBack addSubview:lblLoading];
        [loadView addSubview:viewBack];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            float y = (loadView.frame.size.height/2 ) - (viewBack.frame.size.height/2);
            float x =(loadView.frame.size.width/2 ) - (viewBack.frame.size.width/2);
            viewBack.frame = CGRectMake(x , y, 160, 50);;
        }
        else{
            float y = (loadView.frame.size.height/2 ) - (viewBack.frame.size.height/2);
            float x =(loadView.frame.size.width/2 ) - (viewBack.frame.size.width/2);
            viewBack.frame = CGRectMake(x , y, 160, 50);;
        }
    }
    if(loadView.superview == nil)
        [self.window addSubview:loadView];
}

-(void) hideLoadingView {
    [loadView removeFromSuperview];
    loadView=nil;
}

-(void)saveDefaultDataRemove {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    [userDefault removeObjectForKey:DEVICETOKEN];
    [userDefault removeObjectForKey:USERID];
    [userDefault removeObjectForKey:APIKEY];
    [userDefault removeObjectForKey:FIRSTNAME];
    [userDefault removeObjectForKey:LASTNAME];
    [userDefault removeObjectForKey:PHONENUMBER];
    [userDefault removeObjectForKey:PROFILEPIC];
    [userDefault removeObjectForKey:SELECTLANG];
    [userDefault removeObjectForKey:SUBPLANID];
    [userDefault removeObjectForKey:TOKEN];
    [userDefault removeObjectForKey:USEREMAIL];
    [userDefault removeObjectForKey:USERNAME];
    [userDefault removeObjectForKey:USERTYPE];
    [userDefault removeObjectForKey:SUBSCRIPPLANEXPIRE];
    [userDefault removeObjectForKey:SUBSCRIPPLANNAME];
    [userDefault removeObjectForKey:SUBSCRIPPLANCOMPETITION];
    [userDefault removeObjectForKey:SUBSCRIPPLANPLAYERS];
    [userDefault removeObjectForKey:SUBSCRIPPLANENDDATE];
    [userDefault removeObjectForKey:SUBSCRIPPLANPRICE];
    [userDefault removeObjectForKey:SUBSCRIPPLANSTARTDATE];
    [userDefault removeObjectForKey:COMPETITIONID];
    [userDefault removeObjectForKey:COMPETITIONNAME];
    
    [userDefault synchronize];
}

-(NSString *)getfileUniqueNameWithextention:(NSString *)extention {
    NSTimeInterval timeStamp = [[NSDate date ] timeIntervalSince1970 ]  * 1000;
    NSString *randomName = [ NSString stringWithFormat:@"%f", timeStamp];
    randomName = [ randomName stringByReplacingOccurrencesOfString:@"." withString:@"" ];
    
    randomName =[NSString stringWithFormat:@"%@.%@", randomName, extention];
    NSLog(@"FILE NAME %@", randomName);
    return randomName;
}

-(NSString *)getNutritionImagePathwithImageName:(NSString *)imgName {
    NSString *catchDir  = [[AppDelegate sharedAppDelegate] applicationCacheDirectory];
    NSString *dataPath = [catchDir stringByAppendingPathComponent:IMAGE_UserPic];
    
    dataPath = [dataPath stringByAppendingPathComponent:imgName];
    NSLog(@"IAMGE SAVE PATH %@", dataPath);
    return dataPath;
}

-(NSString *)applicationCacheDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

@end
